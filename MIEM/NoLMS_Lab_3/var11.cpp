#include <iostream>
#include <set>
#include <vector>
#include <sstream>
#include <iterator>
#include <list>
#include <algorithm>

using namespace std;

void parseString(const string &src, vector<set<string>> &dst) {
	stringstream strstr(src);
	istream_iterator<string> it(strstr);
	istream_iterator<string> end;
	vector<string> words(it, end);
	for (auto str: words) {
		dst[127-str[0]].insert(str);
	}
}

void arreyListToList(const vector<list<int>> &src, list<int> &dst) {
	vector<list<int>> good_lists;
	for (auto lst: src) {
		if (lst.size() < 4) {
			good_lists.push_back(lst);
			good_lists[good_lists.size()-1].sort([&] (const int& a, const int& b) {
				return a > b;
			});
		}
	}
	sort(good_lists.begin(), good_lists.end(), [&] (const list<int>& A, const list<int>& B) {
		return A.back() < B.back();
	});
	for (auto lst: good_lists) {
		for (auto e: lst) {
			dst.push_back(e);
		}
	}
}

int main() {
	string test_str;
	vector<set<string>> test_sets(127);
	getline(cin, test_str);
	parseString(test_str, test_sets);
	for (int i = 0; i < 127; i++) {
		if (!test_sets[i].empty()) {
			cout << char(127-i) << ": ";
			for (auto str: test_sets[i])
				cout << str << " ";
			cout << "\n";
		}
	}

	list<int> test_res_list;
	int cnt_lst;
	cin >> cnt_lst;
	vector<list<int>> test_lists(cnt_lst);
	for (int i = 0; i < cnt_lst; i++) {
		int size_lst;
		cin >> size_lst;
		for (int j = 0; j < size_lst; j++) {
			int val;
			cin >> val;
			test_lists[i].push_back(val);
		}
	}
	arreyListToList(test_lists, test_res_list);
	for (auto v: test_res_list) {
		cout << v << " ";
	}
	cout << "\n";
	return 0;
}