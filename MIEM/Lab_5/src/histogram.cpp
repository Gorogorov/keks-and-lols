//#define _CRT_SECURE_NO_WARNINGS
#include "../include/histogram.h"
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <cstring>
#include <vector>
#include <algorithm>

using namespace std;

void AddBar(Histogram &historgam, const char *strName);
bool AppendBar(Histogram &historgam, const char *strName);

void AddBlock(Histogram &historgam, const char *strName){
	if(!AppendBar(historgam, strName)){
		AddBar(historgam, strName);
	}
}
void AddBlockModif(Histogram & histogram, const char *strName) {
	for (int i = 0; i < strlen(strName); i++) { 
		if (strName[i] <= 'z' && strName[i] >= 'a') {
			char *temp = new char[strlen(strName)];
			strcpy(temp, strName);
			temp[i+1] = '\0';
			AddBlock(histogram, &temp[i]);
		}
	}
}

int MaxLen(const Histogram &histogram) {
	int maxx = 0;
	for (int i = 0; i < histogram.nBarsSize; i++) {
		maxx = max(maxx, (int)strlen(histogram.BarNames[i]));
	}
	return maxx;
}

void PrintHistogram(const Histogram &histogram) {
   for (int i = 0; i < histogram.nBarsSize; i++) {
       cout << setfill(' ') << setw(MaxLen(histogram)) << left << histogram.BarNames[i] << '|';
       cout << setfill(histogram.chBlock) << setw(10 * histogram.Bars[i] / histogram.nMaxBar) << ""
            << endl;
   }
}

/*`void PrintHistogram(const Histogram &histogram) {
   for (int i = 0; i < histogram.nBarsSize; i++) {
       cout << setfill(' ') << setw(MaxLen(histogram)) << right << histogram.BarNames[i] << '|';
       cout << setfill(histogram.chBlock) << setw(10 * histogram.Bars[i] / histogram.nMaxBar) << ""
            << endl;
   }
}`*/

bool AppendBar(Histogram &historgam, const char *strName){
	for(int i=0; i<historgam.nBarsSize; i++){
		if(strcmp(historgam.BarNames[i], strName)==0){
			historgam.nMaxBar = max(historgam.nMaxBar, ++historgam.Bars[i]);
			return true;
		}
	}
	return false;
}

void AddBar(Histogram &historgam, const char *strName){
	char **newNames = new char *[historgam.nBarsSize+1];
	int   *newBars  = new int[historgam.nBarsSize+1];
	if(historgam.nBarsSize > 0){
		for(int i=0; i<historgam.nBarsSize; i++){
			newNames[i] = historgam.BarNames[i];
			newBars[i] = historgam.Bars[i];
		}
		delete[] historgam.BarNames;
		delete[] historgam.Bars;
	}

	newNames[historgam.nBarsSize] = new char[strlen(strName)+1];
	strcpy(newNames[historgam.nBarsSize], strName);
	newBars[historgam.nBarsSize] = 1;

	historgam.nBarsSize++;
	historgam.BarNames = newNames;
	historgam.Bars = newBars;
}

void SortByName(Histogram &histogram, bool bAscending) {
	vector<pair<string,int>> V;
	for (int i = 0; i < histogram.nBarsSize; i++) {
		string temp;
		for (int j = 0; j < strlen(histogram.BarNames[i]); j++) {
			temp.push_back(histogram.BarNames[i][j]);
		} 
		V.push_back(make_pair(temp, histogram.Bars[i]));
	}
	sort(V.begin(), V.end(), [&](const pair<string, int> &a, const pair<string, int> &b) {
		return a.first > b.first;
	});
	if (bAscending) reverse(V.begin(), V.end());
	for (int i = 0; i < histogram.nBarsSize; i++) {
		delete histogram.BarNames[i];
		histogram.BarNames[i] = new char[V[i].first.size()+1];
		for (int j = 0; j < V[i].first.size(); j++) {
			histogram.BarNames[i][j] = V[i].first[j];
		}
		histogram.BarNames[i][V[i].first.size()] = '\0';
		histogram.Bars[i] = V[i].second;
	}
}