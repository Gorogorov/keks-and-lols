#include "../include/histogram.h"
#include <iostream>
#include <iomanip>
#include <cstring>

 
int main(){
	const char * const strProgramName = "Histogramer 1.0";

	std::cout << std::setfill('*') << std::setw(strlen(strProgramName)+4) << "" << std::endl;
	std::cout << '*' << std::setfill(' ') << std::setw(strlen(strProgramName)+3) << '*' << std::endl;
	std::cout << "* " << strProgramName << " *" << std::endl;
	std::cout << '*' << std::setfill(' ') << std::setw(strlen(strProgramName)+3) << '*' << std::endl;
	std::cout << std::setfill('*') << std::setw(strlen(strProgramName)+4) << "" << std::endl << std::endl;

	Histogram myBarChart;

	InitHistogram(myBarChart);
	AddBlockModif(myBarChart, "abcYZab");
	AddBlockModif(myBarChart, "rrrrrrrrrrrrrrrrrrrr");
	AddBlockModif(myBarChart, "a");
	AddBlockModif(myBarChart, "b");
	AddBlockModif(myBarChart, "c");
	AddBlockModif(myBarChart, "a");
	AddBlockModif(myBarChart, "d");
	AddBlockModif(myBarChart, "e");
	AddBlockModif(myBarChart, "c");
	AddBlockModif(myBarChart, "z");
	AddBlockModif(myBarChart, "z");
	AddBlockModif(myBarChart, "z");
	AddBlockModif(myBarChart, "z");
	AddBlockModif(myBarChart, "z");
	AddBlockModif(myBarChart, "Y");
	AddBlockModif(myBarChart, "Y");
	/*AddBlock(myBarChart, "dsgfddf");
	AddBlock(myBarChart, "a");
	AddBlock(myBarChart, "a");
	AddBlock(myBarChart, "ar");*/
	SortByName(myBarChart);


	PrintHistogram(myBarChart);

	DestroyHistogram(myBarChart);

	return 0;
}