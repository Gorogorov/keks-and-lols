#include "../include/Polynomial.h"

using namespace std;

int main() {
	double* test_coef1 = new double[3];
	test_coef1[0] = 5, test_coef1[1] = 6, test_coef1[2] = 7;
	Polynomial P1(2, test_coef1);
	cout << P1.get_deg() << " " << P1.get_coef()[1] << "\n";

	double* test_coef2 = new double[2];
	test_coef2[0] = 2, test_coef2[1] = 3;
	Polynomial P2(1, test_coef2);
	cout << "P1: " << P1 << "P2: " << P2; 
	Polynomial P3 = P1+P2;
	cout << "P1+P2: " << P3;
	Polynomial P4 = P2-P1;
	cout << "P2-P1: " << P4;
	Polynomial P5 = P1*P2;
	cout << "P1*P2: " << P5;
	return 0;
}