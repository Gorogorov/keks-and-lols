#include "../include/Polynomial.h"
#include <iostream>

Polynomial::Polynomial() {
	deg = 0;
	coef = new double[1];
	coef[0] = 0;
}

Polynomial::Polynomial(int a, double* b) {
	deg = a;
	coef = b;
}

Polynomial::Polynomial(const Polynomial& P) {
	deg = P.deg;
	coef = P.coef;
}

Polynomial Polynomial::operator=(const Polynomial& P) {
	deg = P.deg;
	coef = P.coef;
	return *this;
}

int Polynomial::get_deg() const{
	return deg;
}

double* Polynomial::get_coef() const{
	return coef;
}

Polynomial Polynomial::operator+(const Polynomial& P) {
	Polynomial NP;
	NP.deg = max(deg, P.deg);
	NP.coef = new double[NP.deg+1];
	for (int i = 0; i <= NP.deg; i++)
		NP.coef[i] = 0;
	for (int i = 0; i <= deg; i++)
		NP.coef[i] = coef[i];
	for (int i = 0; i <= P.deg; i++)
		NP.coef[i] += P.coef[i];
	return NP;
}

Polynomial Polynomial::operator-(const Polynomial& P) {
	Polynomial NP;
	NP.deg = max(deg, P.deg);
	NP.coef = new double[NP.deg+1];
	for (int i = 0; i <= NP.deg; i++)
		NP.coef[i] = 0;
	for (int i = 0; i <= deg; i++)
		NP.coef[i] = coef[i];
	for (int i = 0; i <= P.deg; i++)
		NP.coef[i] -= P.coef[i];
	return NP;
}

Polynomial Polynomial::operator*(const Polynomial& P) {
	Polynomial NP;
	NP.deg = deg+P.deg;
	NP.coef = new double[NP.deg+1];
	for (int i = 0; i <= NP.deg; i++)
		NP.coef[i] = 0;
	for (int i = 0; i <= deg; i++) {
		for (int j = 0; j <= P.deg; j++) {
			NP.coef[i+j] += coef[i]*P.coef[j];
		}
	}
	return NP;
}

ostream& operator<<(ostream& output, const Polynomial& P) {
	output << P.coef[0];
	for (int i = 1; i <= P.deg; i++) {
		output << (P.coef[i] < 0 ? " - " : " + ");
		output << (P.coef[i] < 0 ? -P.coef[i] : P.coef[i]);
		output << "*x^" << i;
	}
	output << "\n";
	return output;
}