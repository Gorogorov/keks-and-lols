#pragma once

#include <iostream>
#include <cmath>
#include <fstream>
#include <ostream>

using namespace std;

class Polynomial {
private:
	int deg;
	double* coef;
public:
	Polynomial();
	Polynomial(int a, double* b);
	Polynomial(const Polynomial& P);
	Polynomial operator=(const Polynomial& P);
	Polynomial operator+(const Polynomial& P);
	Polynomial operator-(const Polynomial& P);
	Polynomial operator*(const Polynomial& P);
	int get_deg() const;
	double* get_coef() const;
	friend ostream& operator<<(ostream& output, const Polynomial& P);
};