#include "../include/Rational.h"

using namespace std;

int main() {
	Rational A(5, 3);
	Rational B(12, -2);
	Rational C;
	Rational T1, T2, T3, T4;
	T1 = A*B;
	T2 = A/B;
	T3 = A-C;
	T4 = A+C;
	cout << "T1:\n" << T1;
	cout << "T2:\n" << T2;
	cout << "T3:\n" << T3;
	cout << "T4:\n" << T4;
	return 0;
}