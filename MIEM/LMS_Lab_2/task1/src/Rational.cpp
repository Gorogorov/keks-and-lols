#include "../include/Rational.h"

Rational::Rational(const Rational& R) {
	p = R.p;
	q = R.q;
}

Rational Rational::operator=(const Rational& R) {
	p = R.p;
	q = R.q;
	return *this;
}

double Rational::get_q() const{
	return q;
}

double Rational::get_p() const{
	return p;
}

int Rational::gcd(int a, int b) {
	if (!b) return a;
	return gcd(b, a%b);
}

void Rational::sign(int& a, int& b) {
	if (b < 0) {
		a *= -1;
		b *= -1;
	}
}

Rational Rational::operator+(const Rational& R) {
	Rational cur(p*R.q + q*R.p, q*R.q);
	int g = gcd(cur.p, cur.q);
	cur.p /= g;
	cur.q /= g;
	return cur;
}

Rational Rational::operator-(const Rational& R) {
	Rational cur(p*R.q - q*R.p, q*R.q);
	int g = gcd(cur.p, cur.q);
	cur.p /= g;
	cur.q /= g;
	return cur;
}

Rational Rational::operator*(const Rational& R) {
	Rational cur(p*R.p, q*R.q);
	int g = gcd(cur.p, cur.q);
	cur.p /= g;
	cur.q /= g;
	return cur;
}

Rational Rational::operator/(const Rational& R) {
	Rational cur(p*R.q, q*R.p);
	int g = gcd(cur.p, cur.q);
	cur.p /= g;
	cur.q /= g;
	return cur;
}

ostream& operator<<(ostream& output, const Rational& R) {
	output << "p: " << R.get_p() << "\n";
	output << "q: " << R.get_q() << "\n";
	return output;
}