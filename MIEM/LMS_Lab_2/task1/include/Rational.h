#pragma once

#include <iostream>
#include <cmath>
#include <fstream>
#include <ostream>

using namespace std;

class Rational {
private:
	int p, q;
	int gcd(int a, int b);
	void sign(int& a, int& b);
public:
	Rational() : p(1), q(1) {}
	Rational(int a, int b) : p(a), q(b) {}
	Rational(const Rational& R);
	Rational operator=(const Rational& R);
	Rational operator+(const Rational& R);
	Rational operator-(const Rational& R);
	Rational operator*(const Rational& R);
	Rational operator/(const Rational& R);
	double get_p() const;
	double get_q() const;
	friend ostream& operator<<(ostream& output, const Rational& R);
};
