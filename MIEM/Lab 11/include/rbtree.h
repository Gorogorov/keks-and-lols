#pragma once

// Node - вершина дерева
typedef struct Node
{
  struct Node *parent, *right, *left;
  char* word;
  int len_word;
  int cnt_use;
  char color;
} Node;

// deque_node - узел дека
typedef struct deque_node
{
  struct Node *data;
  struct deque_node* next;
  struct deque_node* prev;
  int num;
} deque_node;

// deque - сам дек
typedef struct deque
{
  struct deque_node* head;
  struct deque_node* tail;
} deque;


Node* grandparent(Node *n);
Node* uncle(Node *n);
void rotate_left(Node *n);
void rotate_right(Node *n);

void insert_case1(Node *n);
void insert_case2(Node *n);
void insert_case3(Node *n);
void insert_case4(Node *n);
void insert_case5(Node *n);

Node* find_root(Node* n);
void insert_node(Node* n, Node* root);
int dfs(Node* v, int depth);
void dfs2(Node* v, int n);
void LRC(Node* v, int depth);
void LCR(Node* v, int depth);
deque_node* pop_front(deque* q);
deque_node* pop_back(deque* q);
void push_back(deque* q, deque_node* new_node);
int length_deque(deque* q);
void bfs(Node* v, int n);
int task(Node* v);
