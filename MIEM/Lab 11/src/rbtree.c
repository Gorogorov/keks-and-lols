#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "../include/rbtree.h"

// Нахождение дедушки вершины n, функция возвращает вершину, соответствующую
// дедушке, либо NULL, если его нет
Node* grandparent(Node *n)
{
  if ((n != NULL) && (n->parent != NULL))
  {
    return n->parent->parent;
  }
  else
  {
    return NULL;
  }
}

// Нахождениедяди вершины n, функция возвращает вершину, соответствующую
// дяде, либо NULL, если его нет
Node* uncle(Node *n)
{
  Node *g = grandparent(n);
  if (g == NULL)
  {
    return NULL;
  }
  if (n->parent == g->left)
  {
    return g->right;
  }
  else
  {
    return g->left;
  }
}

// Далее описаны функции, выполняющие балансировку красно-черного дерева.
// Подробнее смотрите на Википедии.

// Левый поворот
void rotate_left(Node *n)
{
  Node *pivot = n->right;
  pivot->parent = n->parent;
  if (n->parent != NULL) {
    if (n->parent->left==n)
    {    
      n->parent->left = pivot;
    }
    else
    {
      n->parent->right = pivot;
    }
  }
                    
  n->right = pivot->left;
  if (pivot->left != NULL)
  {
    pivot->left->parent = n;
  }

  n->parent = pivot;
  pivot->left = n;
}

// Правый поворот
void rotate_right(Node *n)
{
  Node *pivot = n->left;
            
  pivot->parent = n->parent;
  if (n->parent != NULL) {
    if (n->parent->left==n)
    {
      n->parent->left = pivot;
    }
    else
    {
      n->parent->right = pivot;
    }
  }

  n->left = pivot->right;
  if (pivot->right != NULL)
  {
    pivot->right->parent = n;
  }

  n->parent = pivot;
  pivot->right = n;
}

// 5 функций insert_case выполняют вставку вершины в красно-черное дерево без 
// потери инварианта
void insert_case1(Node *n)
{
  if (n->parent == NULL)
  {
    n->color = 'b';
  }
  else
  {
    insert_case2(n);
  }
}

void insert_case2(Node *n)
{
  if (n->parent->color == 'b')
  {
    return;
  }
  else
  {
    insert_case3(n);
  }
}

void insert_case3(Node *n)
{
  Node *u = uncle(n), *g;

  if ((u != NULL) && (u->color == 'r')) 
  {
    n->parent->color = 'b';
    u->color = 'b';
    g = grandparent(n);
    g->color = 'r';
    insert_case1(g);
  } 
  else 
  {
    insert_case4(n);
  }
}

void insert_case4(Node *n)
{
  Node *g = grandparent(n);

  if ((n == n->parent->right) && (n->parent == g->left)) 
  {
    rotate_left(n->parent);
    n = n->left;
  } 
  else if ((n == n->parent->left) && (n->parent == g->right)) 
  {
    rotate_right(n->parent);
    n = n->right;
  }
  insert_case5(n);
}

void insert_case5(Node *n)
{
  Node *g = grandparent(n);

  n->parent->color = 'b';
  g->color = 'r';
  if ((n == n->parent->left) && (n->parent == g->left)) 
  {
    rotate_right(g);
  } 
  else 
  {
    rotate_left(g);
  }
}

// Возвращает корень дерева. n - какая-либо вершина этого дерева
Node* find_root(Node* n)
{
  Node* root = n;
  while (root->parent != NULL)
  {
    root = root->parent;
  }
  return root;
}

// Выполняет вставку вершины n в дерево с корнем root
void insert_node(Node* n, Node* root)
{
  Node* cur_nde = root;
  // Выполняется поиск подходящего листа. Как только находим лист, 
  // прерываем цикл и вызываем функцию insert_node1.
  // Если по пути встречаем слово, совпадающее со вставляемым, не 
  // создаем новую вершины, а просто повышаем cnt_use этого слова на 1
  while (1)
  {
    if (strcmp(n->word, cur_nde->word) > 0)
    {
      if (cur_nde->right != NULL)
      {
        cur_nde = cur_nde->right;
      }
      else
      {
        cur_nde->right = n;
        n->parent = cur_nde;
        insert_case1(n);
        break;
      }
    }
    else if (strcmp(n->word, cur_nde->word) < 0)
    {
      if (cur_nde->left != NULL)
      {
        cur_nde = cur_nde->left;
      }
      else
      {
        cur_nde->left = n;
        n->parent = cur_nde;
        insert_case1(n);
        break;
      }
    }
    else
    {
      cur_nde->cnt_use++;
      break;
    }
  }
}

// Обход дерева: сначала выводится левое поддерево, затем правое, затем
// сама вершина. v - изначально корень дерева, затем, в ходе рекурсии,
// просто текущая вершина. depth - текущая глубина
void LRC(Node* v, int depth)
{
  if (v->left != NULL)
  {
    LRC(v->left, depth+1);
  }
  if (v->right != NULL)
  {
    LRC(v->right, depth+1);
  }
  printf("for %s (%d) depth = %d\n", v->word, v->cnt_use, depth);
}

// Обход дерева: сначала выводится левое поддерево, затем сама вершина,
// затем правое поддерево. v - изначально корень дерева, затем, в ходе 
// рекурсии, просто текущая вершина. depth - текущая глубина
void LCR(Node* v, int depth)
{
  if (v->left != NULL)
  {
    LCR(v->left, depth+1);
  }
  printf("for %s (%d) depth = %d\n", v->word, v->cnt_use, depth);
  if (v->right != NULL)
  {
    LCR(v->right, depth+1);
  }
}

// Рекурсивный обход в глубину. v - текущая вершина, depth - глубина
int dfs(Node* v, int depth) 
{
  printf("for %s (%d) depth = %d\n", v->word, v->cnt_use, depth);
  int rsubt = 0, lsubt = 0;
  if (v->left != NULL)
  {
    lsubt = dfs(v->left, depth+1);
  }
  else 
  {
    lsubt = 0;
  }
  if (v->right != NULL)
  {
    rsubt = dfs(v->right, depth+1);
  }
  else
  {
    rsubt = 0;
  }
  return rsubt + lsubt + 1;
}

// Обход в глубину с помощью стека (в данном случае это дек)
void dfs2(Node* v, int n)
{
  deque* q = (deque*) malloc(sizeof(deque));
  q->head = NULL;
  q->tail = NULL;
  // Добавляем первую вершину - корень дерева - в стек
  deque_node* cur_v = (deque_node*) malloc(sizeof(deque_node));
  cur_v->next = NULL;
  cur_v->prev = NULL;
  cur_v->data = v;
  cur_v->num = 0;
  push_back(q, cur_v);
  int* dist = (int*) malloc(sizeof(int)*n);
  dist[0] = 0;
  int num_v = 0;
  printf("for %s (%d) distance = %d\n", cur_v->data->word, 
          cur_v->data->cnt_use, dist[num_v]);
  while (length_deque(q) > 0) 
  {
    cur_v = pop_back(q);
    // Если есть правый ребенок, добавляем его
    if (cur_v->data->right != NULL)
    {
      deque_node* right_v = (deque_node*) malloc(sizeof(deque_node));
      right_v->next = NULL;
      right_v->prev = NULL;
      right_v->data = cur_v->data->right;
      num_v++;
      right_v->num = num_v;
      push_back(q, right_v);
      dist[num_v] = dist[cur_v->num] + 1;
      printf("for %s (%d) distance = %d\n", cur_v->data->right->word, 
              cur_v->data->right->cnt_use, dist[num_v]);
    }
    // Если есть левый ребенок, добавляем его
    if (cur_v->data->left != NULL)
    {
      deque_node* left_v = (deque_node*) malloc(sizeof(deque_node));
      left_v->next = NULL;
      left_v->prev = NULL;
      left_v->data = cur_v->data->left;
      num_v++;
      left_v->num = num_v;
      push_back(q, left_v);
      dist[num_v] = dist[cur_v->num] + 1;
      printf("for %s (%d) distance = %d\n", cur_v->data->left->word, 
              cur_v->data->left->cnt_use, dist[num_v]);
    }
  } 
}

// Извечь узел из начала дека q. Возвращает извлеченную вершину
deque_node* pop_front(deque* q)
{
  deque_node* temp = q->head;
  q->head = q->head->next;
  return temp;
}

// Извечь узел из конца дека q. Возвращает извлеченную вершину
deque_node* pop_back(deque* q)
{
  deque_node* temp = q->tail;
  if (q->tail != q->head)
  {
    q->tail = q->tail->prev;
  }
  else
  {
    q->tail = q->head = NULL;
  }
  return temp;
}

// Вставить вершину в конец дека q. new_node - вставляемая вершина
void push_back(deque* q, deque_node* new_node)
{
  if (q->head == NULL)
  {
    q->head = new_node;
    q->tail = new_node;
  }
  else
  {
    new_node->prev = q->tail;
    new_node->next = NULL;
    q->tail->next = new_node;
    q->tail = q->tail->next;
  }
}

// Возвращает длину дека q.
int length_deque(deque* q)
{
  if (q->head == NULL)
  {
    return 0;
  }
  deque_node* cur_v = q->head;
  int length = 1;
  while(cur_v->next != NULL)
  {
    length++;
    cur_v = cur_v->next;
  }
  return length;
}

// Обход в ширину с помощью очереди (в данном случае дека).
// v - корень дерева, n - количество вершин дерева
void bfs(Node* v, int n)
{
  deque* q = (deque*) malloc(sizeof(deque));
  q->head = NULL;
  q->tail = NULL;
  deque_node* cur_v = (deque_node*) malloc(sizeof(deque_node));
  cur_v->next = NULL;
  cur_v->prev = NULL;
  cur_v->data = v;
  cur_v->num = 0;
  push_back(q, cur_v);
  int* dist = (int*) malloc(sizeof(int)*n);
  dist[0] = 0;
  int num_v = 0;
  printf("for %s (%d) distance = %d\n", cur_v->data->word, 
          cur_v->data->cnt_use, dist[num_v]);
  while (length_deque(q) > 0) 
  {
    cur_v = pop_front(q);
    if (cur_v->data->right != NULL)
    {
      deque_node* right_v = (deque_node*) malloc(sizeof(deque_node));
      right_v->next = NULL;
      right_v->prev = NULL;
      right_v->data = cur_v->data->right;
      num_v++;
      right_v->num = num_v;
      push_back(q, right_v);
      dist[num_v] = dist[cur_v->num] + 1;
      printf("for %s (%d) distance = %d\n", cur_v->data->right->word, 
              cur_v->data->right->cnt_use, dist[num_v]);
    }
    if (cur_v->data->left != NULL)
    {
      deque_node* left_v = (deque_node*) malloc(sizeof(deque_node));
      left_v->next = NULL;
      left_v->prev = NULL;
      left_v->data = cur_v->data->left;
      num_v++;
      left_v->num = num_v;
      push_back(q, left_v);
      dist[num_v] = dist[cur_v->num] + 1;
      printf("for %s (%d) distance = %d\n", cur_v->data->left->word, 
              cur_v->data->left->cnt_use, dist[num_v]);
    }
  }
}

// Выводит на экран внутренние вершины, у которых согласных букв больше, 
// чем гласных
int task(Node* v)
{
  // is_need - больше ли гласных, чем согласных
  int is_need = 0;
  char* vowel_letters = "AaEeIiOoUuYy";
  char* consonant_letters = "BbCcDdFfGgHhJjKkLlMmNnPpQqRrSsTtVvWwXxZz";
  // cnt_vowel - количество гласных, cnt_consonant - количество согласных
  int cnt_vowel = 0, cnt_consonant = 0;
  for (int i = 0; i < strlen(v->word); i++)
  {
    for (int j = 0; j < strlen(vowel_letters); j++)
    {
      if (v->word[i] == vowel_letters[j])
      {
        cnt_vowel++;
        break;
      }
    }
    for (int j = 0; j < strlen(consonant_letters); j++)
    {
      if (v->word[i] == consonant_letters[j])
      {
        cnt_consonant++;
        break;
      }
    }
  }
  if (cnt_consonant > cnt_vowel)
  {
    is_need = 1;
  }
  if (is_need)
  {
    printf("%s : cnt_vowel = %d, cnt_consonant = %d\n", v->word,
            cnt_vowel, cnt_consonant);
  }
  int rsubt = 0, lsubt = 0;
  if (v->left != NULL)
  {
    lsubt = task(v->left);
  }
  else 
  {
    lsubt = 0;
  }
  if (v->right != NULL)
  {
    rsubt = task(v->right);
  }
  else
  {
    rsubt = 0;
  }
  if (is_need && (v->right != NULL || v->left != NULL))
  {
    return rsubt + lsubt + 1; 
  }
  else
  {
    return rsubt + lsubt;
  }
}

