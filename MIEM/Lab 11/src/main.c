#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "../include/rbtree.h"

int main()
{
  // Считываем текст из файла
  FILE* fr;
  if((fr=fopen("../data/lab11.txt", "r"))==NULL) {
    printf ("Невозможно открыть файл.\n");
    exit(1);
  }
  char* temp = (char*) malloc(sizeof(char)*1000);

  Node* root = NULL;
  int ind_temp = 0;
  // Заполняем дерево
  while ((temp[ind_temp] = fgetc(fr)) != EOF)
  {
    if (temp[ind_temp] == ' ' || temp[ind_temp] == '\n')
    {
      temp[ind_temp] = '\0';
      if (temp[0] == '\0')
      {
        ind_temp = 0;
        continue;
      }
      Node* new_node = (Node*) malloc(sizeof(Node));
      new_node->right = NULL;
      new_node->left = NULL;
      new_node->word = (char*) malloc(sizeof(char) * 1000);
      strcpy(new_node->word, temp);
      new_node->len_word = ind_temp;
      new_node->cnt_use = 1;
      // Если эта вершина - первая в дереве, т.е. дерево еще пусто
      if (root == NULL)
      {
        root = new_node;
        root->parent = NULL;
        insert_case1(root);
      }
      // Если дерево не пусто
      else
      {
        root = find_root(root);
        insert_node(new_node, root);
      }
      ind_temp = 0;
      continue;
    }
    ind_temp++;
  } 
  fclose(fr);
  // Находим корень
  root = find_root(root);
  printf("DFS:\n");
  // Вызываем все обходы
  int cnt_ver = dfs(root, 0);
  //printf("\n---\n\nDFS2:\n");
  //dfs2(root, cnt_ver);
  printf("\n---\n\nBFS:\n"); 
  bfs(root, cnt_ver);
  printf("\n---\n\nLRC:\n");
  LRC(root, 0);
  printf("\n---\n\nLCR:\n");
  LCR(root, 0);
  printf("\n---\n\nTask:\n");
  int cnt_need_words = task(root);
  printf("count = %d\n", cnt_need_words);
  return 0;
}
