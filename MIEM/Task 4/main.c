#include <stdio.h>
#include <stdlib.h>

int main()
{
  int N, M, K, D;
  scanf("%d %d %d %d", &N, &M, &K, &D);
  int ind = N*M;
  int* matrix = (int*) malloc(sizeof(int) * N*M);
  for (int i = 0; i < N; i++)
  {
    for (int j = 0; j < M; j++)
    {
      matrix[i*M + j] = -1;
    }
  }
  int dir = 0; // направление 0 - вверх, 1 - влево ...
  int i, j;
  if (K == 0) 
  {
    i = 0;
    j = 0;
    if (D == 0)
    {
      dir = 2;
    }
    else
    {
      dir = 3;
    }
  }
  if (K == 1) {
    i = 0;
    j = M-1;
    if (D == 0)
    {
      dir = 1;
    }
    else
    {
      dir = 2;
    }
  }
  if (K == 2) {
    i = N-1;
    j = M-1;
    if (D == 0)
    {
      dir = 0;
    }
    else
    {
      dir = 1;
    }
  }
  if (K == 3) {
    i = N-1;
    j = 0;
    if (D == 0)
    {
      dir = 3;
    }
    else
    {
      dir = 0;
    }
  }
  int NUMBER = 0;
  while (ind > 0)
  {
    matrix[i*M + j] = NUMBER;
    NUMBER++;
    if (NUMBER == 16)
      NUMBER = 0;
    ind--;
    int is_good = 0;
    while (is_good == 0)
    {
      if (ind == 0)
        break;
      if (dir == 0)
      {
        if (i > 0 && matrix[(i-1)*M + j] == -1)
        {
          i--;
          is_good = 1;
        }
        else
        {
          if (D == 0)
            dir = 1;
          else
            dir = 3;
        }
      }
      else if (dir == 1) 
      {
        if (j > 0 && matrix[i*M + j -1] == -1)
        {
          j--;
          is_good = 1;
        }
        else
        {
          if (D == 0)
            dir = 2;
          else 
            dir = 0;
        }
      }
      else if (dir == 2)
      {
        if (i < N-1 && matrix[(i+1)*M + j] == -1)
        {
          i++;
          is_good = 1;
        }
        else
        {
          if (D==0)
            dir = 3;
          else 
            dir = 1;
        }
      }
      else if (dir == 3)
      {
        if (j < M-1 && matrix[i*M + j + 1] == -1)
        {
          j++;
          is_good = 1;
        }
        else
        {
          if (D==0)
            dir = 0;
          else
            dir = 2;
        }
      }
    }
  }
  for (int i = 0; i < N; i++)
  {
    for (int j = 0; j < M; j++)
    {
      printf("%X ", matrix[i*M + j]);
    }
    printf("\n");
  }
  return 0;
}
