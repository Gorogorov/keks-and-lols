#include "../include/edit_database.h"

// Вычисляет длину базы данных, которая расположена по пути
// "../data/lab8_data_base"
int length_data_base() 
{
  FILE* fr;
  if((fr=fopen("../data/lab8_data_base", "r"))==NULL) 
  {
    printf ("Невозможно открыть файл.\n");
    exit(1);
  }
  int length = 0;
  ssize_t read = 0;
  size_t len = 0;
  char* line = NULL;
  while ((read = getline(&line, &len, fr)) != -1)
  {
    length++;
  }

  fclose(fr);
  return length;
}


// Добавляет новую команду, данные которой введены пользователем, 
// в файл с потоком fw
void new_team(FILE* fw) 
{
  char* temp_str = (char*) malloc(sizeof(char) * 100);

  // Записываем название команды
  printf("Введите имя команды: ");
  fgets(temp_str, 100, stdin);
  char a = temp_str[0];
  int ind_in_str = 0;
  while (a != '\n')
  {
    fprintf(fw, "%c", a);
    ind_in_str++;
    a = temp_str[ind_in_str];
  }
  fprintf(fw, ",");

  // Записываем город команды
  printf("Введите город команды: ");
  fgets(temp_str, 100, stdin);
  a = temp_str[0];
  ind_in_str = 0;
  while (a != '\n') 
  {
    fprintf(fw, "%c", a);
    ind_in_str++;
    a = temp_str[ind_in_str];
  }
  fprintf(fw, ",");

  // Записываем количество очков команды
  printf("Введите количество очков команды: ");
  fgets(temp_str, 100, stdin);
  a = temp_str[0];
  ind_in_str = 0;
  while (a != '\n') 
  {
    fprintf(fw, "%c", a);
    ind_in_str++;
    a = temp_str[ind_in_str];
  }
  fprintf(fw, ",");

  // Введите разницу забитых и пропущенных шайб
  printf("Введите разницу забитых и пропущенных шайб: ");
  fgets(temp_str, 100, stdin);
  a = temp_str[0];
  ind_in_str = 0;
  while (a != '\n') 
  {
    fprintf(fw, "%c", a);
    ind_in_str++;
    a = temp_str[ind_in_str];
  }
  fprintf(fw, ",");

  // Введите имя тренера
  printf("Введите имя тренера: ");
  fgets(temp_str, 100, stdin);
  a = temp_str[0];
  ind_in_str = 0;
  while (a != '\n') 
  {
    fprintf(fw, "%c", a);
    ind_in_str++;
    a = temp_str[ind_in_str];
  }
  fprintf(fw, "\n");
}

// Добавляет cnt_cmd команд в базу данных
void write_data_from_keyboard(int cnt_cmd) 
{
  FILE* fw;
  if((fw=fopen("../data/lab8_data_base", "w"))==NULL) 
  {
    printf ("Невозможно открыть файл.\n");
    exit(1);
  }
  while (getchar() == ' ') {};
  for (int i = 0; i < cnt_cmd; i++)
  {
    printf("Данные %d-ой команды:\n", i+1);

    new_team(fw);
  }
  
  fclose(fw);  
}


// Добавляет команду в базу данных
void add_new_team()
{
  FILE* fa;
  if((fa=fopen("../data/lab8_data_base", "a"))==NULL) 
  {
    printf ("Невозможно открыть файл.\n");
    exit(1);
  }
  printf("Введите данные для новой команды:\n");
  while (getchar() == ' ') {};
  new_team(fa);
  
  fclose(fa);
}


// Удаляет команду из базы данных. Можно удалять либо по полной 
// информации о команде, либо по номеру команды в базе данных.
// cnt_teams - количество команд в базе данных
void del_team(int cnt_teams)
{
  int action;
  team* tm = (team*) malloc(sizeof(team) * cnt_teams);
  for (int i = 0; i < cnt_teams; i++)
  {
    tm[i].name = malloc(sizeof(char) * 100);
    tm[i].city = malloc(sizeof(char) * 100);
    tm[i].coach_name = malloc(sizeof(char) * 100);
  }
  printf("Удалить по записи (1) или по номеру, начиная с единицы (2)?\n");
  scanf("%d", &action);
  // Удаляем по полной информации о команде
  if (action == 1)
  {
    team search_team;
    search_team.name = (char*) malloc(sizeof(char) * 100);
    search_team.city = (char*) malloc(sizeof(char) * 100);
    search_team.coach_name = (char*) malloc(sizeof(char) * 100);

    // Считываем данные о команде
    while (getchar() == ' ') {};
    printf("Введите имя команды: ");
    fgets(search_team.name, 100, stdin);
    search_team.name[strlen(search_team.name)-1] = '\0';

    printf("Введите город команды: ");
    fgets(search_team.city, 100, stdin);
    search_team.city[strlen(search_team.city)-1] = '\0';

    printf("Введите количество очков команды: ");
    scanf("%d", &search_team.points);

    printf("Введите разницу забитых и пропущенных шайб: ");
    scanf("%d", &search_team.diff_washers);

    while (getchar() == ' ') {};
    printf("Введите имя тренера: ");
    fgets(search_team.coach_name, 100, stdin);
    search_team.coach_name[strlen(search_team.coach_name)-1] = '\0';

    FILE* fr;
    if((fr=fopen("../data/lab8_data_base", "r"))==NULL) 
    {
      printf ("Невозможно открыть файл.\n");
      exit(1);
    }

    size_t len = 0;
    char* line = NULL;
    int del_ind = -1;
    // Идем циклом по базе данных. Если нашли команду, которая
    // соответствует введенной пользователем, то запоминаем ее
    // индекс в del_ind
    for (int tm_ind = 0; tm_ind < cnt_teams; tm_ind++)
    {
      getline(&line, &len, fr);
      int ind_in_line = 0;
      int ind_in_str = 0;
      while (line[ind_in_line] != ',')
      {
        tm[tm_ind].name[ind_in_str] = line[ind_in_line];
        ind_in_line++;
        ind_in_str++;
      }
      tm[tm_ind].name[ind_in_str] = '\0';
      ind_in_line++;
      ind_in_str = 0;
      while (line[ind_in_line] != ',') 
      {
        tm[tm_ind].city[ind_in_str] = line[ind_in_line];
        ind_in_line++;
        ind_in_str++;
      }
      tm[tm_ind].city[ind_in_str] = '\0';
      ind_in_line++;
      char * number = (char*) malloc(sizeof(char)*100);
      ind_in_str = 0;
      while (line[ind_in_line] != ',') 
      {
        number[ind_in_str] = line[ind_in_line];
        ind_in_line++;
        ind_in_str++;
      }
      number[ind_in_str] = '\0';
      char* end;
      tm[tm_ind].points = strtol(number, &end, 10);
      ind_in_line++;
      ind_in_str = 0;
      while (line[ind_in_line] != ',') 
      {
        number[ind_in_str] = line[ind_in_line];
        ind_in_line++;
        ind_in_str++;
      }
      number[ind_in_str] = '\0';
      tm[tm_ind].diff_washers = strtol(number, &end, 10);
      ind_in_line++;
      ind_in_str = 0;
      while (line[ind_in_line] != '\n') 
      {
        tm[tm_ind].coach_name[ind_in_str] = line[ind_in_line];
        ind_in_line++;
        ind_in_str++;
      }
      tm[tm_ind].coach_name[ind_in_str] = '\0';
      // Сравниваем считанную из базы данных команду и 
      // удаляемую
      if (!strcmp(tm[tm_ind].name, search_team.name) && 
          !strcmp(tm[tm_ind].city, search_team.city) && 
          tm[tm_ind].points == search_team.points && 
          tm[tm_ind].diff_washers == search_team.diff_washers &&
          !strcmp(tm[tm_ind].coach_name, search_team.coach_name))
      {
        del_ind = tm_ind;
      }
    }
    // Если совпадений не нашлось, то уведомляем об этом пользователя
    if (del_ind == -1)
    {
      printf("Такой записи не найдено\n");
    }
    // Если совпадение нашлось, то перезаписываем в базу данных все
    // команды, кроме команды с индексом del_ind
    else
    {
      FILE* fw;
      if((fw=fopen("../data/lab8_data_base", "w"))==NULL) 
      {
        printf ("Невозможно открыть файл.\n");
        exit(1);
      }
    
      for (int i = 0; i < cnt_teams; i++)
      {
        if (del_ind != i) 
        {
          fprintf(fw, "%s,%s,%d,%d,%s\n", tm[i].name, tm[i].city, \
                  tm[i].points, tm[i].diff_washers, tm[i].coach_name);
        }
      }
      fclose(fw);
    }
  }
  // Удаляем команду по ее номеру
  else if (action == 2)
  {
    printf("Введите индекс:\n");
    int db_ind;
    scanf("%d", &db_ind);
    FILE* fr;
    if((fr=fopen("../data/lab8_data_base", "r"))==NULL) 
    {
      printf ("Невозможно открыть файл.\n");
      exit(1);
    }

    size_t len = 0;
    char* line = NULL;
    int del_ind = -1;
    for (int tm_ind = 0; tm_ind < cnt_teams; tm_ind++)
    {
      getline(&line, &len, fr);
      int ind_in_line = 0;
      int ind_in_str = 0;
      while (line[ind_in_line] != ',')
      {
        tm[tm_ind].name[ind_in_str] = line[ind_in_line];
        ind_in_line++;
        ind_in_str++;
      }
      tm[tm_ind].name[ind_in_str] = '\0';
      ind_in_line++;
      ind_in_str = 0;
      while (line[ind_in_line] != ',') 
      {
        tm[tm_ind].city[ind_in_str] = line[ind_in_line];
        ind_in_line++;
        ind_in_str++;
      }
      tm[tm_ind].city[ind_in_str] = '\0';
      ind_in_line++;
      char * number = (char*) malloc(sizeof(char)*100);
      ind_in_str = 0;
      while (line[ind_in_line] != ',') 
      {
        number[ind_in_str] = line[ind_in_line];
        ind_in_line++;
        ind_in_str++;
      }
      number[ind_in_str] = '\0';
      char* end;
      tm[tm_ind].points = strtol(number, &end, 10);
      ind_in_line++;
      ind_in_str = 0;
      while (line[ind_in_line] != ',') 
      {
        number[ind_in_str] = line[ind_in_line];
        ind_in_line++;
        ind_in_str++;
      }
      number[ind_in_str] = '\0';
      tm[tm_ind].diff_washers = strtol(number, &end, 10);
      ind_in_line++;
      ind_in_str = 0;
      while (line[ind_in_line] != '\n') 
      {
        tm[tm_ind].coach_name[ind_in_str] = line[ind_in_line];
        ind_in_line++;
        ind_in_str++;
      }
      tm[tm_ind].coach_name[ind_in_str] = '\0';
      if (db_ind == tm_ind+1)
      {
        del_ind = tm_ind;
      }
    }
    // Совпадений не найдено, если команда с номером, введенным 
    // пользователем, не существует
    if (del_ind == -1)
    {
      printf("Такой записи не найдено\n");
    }
    // Если команда с таким номером нашлась, то удаляем ее
    else
    {
      FILE* fw;
      if((fw=fopen("../data/lab8_data_base", "w"))==NULL) 
      {
        printf ("Невозможно открыть файл.\n");
        exit(1);
      }
    
      for (int i = 0; i < cnt_teams; i++)
      {
        if (del_ind != i) 
        {
          fprintf(fw, "%s,%s,%d,%d,%s\n", tm[i].name, tm[i].city, \
                  tm[i].points, tm[i].diff_washers, tm[i].coach_name);
        }
      }
      fclose(fw);
    }
    fclose(fr);
  }
}


// Редактирование команды по запросу пользователя
void edit_team(int cnt_teams)
{
  team* tm = (team*) malloc(sizeof(team) * cnt_teams);
  for (int i = 0; i < cnt_teams; i++)
  {
    tm[i].name = malloc(sizeof(char) * 100);
    tm[i].city = malloc(sizeof(char) * 100);
    tm[i].coach_name = malloc(sizeof(char) * 100);
  }
  team search_team;
  search_team.name = (char*) malloc(sizeof(char) * 100);
  search_team.city = (char*) malloc(sizeof(char) * 100);
  search_team.coach_name = (char*) malloc(sizeof(char) * 100);

  team new_team;
  new_team.name = (char*) malloc(sizeof(char) * 100);
  new_team.city = (char*) malloc(sizeof(char) * 100);
  new_team.coach_name = (char*) malloc(sizeof(char) * 100);

  // Считываем данные о команде, которую нужно отредактировать
  while (getchar() == ' ') {};
  printf("Введите имя искомой команды: ");
  fgets(search_team.name, 100, stdin);
  search_team.name[strlen(search_team.name)-1] = '\0';

  printf("Введите город искомой команды: ");
  fgets(search_team.city, 100, stdin);
  search_team.city[strlen(search_team.city)-1] = '\0';

  printf("Введите количество очков искомой команды: ");
  scanf("%d", &search_team.points);

  printf("Введите разницу забитых и пропущенных шайб искомой команды: ");
  scanf("%d", &search_team.diff_washers);

  while (getchar() == ' ') {};
  printf("Введите имя тренера искомой команды: ");
  fgets(search_team.coach_name, 100, stdin);
  search_team.coach_name[strlen(search_team.coach_name)-1] = '\0';

  // Считываем данные, которыми нужно заменить найденную команду
  printf("Введите имя новой команды: ");
  fgets(new_team.name, 100, stdin);
  new_team.name[strlen(new_team.name)-1] = '\0';

  printf("Введите город новой команды: ");
  fgets(new_team.city, 100, stdin);
  new_team.city[strlen(new_team.city)-1] = '\0';

  printf("Введите количество очков новой команды: ");
  scanf("%d", &new_team.points);

  printf("Введите разницу забитых и пропущенных шайб новой команды: ");
  scanf("%d", &new_team.diff_washers);

  while (getchar() == ' ') {};
  printf("Введите имя тренера новой команды: ");
  fgets(new_team.coach_name, 100, stdin);
  new_team.coach_name[strlen(new_team.coach_name)-1] = '\0';

  FILE* fr;
  if((fr=fopen("../data/lab8_data_base", "r"))==NULL) 
  {
    printf ("Невозможно открыть файл.\n");
    exit(1);
  }

  size_t len = 0;
  char* line = NULL;
  int del_ind = -1;
  // Выполняем поиск команды, которую нужно отредактировать
  for (int tm_ind = 0; tm_ind < cnt_teams; tm_ind++)
  {
    getline(&line, &len, fr);
    int ind_in_line = 0;
    int ind_in_str = 0;
    while (line[ind_in_line] != ',')
    {
      tm[tm_ind].name[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    tm[tm_ind].name[ind_in_str] = '\0';
    ind_in_line++;
    ind_in_str = 0;
    while (line[ind_in_line] != ',') 
    {
      tm[tm_ind].city[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    tm[tm_ind].city[ind_in_str] = '\0';
    ind_in_line++;
    char * number = (char*) malloc(sizeof(char)*100);
    ind_in_str = 0;
    while (line[ind_in_line] != ',') 
    {
      number[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    number[ind_in_str] = '\0';
    char* end;
    tm[tm_ind].points = strtol(number, &end, 10);
    ind_in_line++;
    ind_in_str = 0;
    while (line[ind_in_line] != ',') 
    {
      number[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    number[ind_in_str] = '\0';
    tm[tm_ind].diff_washers = strtol(number, &end, 10);
    ind_in_line++;
    ind_in_str = 0;
    while (line[ind_in_line] != '\n') 
    {
      tm[tm_ind].coach_name[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    tm[tm_ind].coach_name[ind_in_str] = '\0';
    // Сравниваем команду, считанную из базы данных, с данными о
    // команде, которые ввел пользователь
    if (!strcmp(tm[tm_ind].name, search_team.name) && 
        !strcmp(tm[tm_ind].city, search_team.city) && 
        tm[tm_ind].points == search_team.points && 
        tm[tm_ind].diff_washers == search_team.diff_washers &&
        !strcmp(tm[tm_ind].coach_name, search_team.coach_name))
    {
      del_ind = tm_ind;
    }
  }
  fclose(fr);
  // Если нужно команды не нашлось, то уведомляем об этом пользователя
  if (del_ind == -1)
  {
    printf("Такой записи не найдено\n");
  }
  // Иначе заменяем старые данные о команде новыми
  else
  {
    FILE* fw;
    if((fw=fopen("../data/lab8_data_base", "w"))==NULL) 
    {
      printf ("Невозможно открыть файл.\n");
      exit(1);
    }
    
    for (int i = 0; i < cnt_teams; i++)
    {
      if (del_ind != i) 
      {
        fprintf(fw, "%s,%s,%d,%d,%s\n", tm[i].name, tm[i].city, \
                tm[i].points, tm[i].diff_washers, tm[i].coach_name);
      }
      else
      {
        fprintf(fw, "%s,%s,%d,%d,%s\n", new_team.name, new_team.city, \
                new_team.points, new_team.diff_washers, new_team.coach_name);
      }
    }
    fclose(fw);
  }
}


// Функция меняет местами команды с индексами ind1 и ind2 в массиве tm
void team_swap(team* tm, int ind1, int ind2)
{
  team tmp_team;
  tmp_team.name = (char*) malloc(sizeof(char) * 100);
  tmp_team.city = (char*) malloc(sizeof(char) * 100);
  tmp_team.coach_name = (char*) malloc(sizeof(char) * 100);

  strcpy(tmp_team.name, tm[ind1].name);
  strcpy(tmp_team.city, tm[ind1].city);
  tmp_team.points = tm[ind1].points;
  tmp_team.diff_washers = tm[ind1].diff_washers;
  strcpy(tmp_team.coach_name, tm[ind1].coach_name);

  strcpy(tm[ind1].name, tm[ind2].name);
  strcpy(tm[ind1].city, tm[ind2].city);
  tm[ind1].points = tm[ind2].points;
  tm[ind1].diff_washers = tm[ind2].diff_washers;
  strcpy(tm[ind1].coach_name, tm[ind2].coach_name);

  strcpy(tm[ind2].name, tmp_team.name);
  strcpy(tm[ind2].city, tmp_team.city);
  tm[ind2].points = tmp_team.points;
  tm[ind2].diff_washers = tmp_team.diff_washers;
  strcpy(tm[ind2].coach_name, tmp_team.coach_name);
}


// Функция для выполнения заданного в qsort-е инварианта
// (подробнее смотреть на Википедии)
// pole - номер поля, по которому будем сортировать
int partition(team* tm, int l, int r, int pole)
{
  team t;
  t.name = (char*) malloc(sizeof(char) * 100);
  t.city = (char*) malloc(sizeof(char) * 100);
  t.coach_name = (char*) malloc(sizeof(char) * 100);
  strcpy(t.name, tm[(l+r)/2].name);
  strcpy(t.city, tm[(l+r)/2].city);
  t.points = tm[(l+r)/2].points;
  t.diff_washers = tm[(l+r)/2].diff_washers;
  strcpy(t.coach_name, tm[(l+r) / 2].coach_name);

  int i = l;
  int j = r;
  while (i <= j)
  {
    // Условная конструкция для выбора поля, по которому нужно сортировать
    if (pole == 1)
    {
      while (strcmp(tm[i].name, t.name) < 0)
      {
        i++;
      }
      while (strcmp(tm[j].name, t.name) > 0)
      {
        j--;
      }
    }
    if (pole == 2)
    {
      while (strcmp(tm[i].city, t.city) < 0)
      {
        i++;
      }
      while (strcmp(tm[j].city, t.city) > 0)
      {
        j--;
      }
    }
    if (pole == 3)
    {
      while (tm[i].points < t.points)
      {
        i++;
      }
      while (tm[j].points > t.points)
      {
        j--;
      }
    }
    if (pole == 4)
    {
      while (tm[i].diff_washers < t.diff_washers)
      {
        i++;
      }
      while (tm[j].diff_washers > t.diff_washers)
      {
        j--;
      }
    }
    if (pole == 5)
    {
      while (strcmp(tm[i].coach_name, t.coach_name) < 0)
      {
        i++;
      }
      while (strcmp(tm[j].coach_name, t.coach_name) > 0)
      {
        j--;
      }
    }
    if (i >= j)
    {
      break;
    }
    team_swap(tm, i, j);
    i++;
    j--;
  }
  return j;
}


// Функция выполняет сортировку tm по полю pole
// Подробнее о quick sort на Википедии
void quicksort(team* tm, int l, int r, int pole)
{
  if (l < r)
  {
    int q = partition(tm, l, r, pole); 
    quicksort(tm, l, q, pole);
    quicksort(tm, q + 1, r, pole);
  }
}


// Функция, выполняющая создание массива для сортировки и выполняющая 
// вызов quicksort по полю pole
void sort_team(int cnt_teams, int pole)
{
  team* tm = (team*) malloc(sizeof(team) * cnt_teams);
  for (int i = 0; i < cnt_teams; i++)
  {
    tm[i].name = malloc(sizeof(char) * 100);
    tm[i].city = malloc(sizeof(char) * 100);
    tm[i].coach_name = malloc(sizeof(char) * 100);
  }

  FILE* fr;
  if((fr=fopen("../data/lab8_data_base", "r"))==NULL) 
  {
    printf ("Невозможно открыть файл.\n");
    exit(1);
  }

  size_t len = 0;
  char* line = NULL;
  for (int tm_ind = 0; tm_ind < cnt_teams; tm_ind++)
  {
    getline(&line, &len, fr);
    int ind_in_line = 0;
    int ind_in_str = 0;
    // Формирование массива для сортировки
    while (line[ind_in_line] != ',')
    {
      tm[tm_ind].name[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    tm[tm_ind].name[ind_in_str] = '\0';
    ind_in_line++;
    ind_in_str = 0;
    while (line[ind_in_line] != ',') 
    {
      tm[tm_ind].city[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    tm[tm_ind].city[ind_in_str] = '\0';
    ind_in_line++;
    char * number = (char*) malloc(sizeof(char)*100);
    ind_in_str = 0;
    while (line[ind_in_line] != ',') 
    {
      number[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    number[ind_in_str] = '\0';
    char* end;
    tm[tm_ind].points = strtol(number, &end, 10);
    ind_in_line++;
    ind_in_str = 0;
    while (line[ind_in_line] != ',') 
    {
      number[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    number[ind_in_str] = '\0';
    tm[tm_ind].diff_washers = strtol(number, &end, 10);
    ind_in_line++;
    ind_in_str = 0;
    while (line[ind_in_line] != '\n') 
    {
      tm[tm_ind].coach_name[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    tm[tm_ind].coach_name[ind_in_str] = '\0';
  }
  fclose(fr);

  // После данного вызова tm отсортиован по полю pole 
  quicksort(tm, 0, cnt_teams-1, pole);

  FILE* fw;
  if((fw=fopen("../data/lab8_data_base", "w"))==NULL) 
  {
    printf ("Невозможно открыть файл.\n");
    exit(1);
  }

  // Записываем отсортированный массив обратно в базу данных
  for (int i = 0; i < cnt_teams; i++)
  {
  fprintf(fw, "%s,%s,%d,%d,%s\n", tm[i].name, tm[i].city, \
          tm[i].points, tm[i].diff_washers, tm[i].coach_name);
  }
  fclose(fw);
}

