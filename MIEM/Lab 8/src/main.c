#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/edit_database.h"
#include "../include/remain_database.h"

int main()
{
  printf("Выберите действие, которое хотите совершить:\n");
  printf("1. Заполнение базы данных\n");
  printf("2. Добавление записи в базу\n");
  printf("3. Удаление заданной записи из базы\n");
  printf("4. Поиск записи по заданному полю\n");
  printf("5. Редактирование заданной записи в базе\n");
  printf("6. Сортировка данных в базе по заданному полю\n");
  printf("7. Вывод на экран содержимого базы в табличном виде\n");
  printf("8. Выход из программы\n");
  // cnt_teams - количество хоккейных комманд, которые в данный момент находятся в базе данных
  int cnt_teams = 0;
  // action - номер действия, которое нужно выполнить
  int action;
  while (1) 
  {
    // length_data_base() - вычисляет количество структур в базе данных
    cnt_teams = length_data_base();
    scanf("%d", &action);
    if (action > 8 || action < 1)
    {
      printf("Введеное число не лежит в пределах от 1 до 8\n");
      continue;
    }
    if (action == 1) 
    {
      printf("Введите количество команд:\n");
      // cnt_cmd - количество комманд, которые составят новую базу данных
      int cnt_cmd;
      scanf("%d", &cnt_cmd);
      // write_data_from_keyboard() - заполняет базу данных информацией, введенной пользователем
      write_data_from_keyboard(cnt_cmd);
    }
    if (action == 2)
    {
      // add_new_team() - добавляет новую команду
      add_new_team();
    }
    if (action == 3)
    {
      // del_team() - удаляет конкретную команду
      del_team(cnt_teams);
    }
    if (action == 4) 
    {
      printf("Введите поле, по которому вы хотите искать совпадения:\n");
      printf("1 - название команды;\n");
      printf("2 - название города;\n");
      printf("3 - количество очков;\n");
      printf("4 - разница шайб;\n");
      printf("5 - имя тренера;\n"); 
      // act - введенное поле
      int act;
      scanf("%d", &act);
      // Если поле это строка
      if (act == 1 || act == 2 || act == 5)
      {
        printf("Введите поле:\n");
        char* name = (char*) malloc(sizeof(char) * 100);
        // scanf оставляет пробелы в буффере
        while (getchar() == ' ') {};
        fgets(name, 100, stdin);
        name[strlen(name)-1] = '\0';
        // search_team_f() - поиск команды по заданному полю и вывод ее на экран
        search_team_f(name, 0, act);
        free(name);
      }
      // Если поле это число
      else if (act == 3 || act == 4)
      {
        char* name = NULL;
        // input_num - номер поля
        int input_num;
        printf("Введите поле:\n");
        while (getchar() == ' ') {};
        scanf("%d", &input_num);
        search_team_f(name, input_num, act);
        free(name);
      }
    }
    if (action == 5)
    {
      // edit_team() - функция редактирования записи
      edit_team(cnt_teams);
    }
    if (action == 6)
    {
      printf("Введите поле, по которому вы хотите сортировать:\n");
      printf("1 - название команды;\n");
      printf("2 - название города;\n");
      printf("3 - количество очков;\n");
      printf("4 - разница шайб;\n");
      printf("5 - имя тренера;\n");
      int act;
      scanf("%d", &act);
      // sort_team() - сортировка записей по заданному полю
      sort_team(cnt_teams, act);
    }
    if (action == 7)
    {
      // show_data_base - показать всю базу данных
      show_data_base(cnt_teams);
    }
    if (action == 8) 
    {
      return 0;
    }
    printf("Введите дальнейшую команду:\n");
  }
  return 0;
}
