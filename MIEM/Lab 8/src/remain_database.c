#include "../include/remain_database.h"

// Выводит все совпадения по заданному пользователем номеру поля act
// team_name - если пользователь выбрал поле со строками, то team_name -
// строка, которую ввел пользователь
// team_number - если пользователь выбрал поле с цифрами, то team_number - 
// число, которое ввел пользователь
void search_team_f(char* team_name, int team_number, int act)
{
  FILE* fr;
  if((fr=fopen("../data/lab8_data_base", "r"))==NULL) 
  {
    printf ("Невозможно открыть файл.\n");
    exit(1);
  }

  team* tm = (team*) malloc(sizeof(team));
  tm[0].name = (char*) malloc(sizeof(char) * 100);
  tm[0].city = (char*) malloc(sizeof(char) * 100);
  tm[0].coach_name = (char*) malloc(sizeof(char) * 100);

  ssize_t read = 0;
  size_t len = 0;
  char* line = NULL;
  // Проходим по базе данных. Если есть совпадение, то вывод данные
  // о команде на экран
  while ((read = getline(&line, &len, fr)) != -1) 
  {
    int ind_in_line = 0;
    int ind_in_str = 0;
    while (line[ind_in_line] != ',') 
    {
      tm[0].name[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    tm[0].name[ind_in_str] = '\0';
    ind_in_line++;
    ind_in_str = 0;
    while (line[ind_in_line] != ',') 
    {
      tm[0].city[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    tm[0].city[ind_in_str] = '\0';
    ind_in_line++;
    char * number = (char*) malloc(sizeof(char)*100);
    ind_in_str = 0;
    while (line[ind_in_line] != ',') 
    {
      number[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    number[ind_in_str] = '\0';
    char* end;
    tm[0].points = strtol(number, &end, 10);
    ind_in_line++;
    ind_in_str = 0;
    while (line[ind_in_line] != ',') 
    {
      number[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    number[ind_in_str] = '\0';
    tm[0].diff_washers = strtol(number, &end, 10);
    ind_in_line++;
    ind_in_str = 0;
    while (line[ind_in_line] != '\n') 
    {
      tm[0].coach_name[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    tm[0].coach_name[ind_in_str] = '\0';
    if (act == 1 && !strcmp(tm[0].name, team_name) || act == 2 && !strcmp(tm[0].city, team_name) ||
        act == 3 && tm[0].points == team_number || act == 4 && tm[0].diff_washers == team_number ||
        act == 5 && !strcmp(tm[0].coach_name, team_name))
    {
      printf("Искомая запись:\n");
      printf("Название команды: %s\n", tm[0].name);
      printf("Город: %s\n", tm[0].city);
      printf("Количество очков: %d\n", tm[0].points);
      printf("Разница шайб: %d\n", tm[0].diff_washers);
      printf("Имя тренера: %s\n", tm[0].coach_name);
    }
  }
  fclose(fr);
}


// Выводит на экран все команды из базы данных
// cnt_teams - количество команд в базе
void show_data_base(int cnt_teams)
{
  char* temp_str;
  // Выводим шапку для таблицы
  temp_str = "Name";
  printf("%25s", temp_str);
  temp_str = "City";
  printf("%25s", temp_str);
  temp_str = "Points";
  printf("%25s", temp_str);
  temp_str = "Diff. washers";
  printf("%25s", temp_str);
  temp_str = "Coach name";
  printf("%25s\n", temp_str);

  team cur_team;
  cur_team.name = (char*) malloc(sizeof(char) * 100);
  cur_team.city = (char*) malloc(sizeof(char) * 100);
  cur_team.coach_name = (char*) malloc(sizeof(char) * 100);

  FILE* fr;
  if((fr=fopen("../data/lab8_data_base", "r"))==NULL) 
  {
    printf ("Невозможно открыть файл.\n");
    exit(1);
  }

  size_t len = 0;
  char* line = NULL;
  for (int tm_ind = 0; tm_ind < cnt_teams; tm_ind++)
  {
    getline(&line, &len, fr);
    int ind_in_line = 0;
    int ind_in_str = 0;
    while (line[ind_in_line] != ',')
    {
      cur_team.name[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    cur_team.name[ind_in_str] = '\0';
    ind_in_line++;
    ind_in_str = 0;
    while (line[ind_in_line] != ',') 
    {
      cur_team.city[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    cur_team.city[ind_in_str] = '\0';
    ind_in_line++;
    char * number = (char*) malloc(sizeof(char)*100);
    ind_in_str = 0;
    while (line[ind_in_line] != ',') 
    {
      number[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    number[ind_in_str] = '\0';
    char* end;
    cur_team.points = strtol(number, &end, 10);
    ind_in_line++;
    ind_in_str = 0;
    while (line[ind_in_line] != ',') 
    {
      number[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    number[ind_in_str] = '\0';
    cur_team.diff_washers = strtol(number, &end, 10);
    ind_in_line++;
    ind_in_str = 0;
    while (line[ind_in_line] != '\n') 
    {
      cur_team.coach_name[ind_in_str] = line[ind_in_line];
      ind_in_line++;
      ind_in_str++;
    }
    cur_team.coach_name[ind_in_str] = '\0';

    printf("%25s%25s%25d%25d%25s\n", cur_team.name, cur_team.city, cur_team.points, \
                                     cur_team.diff_washers, cur_team.coach_name);
  }
  fclose(fr);
  printf("\n");
}

