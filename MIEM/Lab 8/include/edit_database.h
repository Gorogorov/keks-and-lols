#ifndef __EDIT_DATABASE_H__
#define __EDIT_DATABASE_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/team.h"

int length_data_base();
void new_team(FILE* fw);
void write_data_from_keyboard(int cnt_cmd);
void add_new_team();
void del_team(int cnt_teams);
void edit_team(int cnt_teams);
void team_swap(team* tm, int ind1, int ind2);
int partition(team* tm, int l, int r, int pole);
void quicksort(team* tm, int l, int r, int pole);
void sort_team(int cnt_teams, int pole); 

#endif
