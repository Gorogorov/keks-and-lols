#ifndef __TEAM_H__
#define __TEAM_H__

typedef struct team 
{
  char* name;
  char* city;
  int points;
  int diff_washers;
  char* coach_name;
} team;

#endif
