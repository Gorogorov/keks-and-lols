#ifndef __REMAIN_DATABASE_H__
#define __REMAIN_DATABASE_H__

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/team.h"

void search_team_f(char* team_name, int team_number, int act);
void show_data_base(int cnt_teams);

#endif
