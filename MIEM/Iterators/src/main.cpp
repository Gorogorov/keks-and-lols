#include "../include/queue.h"
#include "../include/linked_list.h"
#include <iostream>

int main(){
	std::cout << "linked_list.h test\n";
	linked_list<double> LL;
	LL.push_back(1);
	LL.push_front(0);
	std::cout << LL.front() << " " << LL.back() << "\n";
	LL.pop_front();
	std::cout << LL.front() << "\n";
	std::cout << LL.size() << " " << LL.empty() << "\n";
	LL.pop_front();
	for(int i = 0; i < 10; i++) {
		LL.push_back(i);
	}
	linked_list<double>::iterator it_ll;
	for (it_ll = LL.begin(); it_ll != LL.end(); it_ll++) {
		std::cout << *it_ll << " ";
	}
	std::cout << "\n";
	LL.insert(LL.begin(), -1);
	for (it_ll = LL.begin(); it_ll != LL.end(); it_ll++) {
		std::cout << *it_ll << " ";
	}
	std::cout << "\n";
	LL.clear();
	std::cout << LL.size() << " " << LL.empty() << "\n";
	LL.push_back(000);

	std::cout << "--------\n";

	std::cout << "queue.h tests\n";
	queue<double> Q;
	for (int i = 0; i < 10; i++) {
		Q.push(i + 0.1*i);
	}

	std::cout << Q.front() << " " << Q.back() << "\n";
	std::cout << Q.size() << "\n";
	Q.pop();
	std::cout << Q.front() << " " << Q.back() << "\n";
	std::cout << Q.size() << "\n";
	queue<double>::iterator it;
	for (it = Q.begin(); it != Q.end(); it++){
		std::cout << *it << " ";
	}
	std::cout << "\n";
	return 0;
}

/*
==22534== Memcheck, a memory error detector
==22534== Copyright (C) 2002-2017, and GNU GPL'd, by Julian Seward et al.
==22534== Using Valgrind-3.14.0 and LibVEX; rerun with -h for copyright info
==22534== Command: ./main
==22534== 
linked_list.h test
0 1
1
1 0
0 1 2 3 4 5 6 7 8 9 
0 -1 1 2 3 4 5 6 7 8 9 
0 1
--------
queue.h tests
0 9.9
10
1.1 9.9
9
1.1 2.2 3.3 4.4 5.5 6.6 7.7 8.8 9.9 
==22534== 
==22534== HEAP SUMMARY:
==22534==     in use at exit: 0 bytes in 0 blocks
==22534==   total heap usage: 26 allocs, 26 frees, 74,112 bytes allocated
==22534== 
==22534== All heap blocks were freed -- no leaks are possible
==22534== 
==22534== For counts of detected and suppressed errors, rerun with: -v
==22534== ERROR SUMMARY: 0 errors from 0 contexts (suppressed: 0 from 0)
*/