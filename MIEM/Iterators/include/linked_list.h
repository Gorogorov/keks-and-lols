#pragma once

#include <cassert>
#include <iostream>

template <typename T>
struct Node{
	T data;
	Node *next;
};

template <class T>
class linked_list {
private:
	Node<T> *head, *tail;
	int length;
public:
	linked_list();
	~linked_list();
	void push_back(const T& elem);
	void push_front(const T& elem);
	void pop_front();
	T& front();
	T& back();
	int size() const;
	bool empty() const;

	class iterator {
	private:
		Node<T>* ptr;
	public:
		iterator() : ptr(nullptr) {}
		iterator(Node<T>* pointer) : ptr(pointer) {}
		iterator operator++() {iterator i = *this; ptr = ptr->next; return i;}
		iterator operator++(int) {ptr = ptr->next; return *this;};
		T& operator*() {return ptr->data;};
		Node<T>* operator->() {return ptr;}
		Node<T>* copy() const {return ptr;}
		iterator operator=(const iterator& itr) {ptr = itr.ptr; return *this;}
		bool operator==(const iterator& itr) { return ptr == itr.ptr_;}
		bool operator!=(const iterator& itr) { return ptr != itr.ptr;}		
	};

	iterator begin();
	iterator end();

	//insert elem AFTER an element bound to itr
	iterator insert(iterator itr, const T& elem);
	void clear();
};

template <class T>
linked_list<T>::linked_list() {
	head = nullptr;
	tail = nullptr;
	length = 0;
}

template <class T>
linked_list<T>::~linked_list() {
	this->clear();
}

template <class T>
void linked_list<T>::push_back(const T& elem) {
	Node<T>* new_node = new Node<T>();
	new_node->data = elem;
	new_node->next = nullptr;
	if (head != nullptr)
		head->next = new_node;
	head = new_node;
	if (tail == nullptr)
		tail = new_node;
	length++;
}

template <class T>
void linked_list<T>::push_front(const T& elem) {
	Node<T>* new_node = new Node<T>();
	new_node->data = elem;
	new_node->next = tail;
	tail = new_node;
	if (head == nullptr)
		head = new_node;
	length++;
}

template <class T>
void linked_list<T>::pop_front() {
	assert(tail != nullptr);
	Node<T>* new_tail = tail->next;
	delete tail;
	tail = new_tail;
	length--;
	if (!length){
		head = nullptr;
	}
}

template <class T>
T& linked_list<T>::front() {
	assert(tail != nullptr);
	return tail->data;
}

template <class T>
T& linked_list<T>::back() {
	assert(head != nullptr);
	return head->data;
}

template <class T>
int linked_list<T>::size() const {
	return length;
}

template <class T>
bool linked_list<T>::empty() const {
	return length == 0;
}

template <class T>
typename linked_list<T>::iterator linked_list<T>::begin() {
	assert(tail != nullptr);
	return iterator(tail);
}

template <class T>
typename linked_list<T>::iterator linked_list<T>::end() {
	assert(head != nullptr);
	return iterator(head->next);
}

template <class T>
typename linked_list<T>::iterator linked_list<T>::insert(iterator itr, const T& elem) {
	assert(itr != nullptr);
	Node<T>* new_elem = new Node<T>();
	new_elem->data = elem;
	new_elem->next = itr->next;
	itr->next = new_elem;
	length++;
	return iterator(new_elem);
}

template <class T>
void linked_list<T>::clear() {
	while (length) {
		this->pop_front();
	}
	tail = nullptr;
	head = nullptr;
}