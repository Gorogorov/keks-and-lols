#pragma once

#include <cassert>
#include "./linked_list.h"

template <class T>
class queue{
private:
	linked_list<T> lst;
public:
	queue() : lst() {}
	T& front();
	T& back();
	void push(const T& elem);
	void pop();
	int size() const;
	bool empty() const;
	void clear();

	class iterator {
	private:
		Node<T>* ptr;
	public:
		iterator() : ptr(nullptr) {}
		iterator(Node<T>* pointer) : ptr(pointer) {}
		iterator operator++() {iterator i = *this; ptr = ptr->next; return i;}
		iterator operator++(int) {ptr = ptr->next; return *this;};
		T& operator*() {return ptr->data;};
		Node<T>* operator->() {return ptr;}
		Node<T>* copy() const {return ptr;}
		iterator operator=(const iterator& itr) {ptr = itr.copy(); return *this;}
		iterator operator=(const typename linked_list<T>::iterator& itr) {ptr = itr.copy(); return *this;}
		bool operator==(const iterator& itr) { return ptr == itr.copy();}
		bool operator==(const typename linked_list<T>::iterator& itr) { return ptr == itr.copy();}
		bool operator!=(const iterator& itr) { return ptr != ptr.copy();}
		bool operator!=(const typename linked_list<T>::iterator& itr) { return ptr != itr.copy();}		
	};

	typename linked_list<T>::iterator begin();
	typename linked_list<T>::iterator end();
};

template <class T>
T& queue<T>::front() {
	assert(lst.size() > 0);
	return lst.front();
}

template <class T>
T& queue<T>::back() {
	assert(lst.size() > 0);
	return lst.back();
}

template <class T>
void queue<T>::push(const T& elem) {
	lst.push_back(elem);
}

template <class T>
void queue<T>::pop() {
	assert(lst.size() > 0);
	lst.pop_front();
}

template <class T>
int queue<T>::size() const {
	return lst.size();
}

template <class T>
bool queue<T>::empty() const {
	return lst.empty();
}

template <class T>
void queue<T>::clear() {
	lst.clear();
}

template <class T>
typename linked_list<T>::iterator queue<T>::begin() {
	return lst.begin();
}

template <class T>
typename linked_list<T>::iterator queue<T>::end() {
	return lst.end();
}