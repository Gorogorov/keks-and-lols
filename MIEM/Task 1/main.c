#include <stdio.h>
#include <stdlib.h>

typedef struct int_pair
{
  int x, y;
} int_pair;

int is_ground(int i, int j, int n, int m, char** matrix)
{
  if (i > 0 && matrix[i-1][j] == '#')
  {
    return 1;
  }
  if (j+1 < m && matrix[i][j+1] == '#')
  {
    return 1;
  }
  if (i+1 < n && matrix[i+1][j] == '#')
  {
    return 1;
  }
  if (j > 0 && matrix[i][j-1] == '#')
  {
    return 1;
  }
  return 0;
}


int build_path(int i, int j, int n, int m, char** matrix, int_pair* path, int path_len, int si, int sj, int depth)
{
  depth++;
  if (path[path_len].x != -1 || path[path_len].y != -1)
  {
    return 0;
  }
  matrix[i][j] = '*';
  path[path_len].x = i;
  path[path_len].y = j;
  path_len++;
  if (depth > 1)
  {
    if (i - si <= 1 && i - si >= -1 && j - sj <= 1 && j - sj >= -1)
    {
      return 1;
    }
  }
  matrix[i][j] = '*';
  int is_go = 0;
  if (i > 0 && matrix[i-1][j] == '.' &&
          is_ground(i-1, j, n, m, matrix))
  {
    is_go += build_path(i-1, j, n, m, matrix, path, path_len, si, sj, depth);
  }
  if (i > 0 && j+1 < m && matrix[i-1][j+1] == '.' && 
          (matrix[i-1][j] != '#' || matrix[i][j+1] != '#') 
          && is_ground(i-1, j+1, n, m, matrix))
  {
    is_go += build_path(i-1, j+1, n, m, matrix, path, path_len, si, sj, depth);
  }
  if (j+1 < m && matrix[i][j+1] == '.' && 
          is_ground(i, j+1, n, m, matrix))
  {
    is_go += build_path(i, j+1, n, m, matrix, path, path_len, si, sj, depth);
  }
  if (i+1 < n && j+1 < m && matrix[i+1][j+1] == '.' && 
          (matrix[i+1][j] != '#' || matrix[i][j+1] != '#') 
          && is_ground(i+1, j+1, n, m, matrix))
  {
    is_go += build_path(i+1, j+1, n, m, matrix, path, path_len, si, sj, depth);
  }
  if (i+1 < n && matrix[i+1][j] == '.' && 
          is_ground(i+1, j, n, m, matrix))
  {
    is_go += build_path(i+1, j, n, m, matrix, path, path_len, si, sj, depth);
  }
  if (i+1 < n && j > 0 && matrix[i+1][j-1] == '.' && 
          (matrix[i+1][j] != '#' || matrix[i][j-1] != '#') && 
          is_ground(i+1, j-1, n, m, matrix))
  {
    is_go += build_path(i+1, j-1, n, m, matrix, path, path_len, si, sj, depth);
  }
  if (j > 0 && matrix[i][j-1] == '.' && 
          is_ground(i, j-1, n, m, matrix))
  {
    is_go += build_path(i, j-1, n, m, matrix, path, path_len, si, sj, depth);
  }
  if (i > 0 && j > 0 && matrix[i-1][j-1] == '.' && 
          (matrix[i-1][j] != '#' || matrix[i][j-1] != '#') && 
          is_ground(i-1, j-1, n, m, matrix))
  {
    is_go += build_path(i-1, j-1, n, m, matrix, path, path_len, si, sj, depth);
  }
  if (!is_go)
  {
    matrix[i][j] = '.';
    path_len--;
    path[path_len].x = -1;
    path[path_len].y = -1;
    return 0;
  }
  return 1;
}


int main()
{
  FILE* fr;
  if ((fr = fopen("../data/task1.txt", "r")) == NULL)
  {
    printf("Невозможно открывать файл\n");
    exit(1);
  }
  int n, m;
  fscanf(fr, "%d", &n);
  fscanf(fr, "%d", &m);
  char** matrix = (char**) malloc(sizeof(char*) * n);
  for (int i = 0; i < n; i++)
  {
    matrix[i] = (char*) malloc(sizeof(char) * m);
  }
  char t;
  fscanf(fr, "%c", &t);
  for (int i = 0; i < n; i++)
  {
    for (int j = 0; j < m; j++)
    {
      fscanf(fr, "%c", &matrix[i][j]);
    }
    fscanf(fr, "%c", &t);
  }

  int start_i = -1, start_j = -1;
  for (int i = 0; i < n; i++)
  {
    for (int j = 0; j < m; j++)
    {
      if (matrix[i][j] == '#' && start_i == -1 && start_j == -1)
      {
        start_i = i;
        start_j = j-1;
      }
    }
  }

  int_pair* path = (int_pair*) malloc(sizeof(int_pair) * n * m);
  for (int i = 0; i < n*m; i++)
  {
    path[i].x = -1;
    path[i].y = -1;
  }
  
  build_path(start_i, start_j, n, m, matrix, path, 0, start_i, start_j, -1);
  for (int i = 0; i < n; i++)
  {
    for (int j = 0; j < m; j++)
    {
      printf("%c", matrix[i][j]);
    }
    printf("\n");
  }
  int path_ind = 0;
  while(1)
  {
    if (path[path_ind].x == -1 || path[path_ind].y == -1)
    {
      break;
    }
    printf("(%d, %d); ", path[path_ind].x, path[path_ind].y);
    path_ind++;
  }
  printf("\n");
  fclose(fr);
}
