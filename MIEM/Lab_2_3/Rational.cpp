#include <iostream>

using namespace std;

class Rational {
private:
	int p, q;
	int gcd(int a, int b);
	void sign(int& a, int& b);
public:
	Rational() : p(0), q(0) {}
	Rational(int a, int b) p(a), q(b);
	Rational operator+(const Rational& R);
	Rational operator-(const Rational& R);
	Rational operator*(const Rational& R);
	Rational operator/(const Rational& R);
}

int Rational::gcd(int a, int b) {
	if (!b) return a;
	return gcd(b, a%b);
}

void Rational::sign(int& a, int& b) {
	if (b < 0) {
		a *= -1;
		b *= -1;
	}
}

Rational Rational::operator+(const Rational& R) {
	Rational cur(p*R.q + q*R.p, q*R.q);
	int g = gcd(cur.p, cur.q);
	cur.p /= g;
	cur.q /= g;
	return cur;
}

Rational Rational::operator-(const Rational& R) {
	Rational cur(p*R.q - q*R.p, q*R.q);
	int g = gcd(cur.p, cur.q);
	cur.p /= g;
	cur.q /= g;
	return cur;
}

Rational Rational::operator*(const Rational& R) {
	Rational cur(p*R.p, q*R.q);
	int g = gcd(cur.p, cur.q);
	cur.p /= g;
	cur.q /= g;
	return cur;
}

Rational Rational::operator/(const Rational& R) {
	Rational cur(p*R.q, q*R.p);
	int g = gcd(cur.p, cur.q);
	cur.p /= g;
	cur.q /= g;
	return cur;
}

int main() {

	return 0;
}