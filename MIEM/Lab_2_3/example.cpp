#include <fstream>
#include <iostream>

using namespace std;

struct Real {
	Real() {
		a=0;
	}
	double a;
	virtual void read_from_file(ifstream& stream) = 0;
	virtual void inverse() = 0;
	virtual void print() = 0;
};

struct Complex : virtual Real {
	Complex() : Real() {
		b=0;
	}
	double a,b;
	void read_from_file(ifstream& stream);
	void inverse();
	void print();
};

struct Quaternion : virtual Real {
	Quaternion() : Real() {
		b = c = d = 0;
	}
	double a, b, c, d;
	void read_from_file(ifstream& stream);
	void inverse();
	void print();
};

void Complex::read_from_file(ifstream& stream) {
	stream >> a >> b;
}

void Complex::inverse() {
	b = -b;
}

void Complex::print() {
	cout << a << " " << b << "\n";
}

void Quaternion::read_from_file(ifstream& stream) {
	stream >> a >> b >> c >> d;
}

void Quaternion::inverse() {
	b = -b;
	c = -c;
	d = -d;
}

void Quaternion::print() {
	cout << a << " " << b << " " << c << " " << d << "\n";
}

int main() {
	ifstream in("input.txt");
	Real* nums[10];
	for (int i = 0; i < 10; i++) {
		if (i%2) nums[i] = new Complex();
		else nums[i] = new Quaternion();
		nums[i]->read_from_file(in);
	}
	in.close();
	for (int i = 0; i < 10; i++) {
		nums[i]->print();
	}
	for (int i = 0; i < 10; i++) {
		nums[i]->inverse();
	}
	for (int i = 0; i < 10; i++) {
		nums[i]->print();
	}
	return 0;
}