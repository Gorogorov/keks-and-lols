#include <iostream>
#include <vector>

using namespace std;

int main() {
	int N, M;
	cin >> N >> M;
	vector<vector<int>> G;
	for (int i = 0; i < N; i++) {
		G.push_back(vector<int>(N,0));
	}
	for (int i = 0; i < M; i++) {
		int a, b;
		cin >> a >> b;
		G[a-1][b-1] = 1;
		G[b-1][a-1] = 1;
	}
	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			cout << G[i][j] << " ";
		}
		cout << "\n";
	}
	return 0;
}