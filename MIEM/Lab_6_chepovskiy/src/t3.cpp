#include <iostream>
#include <queue>
#include <vector>
#include <algorithm>

void bfs(std::vector<std::vector<int> > G, int F, int S) {
	std::queue<int> Q;
	std::vector<bool> used(G.size(), false);
	std::vector<int> depth(G.size(), -1);
	std::vector<int> parent(G.size(), -1);
	used[F] = true;
	depth[F] = 0;
	parent[F] = -1;
	Q.push(F);
	while (!Q.empty()) {
		int v = Q.front();
		for (int i = 0; i < G[v].size(); i++) {
			if (!used[G[v][i]]) {
				used[G[v][i]] = true;
				depth[G[v][i]] = depth[v]+1;
				parent[G[v][i]] = v;
				Q.push(G[v][i]);
			}
		}
		Q.pop();
	} 
	if (!used[S]) {
		std::cout << "-1\n";
		return;
	}
	std::cout << depth[S] << "\n";
	int v = S;
	std::vector<int> ans;
	while (v != -1) {
		ans.push_back(v+1);
		v = parent[v];
	}
	std::reverse(ans.begin(), ans.end());
	for (auto i : ans) {
		std::cout << i << " ";
	}
	std::cout << "\n";
}

int main() {
	int N,M,S,F;
	std::cin >> N >> M >> F >> S;
	std::vector<std::vector<int> > G(N);
	int a, b;
	for (int i = 0; i < M; i++) {
		std::cin >> a >> b;
		G[a-1].push_back(b-1);
	}
	bfs(G, F-1, S-1);
}
