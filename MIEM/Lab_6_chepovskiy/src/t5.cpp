#include <iostream>
#include <vector>
#include "../include/priority_queue.h"

const int INF = 10000000;

int Prim(int N, std::vector<std::vector<Vertex>> G) {
	priority_queue PQ;
	PQ.insert(Vertex(1, 0));
	for (int i = 2; i <= N; i++) {
		PQ.insert(Vertex(i, INF));
	}
	std::vector<int> dist(G.size()+3, INF);
	std::vector<bool> used(G.size()+3, false);
	dist[1] = 0;
	int ans = 0;
	while (!PQ.empty()) {
		Vertex t = PQ.extract_top();
		int u = t.v;
		ans += t.w;
		used[u] = true;
		for (int i = 0; i < G[u].size(); i++) {
			int v = G[u][i].v;
			int weight = G[u][i].w;
			if (dist[v] > weight && !used[v]) {
				dist[v] = weight;
				PQ.decrease_key(PQ.position(G[u][i]), Vertex(v, dist[v]));
			}
		}
	}
	return ans;
}

int main() {
	int N, M;
	std::cin >> N >> M;
	std::vector<std::vector<Vertex>> G(N+1);
	for (int i = 0; i < M; i++) {
		int a, b, d;
		std::cin >> a >> b >> d;
		G[a].push_back(Vertex(b, d));
		G[b].push_back(Vertex(a, d));
	}
	std::cout << Prim(N, G) << "\n";
	return 0;
}