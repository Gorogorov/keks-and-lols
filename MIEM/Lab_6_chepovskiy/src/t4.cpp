#include <iostream>
#include <vector>
#include "../include/priority_queue.h"

const int INF = 10000000;

int Dijkstra(int N, std::vector<std::vector<Vertex>> G, int w, int s) {
	priority_queue PQ;
	PQ.insert(Vertex(w, 0));
	for (int i = 1; i <= N; i++) {
		if (i != w) {
			PQ.insert(Vertex(i, INF));
		}
	}
	std::vector<int> dist(G.size()+3, INF);
	dist[w] = 0;
	while (!PQ.empty()) {
		int u = PQ.extract_top().v;
		for (int i = 0; i < G[u].size(); i++) {
			int v = G[u][i].v;
			int weight = G[u][i].w;
			if (dist[v] > dist[u]+weight) {
				dist[v] = dist[u]+weight;
				PQ.decrease_key(PQ.position(G[u][i]), Vertex(v, dist[v]));
			}
		}
	}
	if (dist[s] == INF) {
		return -1;
	}
	return dist[s];
}

int main() {
	int N, M, S, F;
	std::cin >> N >> M >> S >> F;
	std::vector<std::vector<Vertex>> G(N+3);
	std::vector<Vertex> vertexes(N+3);
	int a, b, d;
	for (int i = 0; i < M; i++) {
		std::cin >> a >> b >> d;
		G[a].push_back(Vertex(b, d));
	}
	std::cout << Dijkstra(N, G, S, F) << "\n";
	return 0;
}