#include <iostream>
#include <map>

int main() {
	int n;
	std::cin >> n;
	int a, b;
	std::map<int, int> G;
	for (int i = 0; i < n; i++) {
		std::cin >> a >> b;
		G[a]++;
		G[b]++;
	}
	std::cout << G.size() << "\n";
	std::map<int,int>::iterator it;
	for (it = G.begin(); it != G.end(); it++) {
		std::cout << (*it).first << " "  << (*it).second << "\n";
	}
	return 0;
}