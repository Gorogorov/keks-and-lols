#!/bin/bash
tassk=1
for i in ./data/Tests/5/*
do
    num=$(echo $i | tail -c +10 | head -c 1)
    echo $tassk
    out="./data/out$tassk.out"
    ./exec/t5 < $i > $out
    echo -e "\033[38;5;148mTest $i\033[39m:"
    cat $out
    let "tassk=tassk+=1"
done