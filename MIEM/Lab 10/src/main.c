#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "../include/doubly_linked_list.h"


// Добаляет элемент списка сразу за элементом lst
// word - добавляемое слово, соответствующее элементу
// Функция возвращает добавленный элемент
Node* add_node(Node* lst, char* word)
{
  Node *temp, *p;
  temp = (Node*) malloc(sizeof(Node));
  // Сейчас lst <--> p
  p = lst->next;
  if (p != NULL)
  {
    p->prev = temp;
  }
  lst->next = temp;
  temp->word = (char*) malloc(sizeof(char) * 1000);
  strcpy(temp->word, word);
  temp->next = p;
  temp->prev = lst;
  // Теперь lst <--> temp <--> p
  return(temp);
}


// Добавляет первую вершину в список
// word - добавляемое слово, соответствующее элементу
// Функция озвращает добавленную вершину
Node* init_list(char* word)
{
  Node *N;
  N = (Node*)malloc(sizeof(Node));
  N->word = (char*) malloc(sizeof(char) * 1000);
  strcpy(N->word, word);
  N->next = NULL;
  N->prev = NULL;
  return(N);
}


// Создает список слов
// Возвращает вершину, являющуюся началом созданного списка
Node* create_list()
{
  Node* head = NULL;
  Node* tail = NULL;
  char* word = (char*) malloc(sizeof(char) * 1000);
  int is_end = 0;
  while (!is_end)
  {
    // Считываем слово
    scanf("%s", word);
    // Если это слово оканчивается точкой, удаляем точку и 
    // ставим флаг, что ввод окончен
    if (word[strlen(word)-1] == '.')
    {
      is_end = 1;
      word[strlen(word)-1] = '\0';
    }
    // Если список не пуст, то добавлям новое слово в хвост
    // функцией add_node
    if (tail != NULL)
    {
      tail = add_node(tail, word); 
    }
    // Иначе добавляем слово функцией init_list
    else
    {
      head = init_list(word);
      tail = head;
    }
  }
  return head;
}


// Выводит на экран весь список слов 
// head - первая вершина списка
void show_list(Node* head)
{
  while (head != NULL)
  {
    printf("%s ", head->word);
    head = head->next;
  }
  printf("\n");
}


// Делит список на 2: с длиной слов > N и <= N
// head - первая вершина делимого списка
// Функция возвращает пару: указатели на первые элементы
// первого и второго списков
node_pair* cut_sentence(Node* head, int N)
{
  node_pair* nd = (node_pair*) malloc(sizeof(node_pair));
  nd->N1 = NULL;
  nd->N2 = NULL;
  Node* tail1 = NULL;
  Node* tail2 = NULL;
  // Идем циклом от начала списка к его концу
  while (head != NULL)
  {
    // Если длина слова > N, добавляем его в первый список
    if (strlen(head->word) > N)
    {
      // Если список еще не инециализирован, то используем 
      // функцию init_list
      if (nd->N1 == NULL)
      {
        nd->N1 = init_list(head->word);
        tail1 = nd->N1;
      }
      // Если список уже не пустой, используем add_node
      else
        tail1 = add_node(tail1, head->word);
    }
    // Если длина слова <= N, добавляем его во второй список
    if (strlen(head->word) <= N)
    {
      // Если список еще не инициализирован, то используем 
      // функцию init_list
      if (nd->N2 == NULL)
      {
        nd->N2 = init_list(head->word);
        tail2 = nd->N2;
      }
      // Если список уже не пустой, используем add_node
      else
        tail2 = add_node(tail2, head->word);
    }
    head = head->next;
  }
  return nd;
}


int main()
{
  printf("Input sentence: ");
  // Создаем список слов
  Node* head = create_list();
  printf("Sentence:\n");
  // Выводим его на экран
  show_list(head);
  printf("Input N: ");
  int N;
  scanf("%d", &N);
  // Делим его на 2 списка: в одном слова длины <= N, в другом > N
  node_pair* nd = cut_sentence(head, N);
  printf("Length > N: \n");
  // Выводим на экран первый список
  show_list(nd->N1);
  printf("Length <= N: \n");
  // Выводим на экран второй список
  show_list(nd->N2);
  return 0;
}
