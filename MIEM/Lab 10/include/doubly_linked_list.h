#pragma once

typedef struct Node
{
    char* word;
    struct Node *next;
    struct Node *prev;
} Node;

typedef struct node_pair
{
  Node *N1, *N2;
} node_pair;
