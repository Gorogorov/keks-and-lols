#include <bits/stdc++.h>

using namespace std;

void reverseNumbers(string &src) {
	size_t found_beg = src.find_first_of("0123456789");
	size_t found_end = src.find_first_not_of("0123456789", found_beg);
	while (found_beg != string::npos) {
		reverse(src.begin() + found_beg, src.begin() + found_end);
		found_beg = src.find_first_of("0123456789", found_end+1);
		found_end = src.find_first_not_of("0123456789", found_beg+1);
		if (found_end == string::npos) found_end = src.size();
	}
}

void make_map(map <string, string>& M) {
	M["0"] = "zero", M["1"] = "one", M["2"] = "two", M["3"] = "three", M["4"] = "four";
	M["5"] = "five", M["6"] = "six", M["7"] = "seven", M["8"] = "eight", M["9"] = "nine"; 
	M["10"] = "ten", M["11"] = "eleven", M["12"] = "twelve", M["13"] = "thirteen", M["14"] = "fourteen"; 
	M["15"] = "fifteen", M["16"] = "sixteen", M["17"] = "seventeen", M["18"] = "eighteen", M["19"] = "nineteen";
	int number = 20;
	while (number < 100) {
		string s_number = to_string(number), res_number;
		if (s_number[0] == '2') res_number = "twenty";
		else if (s_number[0] == '3') res_number = "thirty";
		else if (s_number[0] == '4') res_number = "fourty";
		else if (s_number[0] == '5') res_number = "fifty";
		else if (s_number[0] == '6') res_number = "sixty";
		else if (s_number[0] == '7') res_number = "seventy";
		else if (s_number[0] == '8') res_number = "eighty";
		else if (s_number[0] == '9') res_number = "ninety";
		res_number += " "+M[string(1, s_number[1])];
		M[s_number] = res_number;
		number++; 
	}
}

void rewriteNumbers(string &src, map <string, string>& M) {
	size_t found_beg = src.find_first_of("0123456789");
	size_t found_end = src.find_first_not_of("0123456789", found_beg);
	while (found_beg != string::npos) {
		if (found_end - found_beg < 3) {
			string substr;
			for (int i = found_beg; i < found_end; i++)
				substr.push_back(src[i]);
			src.replace(found_beg, found_end - found_beg, M[substr]);
		}
		found_beg = src.find_first_of("0123456789", found_end+1);
		found_end = src.find_first_not_of("0123456789", found_beg+1);
		if (found_end == string::npos) found_end = src.size();
	}	
}

int main() {
	string test1, test2;
	
	getline(cin, test1);
	reverseNumbers(test1);
	cout << test1 << "\n";

	getline(cin, test2);
	map <string, string> M;
	make_map(M);
	rewriteNumbers(test2, M);
	cout << test2 << "\n";
	return 0;
}