#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

void maximizeNumbers(string &src) {
	bool is_find = false;
	string res;
	int max_num = -1;
	int  cur_num = -1;
	for (int i = 0; i < src.size(); i++) {
		if ((is_find && (src[i] < '0' || src[i] > '9')) || i == src.size()-1) {
			if (src[i] >= '0' && src[i] <= '9') cur_num = 10*max(cur_num,0) + int(src[i]) - int('0');
			if (cur_num == -1) continue;
			is_find = false;
			if (cur_num > max_num) max_num = cur_num;
			cur_num = -1;
		}
		else if (!is_find && src[i] >= '0' && src[i] <= '9') {
			is_find = true;
			cur_num = int(src[i]-'0');
		}
		else if (src[i] >= '0' && src[i] <= '9') cur_num = 10*cur_num + int(src[i]) - int('0');
	}
	is_find = false;
	cur_num = -1;
	for (int i = 0; i < src.size(); i++) {
		if ((is_find && (src[i] < '0' || src[i] > '9')) || i == src.size()-1) {
			if (src[i] >= '0' && src[i] <= '9') cur_num = 10*max(cur_num, 0) + int(src[i]) - int('0');
			if (cur_num == -1) {
				if (src[i] < '0' || src[i] > '9') res.push_back(src[i]);
				continue;
			}
			is_find = false;
			string num;
			cur_num = max_num;
			if (!cur_num) num.push_back('0');
			while(cur_num) {
				num.push_back(char(cur_num%10 + '0'));
				cur_num /= 10;
			}
			cur_num = -1;
			for (int j = num.size()-1; j >=0; j--) {
				res.push_back(num[j]);
			}
			if (src[i] < '0' || src[i] > '9') res.push_back(src[i]);
		}
		else if (!is_find && src[i] >= '0' && src[i] <= '9') {
			is_find = true;
			cur_num = int(src[i]-'0');
		}
		else if (src[i] >= '0' && src[i] <= '9') cur_num = 10*cur_num + int(src[i]) - int('0');
		else res.push_back(src[i]);
	}
	src = res;
}

void sortNumbers(string& src) {
	vector <int> numbers;
	bool is_find = false;
	string res;
	int  cur_num = -1;
	for (int i = 0; i < src.size(); i++) {
		if ((is_find && (src[i] < '0' || src[i] > '9')) || i == src.size()-1) {
			if (src[i] >= '0' && src[i] <= '9') cur_num = 10*max(0, cur_num) + int(src[i]) - int('0');
			if (cur_num == -1) {
				continue;
			}
			is_find = false;
			string bool_num;
			numbers.push_back(cur_num);
			cur_num = -1;
		}
		else if (!is_find && src[i] >= '0' && src[i] <= '9') {
			is_find = true;
			cur_num = int(src[i]-'0');
		}
		else if (src[i] >= '0' && src[i] <= '9') cur_num = 10*cur_num + int(src[i]) - int('0');
	}
	sort(numbers.begin(), numbers.end());
	is_find = false;
	cur_num = -1;
	int ind = 0;
	for (int i = 0; i < src.size(); i++) {
		if ((is_find && (src[i] < '0' || src[i] > '9')) || i == src.size()-1) {
			if (src[i] >= '0' && src[i] <= '9') cur_num = 10*max(0, cur_num) + int(src[i]) - int('0');
			if (cur_num == -1) {
				if (src[i] < '0' || src[i] > '9') res.push_back(src[i]);
				continue;
			}
			is_find = false;
			string num;
			int q = numbers[ind];
			ind++;
			if (!q) num.push_back('0');
			while(q) {
				num.push_back(char(q%10 + '0'));
				q /= 10;
			}
			for (int j = num.size()-1; j >=0; j--) {
				res.push_back(num[j]);
			}
			cur_num = -1;
			if (src[i] < '0' || src[i] > '9') res.push_back(src[i]);
		}
		else if (!is_find && src[i] >= '0' && src[i] <= '9') {
			is_find = true;
			cur_num = int(src[i]-'0');
		}
		else if (src[i] >= '0' && src[i] <= '9') cur_num = 10*cur_num + int(src[i]) - int('0');
		else res.push_back(src[i]);
	}
	src=res;
}

int main() {
	string test1;
	cout << "Input your string for task 1\n";
	getline(cin, test1);
	maximizeNumbers(test1);
	cout << test1 << "\n";

	string test2;
	cout << "Input your string for task 2\n";
	getline(cin, test2);
	sortNumbers(test2);
	cout << test2 << "\n";
	return 0;
}