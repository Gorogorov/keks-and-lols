#include <iostream>

//Интерфейс 1
class fur {
public:
	virtual void fluffiness() = 0;
};

class fluffy : public fur {
public:
	void fluffiness() {
		std::cout << "Very fluffy\n";
	}
};

class so_so_fluffy : public fur {
public:
	void fluffiness() {
		std::cout << "So-so fluffy\n";
	}
};

class hairless : public fur {
public:
	void fluffiness() {
		std::cout << "Hairless :c\n";
	}
};

//Интерфейс 2
class feet {
public:
	virtual void softness() = 0;
};

class soft_feet : public feet {
public:
	void softness(){
		std::cout << "Very soft feed\n";
	}
};

class hard_feet : public feet {
public:
	void softness(){
		std::cout << "Hard feet :c\n";
	}
};


// Основной класс, инкапсулирующий интерфейсы fur и feet
class Cat {
protected:
	fur* _fur;
	feet* _feet;
public:
	virtual void display() = 0;
	void performFur(){
		_fur->fluffiness();
	}
	void performFeet(){
		_feet->softness();
	}
	~Cat(){
		delete _fur;
		delete _feet;
	}
};

//Классы-наследники Cat, использующие конкретные аспекты поведения
class Siberian_cat : public Cat {
public:
	Siberian_cat(){
		_fur = new fluffy();
		_feet = new soft_feet();
	}
	void display(){
		std::cout << "I'm a siberian cat!\n";
	}
};

class British_shorthair_cat : public Cat {
public:
	British_shorthair_cat() {
		_fur = new so_so_fluffy();
		_feet = new soft_feet();
	}
	void display() {
		std::cout << "I'm a british shorthair cat!\n";
	}
};


class Sphynx_cat : public Cat {
public:
	Sphynx_cat() {
		_fur = new hairless();
		_feet = new hard_feet();
	}
	void display() {
		std::cout << "I'm a sphynx cat!\n";
	}
};


void mini_cat_simulator() {
	Cat* siberian_cat = new Siberian_cat();
	siberian_cat->display();
	siberian_cat->performFur();
	siberian_cat->performFeet();

	Cat* british_shorthair_cat = new British_shorthair_cat();
	british_shorthair_cat->display();
	british_shorthair_cat->performFur();
	british_shorthair_cat->performFeet();

	Cat* sphynx_cat = new Sphynx_cat();
	sphynx_cat->display();
	sphynx_cat->performFur();
	sphynx_cat->performFeet();
	
	delete siberian_cat;
	delete british_shorthair_cat;
	delete sphynx_cat;	
}

int main() {
	mini_cat_simulator();
	return 0;
}