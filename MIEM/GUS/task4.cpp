#include <iostream>
#include <vector>

using namespace std;

void f1() {
	int n;
	cin >> n;
	vector <int> V(n);
	for (int i = 1; i <= n; i++) {
		V[i-1] = i;
	}
	for (auto v: V) {
		if (!(v%3)) cout << "three";
		if (!(v%5)) cout << "five";
		if (v%3 && v%5) cout << v;
		cout << "\n";
	}
}

void f2(vector<int>& V) {
	int n = V.size();
	if (!V.size()) {
		cout << "Input size of vector: ";
		cin >> n;
	}
	for (int i = 0; i < n; i++) {
		int t;
		cin >> t;
		V.push_back(t);
	}
}

void f3(vector<int>& V) {
	for (auto v: V) {
		cout << v << " ";
	}
	cout << "\n";
}

int  f4(vector <int> V, int index) { 
	int ans1, ans2;
	try {
		ans1 = V.at(index);
		if (index < 0 || index > V.size()-1) {
			throw logic_error("index > size\n");
		}
		ans2 = V[index];
	}
	catch (exception& e){
		cout << e.what() << "\n";
		return -1;
	}
	if (ans1 != ans2) {
		cout << "ans1 != ans2\n";
		return -1;
	}
	return ans1;
}

int f5(vector <int> V) {
	if (!V.size()) throw runtime_error("Size of vector == 0");
	return V[0]*V[V.size()-1];
}

long double f6(vector <int> V) {
	long long sum = 0;
	for (auto v: V) {
		sum+=v;
	}
	return 1.0*sum/V.size();
}

long double f7(vector <int> V) {
	long double av = f6(V);
	long double q = 0;
	for (auto v: V) {
		q += (av-v)*(av-v);
	}
	q /= V.size();
	return q;
}

void f8() {
	vector<vector<int>> V;
	int n, m;
	cin >> n >> m;
	for (int i = 0; i < n; i++) {
		vector <int> temp(m);
		V.push_back(temp);
	}
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			if (i == j) V[i][j] = i;
			else if (i > j) V[i][j]= 10;
			else V[i][j] = 2;
		}
	}
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < m; j++) {
			cout << V[i][j] << " ";
		}
		cout << "\n";
	}
}

vector <int>& f9(vector <int>& V) {
	vector <int> ans;
	int n = V.size();
	for (int i = 0; i < n; i++) {
		if ((!i || V[i-1] != V[i]) && (i == n-1 || V[i+1] != V[i])) ans.push_back(V[i]);
	}
	V = ans;
	return V;
}

int main() {
	f1();
	vector<int> v;
	f2(v);
	f3(v);
	if (f4(v, 3)!=-1) {
		cout << f4(v, 3) << "\n";
	}
	if (f4(v, 6) != -1) {
		cout << f4(v, 6) << "\n";
	}
	try{
		cout << f5(v) << "\n";
	}
	catch(exception& e) {
		cout << e.what() << "\n";
	}
	vector<int> v2(100, 2e8);
	cout << f6(v2) << "\n";
	vector<int> v3;
	v3.push_back(1);
	v3.push_back(1);
	cout << f7(v3) << "\n";
	f8();
	v3.push_back(2);
	v3.push_back(2);
	vector <int > ans = f9(v3);
	for (auto v: ans) {
		cout << v << " ";
	}
	cout << "\n";
	return 0;
}