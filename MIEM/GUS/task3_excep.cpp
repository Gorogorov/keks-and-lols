#include <bits/stdc++.h>

using namespace std;

void task1() {
	int age;
	cout << "Input age for task 1\n";
	cin >> age;
	try {
		if (age < 0) throw logic_error("Age cannot be negative");
	}
	catch (exception& e) {
		cout << e.what() << "\n";
	}
}

void task2() {
	double a, b;
	cout << "Input a and b for task 2\n";
	cin >> a >> b;
	try {
		if (!b) throw logic_error("b == 0");
		cout << "a/b=" << a/b << "\n";
	}
	catch(exception& e) {
		cout << e.what() << "\n";
	}
}

void task3() {
	double x;
	cout << "Input x for task 3\n";
	cin >> x;
	try {
		if (x <= 0) throw logic_error("x<=0");
		cout << "log(x)=" << log(x) << "\n";
	}
	catch(exception& e) {
		cout << e.what() << "\n";
	}
}

void mysqrt() {
	double x;
	cout << "Input x for task 4\n";
	cin >> x;
	try{
		if (x < 0) throw logic_error("x < 0");
		cout << "sqrt(x)=" << sqrt(x) << "\n";
	}
	catch (exception& e) {
		throw e;
	}
}

void scream() {
	throw runtime_error("Scream");
}

void task5() {
	scream();
}

int main() {
	task1();
	task2();
	task3();
	try {
		mysqrt();
	}
	catch (exception& e) {
		cout << e.what() << "\n";
	}
	try {
		task5();
	}
	catch (exception& e) {
		cout << e.what() << "\n";
	}
	return 0;
}