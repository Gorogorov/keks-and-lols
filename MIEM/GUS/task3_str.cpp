#include <bits/stdc++.h>
#include <regex.h>

#define pattern1 "^[0-9]{1,2}\\.[0-9]{1,2}\\.[0-9]{1,4}$"
#define pattern2 "(.*)\\.(.*)\\.(.*)"

using namespace std;

void task1(string& s) {
	for (auto& a: s) {
		if (a == 'a') a = 'o';
	}
}

void task2(string& s) {
	replace(s.begin(), s.end(), 'a', 'o');
}

string task3(string s) {
	string ns;
	for (auto a: s) {
		if (a == 'a') ns.push_back('o');
		else ns.push_back(a);
	}
	return ns;
}

string task4(string s, int n) {
	string ns;
	while (n--) {
		for (auto a: s) {
			ns.push_back(a);
		}
	}
	return ns;
}

char  task5(string s, int index) { 
	char ans1, ans2;
	try {
		ans1 = s.at(index);
		if (index < 0 || index > s.size()-1) {
			throw logic_error("index > size\n");
		}
		ans2 = s[index];
	}
	catch (exception& e){
		cout << e.what() << "\n";
		throw e;
	}
	if (ans1 != ans2) {
		cout << "ans1 != ans2\n";
	}
	return ans1;
}

string task6(string s) {
	reverse(s.begin(), s.end());
	return s;
}

string task7(string s) {
	string ns;
	ns.push_back(s[0]);
	ns.push_back(s[s.size()-1]);
	return ns;
}

string task8(string s){
	string ns;
	for (auto a: s) {
		if (a >= '0' && a <= '9') ns += "**";
		else ns.push_back(a);
	}
	return ns;
}

string task9(string s) {
	int cntz = 0;
	string ns;
	for (auto a: s) {
		if (a=='*') cntz++;
	}
	try {
		if (cntz%2) throw logic_error("Exception!\n");
		bool is_z = false;
		for (auto a: s) {
			if (a == '*' && !is_z) is_z = true;
			else if (a=='*' && is_z) is_z = false;
			else if (!is_z) ns+=a;
		}
	}
	catch(exception& e) {
		cout << e.what() << "\n";
		throw e;
	}
	return ns;
}

string task10(string s) {
	string ns;
	ns.push_back(s[0]);
	ns.push_back(s[1]);
	for (int i = 2; i < s.size(); i++) {
		if (s[i] == 'm' && s[i-1]=='m' && s[i-2]=='h') {
			ns.pop_back();
			ns.pop_back();
		} 
		else ns.push_back(s[i]);
	}
	return ns;
}

void task11(string s) {
	regex e(pattern1);
	smatch m, t;
	if (regex_search(s, m, e)) {
		regex ee(pattern2);
		regex_search(s, t, ee);
		//cout << t[1] << "!" << t[2] << "!" << t[3] << "\n";
		int d = stoi(t[1]), m = stoi(t[2]), y = stoi(t[3]);
		// d, m, y >0 т.к. иначе регулярка бы не нашла эту строку
		try {
			if (m > 12) throw runtime_error("Incorrect date\n");
			if ((m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12) && d > 31) throw runtime_error("Incorrect date\n");
			else if ((m == 4 || m == 6 || m == 9 || m == 11) && d>30) throw runtime_error("Incorrect date\n");
			else if (m == 2 && d > 28) throw runtime_error("Incorrect date\n");
			cout << "Correct date\n";
		}
		catch (exception& e){
			cout << e.what() << "\n";
			throw e;
		}
	}
	else {
		cout << "No date\n";
	}
}

int main () {
	string s;
	cout << "Input string for tasks 1, 2, 3\n";
	cin >> s;
	string t1 = s, t2 = s, t3 = s;
	task1(t1);
	cout << "Task1: " << t1 << "\n";
	task2(t2);
	cout << "Task2: " << t2 << "\n";
	cout << "Task3: " << task3(t3) << "\n";
	
	string t4;
	int n;
	cout << "Input string and n for task 4\n";
	cin >> t4 >> n;
	cout << "Task4: " << task4(t4, n) << "\n";

	string t5;
	int index;
	cout << "Input string and index for task 5\n";
	cin >> t5 >> index;
	cout << "Task5: " << task5(t5, index) << "\n";

	string t6;
	cout << "Input string for task 6\n";
	cin >> t6;
	cout << "Task 6: " << task6(t6) << "\n";

	string t7;
	cout << "Input string for task 7\n";
	cin >> t7;
	cout << "Task 7: " << task7(t7) << "\n";

	string t8;
	cout << "Input string for task 8\n";
	cin >> t8;
	cout << "Task 8: " << task8(t8) << "\n";

	string t9;
	cout << "Input string for task 9\n";
	cin >> t9;
	cout << "Task 9: " << task9(t9) << "\n";

	string t10;
	cout << "Input string for task 10\n";
	cin >> t10;
	cout << "Task 10: " << task10(t10) << "\n";	

	string t11;
	cout << "Input date for task 11\n";
	cin >> t11;
	cout << "Task 11: ";
	task11(t11);
	return 0;
}