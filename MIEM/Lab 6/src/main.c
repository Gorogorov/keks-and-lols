#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../include/television.h"
#include "../include/cnt_models.h"

//Переопределяем task как: 'a' - если пользователь сам выполняет ввод
//'b' - массив с данными заранее определен 
#define task 'a'

//input_data() - функция заполнения массива с телевизорами tvs 4-мя элементами 
void input_data(television* tvs) {
  strcpy(tvs[0].model, "Model 1");
  strcpy(tvs[0].mfr.city, "Moscow");
  strcpy(tvs[0].mfr.factory, "Moscow factory num 1");
  tvs[0].mfr.year = 2019;
  tvs[0].ftrs.price = 20000;
  tvs[0].ftrs.screen_size = 1232;
  strcpy(tvs[1].model, "Model 2");
  strcpy(tvs[1].mfr.city, "Spb");
  strcpy(tvs[1].mfr.factory, "Spb factory num 1");
  tvs[1].mfr.year = 2025;
  tvs[1].ftrs.price = 2034000;
  tvs[1].ftrs.screen_size = 124332;
  strcpy(tvs[2].model, "Model 1");
  strcpy(tvs[2].mfr.city, "Moscow");
  strcpy(tvs[2].mfr.factory, "Moscow factory num 2");
  tvs[2].mfr.year = 2011;
  tvs[2].ftrs.price = 200;
  tvs[2].ftrs.screen_size = 12;
  strcpy(tvs[3].model, "Model 1");
  strcpy(tvs[3].mfr.city, "Moscow13");
  strcpy(tvs[3].mfr.factory, "Moscow factory num 111");
  tvs[3].mfr.year = 20120;
  tvs[3].ftrs.price = 20000344;
  tvs[3].ftrs.screen_size = 12343432;
}

//popular_model() - функция на основе массива с телевизорами tvs, введенного пользователем
//года year и количества телевизоров n находит самые популярные модели и выводит их на экран
void popular_model(television* tvs, int year, int n) {
  //cnt_models - массив с названиями моделей и количеством их употреблений
  cnt_models* cm = (cnt_models*) malloc(sizeof(cnt_models) * n);
  for (int i = 0; i < n; i++) {
    cm[i].model = (char*) malloc(sizeof(char) * 100);
    cm[i].count = 0;
  }

  //ind_model - индекс количества различных употребленных моделей
  int ind_model = 0;
  //Цикл по массиву телевизоворов, формирующий массив cm 
  for (int i = 0; i < n; i++) {
    //Обрабатываем телевизор только если год его выпуска больше введенного
    if (tvs[i].mfr.year >= year) {
      //is_find - употребляли мы назавние данной модели прежде. 0 - нет, 1 - да
      int is_find = 0;
      for(int j = 0; j < ind_model; j++) {
        //Если марка текущего телевизора найдена в массиве с раннее употребленными,
        //то увеличиваем количество ее употреблений на 1  
        if (!strcmp(cm[j].model, tvs[i].model)) {
          cm[j].count++;
          is_find = 1;
          break;
        }
      }
      //Если марка употреблена впервые, то добавляем ее в массив с марками cm 
      if (!is_find) {
        strcpy(cm[ind_model].model, tvs[i].model);
        ind_model++;
      }
    }
  }
  //max_cnt - число, которое обозначает количество употреблений самой популярной марки телевизора
  int max_cnt = 0;
  //Находим максимальное количество употреблений
  for (int i = 0; i < ind_model; i++) {
    if (cm[i].count > max_cnt) {
      max_cnt = cm[i].count;
    }
  }

  printf("Наиболее встречающиеся марки телевизоров, выпущенные после %d-ого года:\n", year);
  //Если количество употреблений марки равно максимальному max_cnt, то выводим имя марки
  for (int i = 0; i < ind_model; i++) {
    if (cm[i].count == max_cnt) {
      printf("%s ", cm[i].model);
    }
  }
  printf("\n");

  //Освобождаем динамическую память, выделенную в popular_model()
  for (int i = 0; i < n; i++) {
    free(cm[i].model);
  }
  free(cm);
}

int main() {
  //Если task == 'a', то пользователь сам вводит данные с клавиатуры
  if (task == 'a'){ 
    printf("Введите количество телевизоров:\n");
    int n;
    scanf("%d", &n);
    printf("Введите год:\n");

    int year;
    scanf("%d", &year);
    //tvs - массив с телевизорами. Выделяем память
    television* tvs = (television*) malloc(sizeof(television) * n);
    for (int i = 0; i < n; i++) {
      tvs[i].model = (char*)malloc(sizeof(char) * 100);
      tvs[i].mfr.city = (char*)malloc(sizeof(char) * 100);
      tvs[i].mfr.factory = (char*)malloc(sizeof(char) * 100);
    }
    //Пользователь вводит данные для n телевизоров
    for (int i = 0; i < n; i++) {
      printf("Введите данные для телевизора №%d:\n", i+1);
      printf("Модель: ");
      scanf("%s", tvs[i].model);
      printf("Город: ");
      scanf("%s", tvs[i].mfr.city);
      printf("Фабрика: ");
      scanf("%s", tvs[i].mfr.factory);
      printf("Год производства: ");
      scanf("%d", &tvs[i].mfr.year);
      printf("Цена: ");
      scanf("%d", &tvs[i].ftrs.price);
      printf("Размер экрана: ");
      scanf("%d", &tvs[i].ftrs.screen_size);
    }

    //Вычисляем самую популярную модель
    popular_model(tvs, year, n);

    //Освобождаем динамическую память, выделенную в main()
    for (int i = 0; i < n; i++) {
      free(tvs[i].model);
      free(tvs[i].mfr.city);
      free(tvs[i].mfr.factory);
    }  
    free(tvs);
  }
  else if (task == 'b') {
    int n = 4;
    int year = 2018;
    //Массив с телевизорами. Выделяем память
    television* tvs = (television*) malloc(sizeof(television) * n);
    for (int i = 0; i < n; i++) {
      tvs[i].model = (char*)malloc(sizeof(char) * 100);
      tvs[i].mfr.city = (char*)malloc(sizeof(char) * 100);
      tvs[i].mfr.factory = (char*)malloc(sizeof(char) * 100);
    }
    //Вводим данные в массив tvs
    input_data(tvs);

    //Вычисляем самую популярную модель
    popular_model(tvs, year, n);

    //Освобождаем динамическую память, выделенную в main()
    for (int i = 0; i < n; i++) {
      free(tvs[i].model);
      free(tvs[i].mfr.city);
      free(tvs[i].mfr.factory);
    }  
    free(tvs);
  }
  return 0;
}
