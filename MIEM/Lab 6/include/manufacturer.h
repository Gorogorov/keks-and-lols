#ifndef __MANUFACTURER_H__
#define __MANUFACTURER_H__

typedef struct manufacturer {
  char* city;
  char* factory;
  int year;
} manufacturer;

#endif
