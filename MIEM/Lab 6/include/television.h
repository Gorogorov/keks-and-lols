#ifndef __TELEVISION_H__
#define __TELEVISION_H__

#include "../include/features.h"
#include "../include/manufacturer.h"

typedef struct television {
  char* model;
  manufacturer mfr;
  features ftrs;
} television;

#endif
