#include <algorithm>
#include <vector>
#include <iostream>
#include <math.h>
#include <numeric>

//Lab5, variant 13: tasks 8, 13, 14

void task_8() {
	std::vector<double> nums;
	int n, k;
	double val;
	std::cin >> n;
	for (int i = 0; i < n; i++) {
		std::cin >> val;
		nums.push_back(val);
	}
	std::cin >> k;

	for_each(nums.begin(), nums.end(), [&] (double& value) {
		value = pow(value, k);
	});
	std::cout << std::accumulate(nums.begin(), nums.end(), 0.0) << "\n";
}

void task_13() {
	int val;
	std::string string_val;
	std::cin >> val;
	string_val = std::to_string(val);
	std::sort(string_val.begin(), string_val.end());
	int ans = 0;
	do {
		if (!(std::stoi(string_val)%11))
			ans++;
	} while (std::next_permutation(string_val.begin(), string_val.end()));
	std::cout << ans << "\n";
}

void task_14() {
	int main_number = 15951;
	int input_number = 12;
	int reverse_number = 21;
	while (input_number != reverse_number || input_number < main_number) {
		std::cout << "input the number:\n";
		std::cin >> input_number;
		std::string string_reverse_number = std::to_string(input_number);
		std::reverse(string_reverse_number.begin(), string_reverse_number.end());
		reverse_number = std::stoi(string_reverse_number);
		if (input_number != reverse_number) {
			std::cout << "your number is not palindrome, try again\n";
		}
		else if(input_number <= main_number) {
			std::cout << "your number < 15951, try again\n";
		}
	}
	std::cout << "answer: " << (input_number - main_number)/2.0 << "\n";
}

int main() {
	std::cout << "task 8: input n, n values and degree k:\n";
	task_8();
	std::cout << "task 13: input the number:\n";
	task_13();
	std::cout << "task 14: input the number (it should be palindrome and more than 15951):\n";
	task_14();
	return 0;
}