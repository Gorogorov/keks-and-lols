/* VARIANT 2 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

//Имена файлов этой программы дожны быть изменены в соответствии с вашими

//task1 решает задачу 1
void task1()
{
  FILE* fr;
  if((fr=fopen("../data/kr2_text.txt", "r"))==NULL) 
  {
    printf ("Невозможно открыть файл.\n");
    exit(1);
  }
  FILE* fw;
  if((fw=fopen("../data/kr2_text_output.txt", "w"))==NULL) 
  {
    printf ("Невозможно открыть файл.\n");
    exit(1);
  }
  ssize_t read = 0;
  size_t len = 0;
  char* line = NULL;
  while ((read = getline(&line, &len, fr)) != -1)
  {
    int i = -1;
    int old_ind = -1;
    int chet = 0, nechet = 0;
    while (line[i] != '\n') 
    {
      i++;
      if (line[i] == ' ' || line[i] == '\n')
      {
        if ((i - old_ind) % 2)
        {
          chet++;
        }
        else
        {
          nechet++;
        }
        old_ind = i;
      }
      if (line[i] != '\n')
      {
        fprintf(fw, "%c", line[i]);
      }
    }
    fprintf(fw, " %d %d\n", nechet, chet);
  }
  fclose(fr);
  fclose(fw);
}

//create_file заполняет файл числами, введенными с клавиатуры 
void create_file() 
{
  printf("Create a file\nInput count of elements: ");
  int n;
  scanf("%d", &n);
  FILE* fw;
  if((fw=fopen("../data/kr2_text2.bin", "wb"))==NULL) 
  {
    printf ("Невозможно открыть файл.\n");
    exit(1);
  }
  printf("Input elements:\n");
  int num;
  for (int i = 0; i < n; i++) 
  {
    scanf("%d", &num);
    fwrite(&num, sizeof(int), 1, fw);
  }
  fclose(fw);
}

// task2 решает задачу 2
void task2()
{
  while(getchar() == ' ') {};
  FILE* f = fopen("../data/kr2_text2.bin", "rb+");
  if (f == NULL)
  {
    printf("Error by input file");
    exit(1);
  }
  fseek(f, 0, SEEK_END);
  int size_f = ftell(f);
  size_f /= sizeof(int);
  fseek(f, 0, SEEK_SET);
  int ind_c = 0;
  int num;
  printf("Input file: ");
  while (ind_c != size_f)
  {
    fread(&num, sizeof(int), 1, f);
    printf("%d ", num);
    ind_c++;
  }
  printf("\n");
  ind_c = 0;
  fseek(f, 0, SEEK_SET);
  int old_ind = 0, new_ind = 0, cnt_del = 0;
  while (ind_c != size_f)
  {
    fread(&num, sizeof(int), 1, f);
    if (!(num % 3))
    {
      cnt_del++; ind_c++;
      continue;
    }
    else
    {
      old_ind = ftell(f);
      fseek(f, new_ind, SEEK_SET);
      fwrite(&num, sizeof(int), 1, f);
      new_ind = ftell(f);
      fseek(f, old_ind, SEEK_SET);
    }
    ind_c++;
  }
  //!!! ftruncate() - изменяет размер файла к (size_f - cnt_del + 1) * sizeof(int) байт. МОЖЕТ БЫТЬ НЕПЕРОНИСИМА 
  ftruncate(fileno(f), (size_f - cnt_del + 1) * sizeof(int));
  fseek(f, new_ind, SEEK_SET);
  fwrite(&cnt_del, sizeof(int), 1, f);

  fclose(f);
  
  f = fopen("../data/kr2_text2.bin", "rb+");
  if (f == NULL)
  {
    printf("Error by input file");
    exit(1);
  }
  fseek(f, 0, SEEK_END);
  int size_file = ftell(f);
  size_file /= sizeof(int);
  fseek(f, 0, SEEK_SET);
  printf("Output file: ");
  for (int i = 0; i < size_file; i++)
  {
    fread(&num, sizeof(int), 1, f);
    printf("%d ", num);
  }
  printf("\n");
  fclose(f);
}


int main()
{
  printf("Which task: input 1 or 2\n");
  int task;
  scanf("%d", &task);
  if (task == 1) 
  {
    task1();
  }
  if (task == 2)
  {
    create_file();
    task2();
  }
  return 0;
}
