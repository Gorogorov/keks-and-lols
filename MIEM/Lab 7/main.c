#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//Функция create_task_a() открывает для записи файл lab7_task_a. Затем она считывает 
//введенные пользователем вещественные числа и записывает их через пробел в 
//открытый файл. В конце функция закрывает файл. Аргумент всего один, это 
//количество вводимых чисел n
void create_task_a(int n) {
  FILE* fw;
  //Открывает файл для записи
  if((fw=fopen("../data/lab7_task_a", "wb"))==NULL) {
    printf ("Невозможно открыть файл.\n");
    exit(1);
  }
  printf("Введите элементы:\n");
  //Считываем все введенные пользователем числа в переменную num, затем записываем
  //их в открытый файл
  float* numbers = (float*) malloc(sizeof(float) * n);
  for (int i = 0; i < n; i++) {
    scanf("%f", &numbers[i]);
  }
  for (int i = 0; i < n; i++) {
    fwrite(&numbers[i], sizeof(float), 1, fw);
  }
  //Закрываем файл
  fclose(fw);
}

//Функция write_task_a() записывает в файл lab7_task_a количество положительный и 
//отрицательных чисел, а затем и сами числа. Эти числа были ранее введены 
//пользователем с клавиатуры. Аргументы: pos_cnt - количество положительных
//чисел, neg_cnt - количество отрицательных чисел, input_data - массив с 
//ранее введенными вещественными числами, n - количество чисел в input_data 
void write_task_a(int pos_cnt, int neg_cnt, float* input_data, int n) {
  FILE* fw;
  //Открываем файл для записи. Если ранее в нем что-то было, то все удалится 
  //и файл создастся заново
  if((fw=fopen("../data/lab7_task_a", "wb"))==NULL) {
    printf ("Невозможно открыть файл.\n");
    exit(1);
  }
  //Записываем количество положительных и отрицательных чисел
  fwrite(&pos_cnt, sizeof(int), 1, fw);
  fwrite(&neg_cnt, sizeof(int), 1, fw);
  //Записываем ранее введенные пользователем числа
  for(int i = 0; i < n; i++) {
    fwrite(&input_data[i], sizeof(float), 1, fw);
  }
  //Закрываем файл
  fclose(fw);

  if((fw=fopen("../data/lab7_task_a", "rb"))==NULL) {
    printf ("Невозможно открыть файл.\n");
    exit(1);
  }
  fread(&pos_cnt, sizeof(int), 1, fw);
  fread(&neg_cnt, sizeof(int), 1, fw);
  printf("\n");
  printf("%d %d ", pos_cnt, neg_cnt);
  for(int i = 0; i < n; i++) {
    fread(&input_data[i], sizeof(float), 1, fw);
    printf("%f ", input_data[i]);
  }
  fclose(fw);
}

//task_a() - основная функция, выполняющая вычислительные операции, требуемые
//в задании (а): посчитать количество положительных и отрицательных чисел 
void task_a(){
  //Ползователь вводит количество элементов
  printf("Введите количество элементов:\n");
  int n;
  scanf("%d", &n);
  //create_task_a(n) - создание файла, считывание введеных пользователем n 
  //элементов и запись их в файл
  create_task_a(n);

  //Открываем только что созданный файл с числами
  FILE* fr;
  if((fr=fopen("../data/lab7_task_a","rw"))==NULL) {
    printf("Невозможно открыть файл");
    exit(1);
  }
  //input_data - массив с числами, которые мы считаем из файла
  float* input_data = (float*) malloc(sizeof(float)*n);
  //pos_cnt - количество положительных чисел, neg_cnt - количество 
  //отрицательных чисел
  int pos_cnt = 0, neg_cnt = 0;
  fread(input_data, 1, sizeof(input_data), fr);
  //Считываем числа из файла. В зависимости от их знака увеличиваем нужный счетчик
  for (int i = 0; i < n; i++) {
    if (input_data[i] < 0) neg_cnt++;
    else if (input_data[i] > 0) pos_cnt++;
  }
  printf("Введенные числа:\n");
  for(int i = 0; i < n; i++) {
    printf("%f ", input_data[i]);
  }
  printf("\nКоличество положительных и отрицательных чисел:");
  printf("\n%d, %d", pos_cnt, neg_cnt);
  printf("\n");
  //Закрываем файл
  fclose(fr);
  
  //write_task_a() - записывает окончательную информацию в файл
  write_task_a(pos_cnt, neg_cnt, input_data, n); 
  
  //Освобождение динамической памяти, выделенной в task_a()
  free(input_data); 
}

//selectionSort() - классическая сортировка выбором. Порядок, в данном случае, задается
//функций strcmp(). result_words - массив со строками, которые нужно отсортировать, 
//n - количество этих строк
void selectionSort(char** result_words, int n) {
  //Цикл до n-1, т.к. последний оставшийся элемент точно встанет на свою позицию 
  for (int i = 0; i < n-1; i++) {
    //min - индекс элемента с минимальным значением
    int min = i;
    //поиск элемента с минимальным значением
    for (int j = i + 1; j < n; j++) {
      //(strcmp(str1, str2) <= 0) <=> str1 лексикографически меньше либо равна str2 
      if (strcmp(result_words[j], result_words[min]) <= 0)
        min = j;
    }
    //Свопаем минимальное найденное слово с i-ым
    //temp_str - вспомогательная переменная для свопа указателей
    char* temp_str = result_words[i];
    result_words[i] = result_words[min];
    result_words[min] = temp_str;
  }
}

//length_of_file() - возвращает количество символов в потоке f
int length_of_file(FILE* f) {
  //Перемещаемся к последнему символу потока f
  fseek(f, 0, SEEK_END);
  //Узнаем, насколько она отличается от начала, записываем результат в length
  int length = ftell(f);
  //Перемещаемся в начало потока f для того, чтобы продолжать с ним работать
  fseek(f, 0, SEEK_SET);
  return length;
}

//Функция read_task_b() считывает из файла lab7_task_b_input все строки,
//записывает их в data
void read_task_b(char* data) {
  //Открываем файл
  FILE* fr;
  if((fr=fopen("../data/lab7_task_b_input", "r"))==NULL) {
    printf ("Невозможно открыть файл.\n");
    exit(1);
  }
  //ctr - текущий индекс в массиве data
  int ctr = 0;
  //Записываем в data, пока не достигнем конца файла
  while (!feof(fr)) {
    data[ctr] = fgetc(fr);
    ctr++;
  }
  //Дописываем символ конца файла
  data[ctr] = '\0';
  //Закрываем файл
  fclose(fr);
}

//Функция write_task_b() записывает в файл lab7_task_b_output выходные данные. 
//result_words - окончательные строки, cnt_str - их количество
void write_task_b(char** result_words, int cnt_str) {
  //Открываем файл
  FILE* fw;
  if((fw=fopen("../data/lab7_task_b_output", "w"))==NULL) {
    printf ("Невозможно открыть файл.\n");
    exit(1);
  }
  //Записываем строки в файл
  for (int i = 0; i < cnt_str; i++) {
    fprintf(fw, "%s\n", result_words[i]);
  }
  //Закрываем файл
  fclose(fw);
}

//task_b() - основная функция, выполняющая вычислительные операции, требуемые
//в задании (b): убрать из каждой строки пробелы в ее начале и конце,
//отсортировать полученные таким образом строки в библиографическом порядке 
void task_b() {
  //Открываем файл для того, чтобы узнать его длину
  FILE* fr;
  if((fr=fopen("../data/lab7_task_b_input", "r"))==NULL) {
    printf ("Невозможно открыть файл.\n");
    exit(1);
  }
  //Узнаем длину с помощью функции length_of_file()
  int length = length_of_file(fr);
  //Закрываем файл
  fclose(fr);

  //input_data - массив со строками, которые будут считаны из входного файла
  char* input_data = (char*) malloc(sizeof(char) * (length + 2));
  //Записываем строки из файла в input_data c помощью read_task_b()
  read_task_b(input_data);

  //data_no_spaces - массив с исходными строками, но с удаленными пробелами
  //в начале и конце каждой из них
  char* data_no_spaces = (char*) malloc(sizeof(char) * (length + 1));
  //ind_in_data - индекс строки, которую обрабатываем в данный момент
  int ind_in_data = 0;
  //beg_str - начало текущей строки, end_str - конец текущей строки
  int beg_str = 0, end_str = 0;
  //max_length_word - максимальная длина строки, cnt_str - количество строк
  int max_length_word = 0, cnt_str = 0;
  //цикл по input_data
  for (int i = 0; i < length; i++) {
    //Если встретили '\n', значит встретили конец строки. Обрабатываем эту строку
    if (input_data[i] == '\n') {
      //Индекс конца строки end_str равен предыдущему индексу
      end_str = i-1;
      //Убираем пробелы из начала строки. Пока не встретим не пробел, 
      //увеличиваем индекс начала строки beg_str
      while (input_data[beg_str] == ' ' && beg_str != end_str) {
        beg_str++;
      }
      //Убираем пробелы из конца строки. Пока не встретим не пробел, 
      //уменьшаем индекс конца строки end_str
      while (input_data[end_str] == ' ' && beg_str != end_str) {
        end_str--;
      }
      //Ищем максимальную длину слова
      if (max_length_word < (end_str - beg_str)) {
        max_length_word = (end_str - beg_str);
      }
      //Записываем полученную строку, без пробелов в начале и конце, в массив с 
      //такими строками data_no_spaces
      while(end_str >= beg_str) {
        data_no_spaces[ind_in_data] = input_data[beg_str];
        beg_str++;
        ind_in_data++;
      }
      //Последним символом записываем обозначение конца строки '\n'
      data_no_spaces[ind_in_data] = '\n';
      ind_in_data++;
      //Начало новой строки - элемент, следующий за '\n'
      beg_str = i+1;
      //Увеличиваем счетчик количества строк
      cnt_str++;
    }
  }

  //result_words - массив со строками без пробелов
  //Выделяем на него память
  char** result_words = (char**)malloc(sizeof(char*) * cnt_str);
  for (int i = 0; i < cnt_str; i++) {
    result_words[i] = (char*) malloc(sizeof(char) * (max_length_word+2));
  }
  //ind_str - номер обрабатываемой строки, ind_let - номер обрабатываемого
  //символа в обрабатываемой строке
  int ind_str = 0, ind_let = 0;
  for (int i = 0; i < ind_in_data; i++) {
    //Если встретили '\n', начинаем обрабатывать новую строку
    if (data_no_spaces[i] == '\n') {
      result_words[ind_str][ind_let] = '\0';
      ind_str++;
      ind_let = 0;
    }
    //Иначе дописываем новый символ в конец текущей строки
    else {
      result_words[ind_str][ind_let] = data_no_spaces[i];
      ind_let++;
    }
  }
  //Сортируем строки в лексикографическом порядке result_words
  selectionSort(result_words, cnt_str);
  //Записываем полученные строки result_words в выходной файл
  write_task_b(result_words, cnt_str);
  //Освобождаем выделенную в task_b() динамическую память
  free(data_no_spaces);
  free(input_data);
  for(int i = 0; i < cnt_str; i++) {
    free(result_words[i]);
  }
  free(result_words);
}

int main() {
  printf("Данная программа решает 2 задачи.\na) Первая из них заключается в том, \
чтобы посчитать количество положительных и отрицательных вещественных \
чисел. Эти два числа дописываются в начало файла с числами. Пользователь \
вводить в консоль количество чисел и сами числа, затем программа создает \
файл с этими числами и выполняет поставленную задачу.\nb) Вторая задача \
состоит в том, чтобы из файла с несколькими строками убрать пробелы в \
начале и конце каждой из строк, затем отсортировать получившиеся строки \
в лексикографическом порядке. Полученный текст записать в новый файл.\n");
  printf("Какую задачу решать: a или b?\n");
  //task - символ, который вводит пользователь для определения того, какую 
  //задачу решать
  char task;
  scanf("%c", &task);
  if (task == 'a'){
    task_a();
  }
  else if (task == 'b') {
    task_b();
  }
  return 0;
}
