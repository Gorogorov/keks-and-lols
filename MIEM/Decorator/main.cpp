#include <iostream>
#include <string>

class Cat {
public:
	std::string description;
	std::string get_description() {
		return description;
	}
	virtual double power() = 0;
};

class Decorator : public Cat {
	virtual std::string get_description() = 0;
};

class Pupil : public Cat {
public:
	Pupil(){
		description = "Pupil";
	}
	double power() {
		return 1;
	}
};

class Master : public Cat {
public:
	Master(){
		description = "Master";
	}
	double power() {
		return 3;
	}
};

class Grandmaster : public Cat {
public:
	Grandmaster(){
		description = "Grandmaster";
	}
	double power() {
		return 5;
	}
};

class Armor : public Decorator {
private:
	Cat *cat;
public:
	Armor(Cat *c) : cat(c) {
		description = cat->get_description() + ", armor";
	}
	std::string get_description() {
		return description;
	}
	double power() {
		return cat->power() + 5;
	}
};

class Sword : public Decorator {
private:
	Cat *cat;
public:
	Sword(Cat *c) : cat(c) {
		description = cat->get_description() + ", sword";
	}
	std::string get_description() {
		return description;
	}
	double power() {
		return cat->power() + 10;
	}
};

class Staff : public Decorator {
private:
	Cat *cat;
public:
	Staff(Cat *c) : cat(c) {
		description = cat->get_description() + ", staff";
	}
	std::string get_description() {
		return description;
	}
	double power() {
		return cat->power() + 100;
	}
};

int main() {
	Cat *cat1 = new Pupil();
	cat1 = new Armor(cat1);
	cat1 = new Sword(cat1);
	std::cout << "-----\n" << cat1->get_description() << "\nPower: " << cat1->power() << "\n-----\n"; 
	Cat *cat2 = new Master();
	cat2 = new Sword(cat2);
	cat2 = new Sword(cat2);
	std::cout << "-----\n" << cat2->get_description() << "\nPower: " << cat2->power() << "\n-----\n";
	Cat *cat3 = new Grandmaster();
	cat3 = new Armor(cat3);
	cat3 = new Staff(cat3);
	std::cout << "-----\n" << cat3->get_description() << "\nPower: " << cat3->power() << "\n-----\n";
	return 0;
}