#pragma once

#include <random>
#include <math.h>
#include <ctime>
#include <iostream>

#define RESET   "\033[0m"
#define RED     "\033[1;31m"
#define GREEN  "\033[1;32m"
#define WHITE   "\033[1;37m"

class Player {
private:
	int pos;
public:
	Player() : pos(0) {}
	void step();
	bool is_win(int n);
	int get_pos() const;
};

class PrintField {
private:
	int n;
	Player *player1, *player2;
public:
	PrintField(int m, Player* p1, Player* p2) : n(m), player1(p1), player2(p2) {}
	void print();
};
