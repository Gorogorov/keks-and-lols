#include <iostream>
#include <iomanip>
#include <thread>
#include <mutex>
#include <condition_variable>
#include "../include/player.h"

//Lab 6, variant 13

int win_finder = -1;
std::mutex game_Mutex;

auto start_game = [](Player* player, int n, PrintField* pf, int num) {
	while(win_finder == -1) {
		if (game_Mutex.try_lock()) {
			player->step();
			if (player->is_win(n)) {
				win_finder = num;
			}
			pf->print();
			std::this_thread::sleep_for(std::chrono::seconds(1));
			game_Mutex.unlock();
		}
	}
};

int main() {
	srand(time(0));
	Player* player1 = new Player();
	Player* player2 = new Player();
	std::cout << "Input amount of cells:\n";
	int n;
	std::cin >> n;
	PrintField* pf = new PrintField(n, player1, player2);
	pf->print();

	std::thread FirstPlayerThread(start_game, player1, n, pf, 1);
	std::thread SecondPlayerThread(start_game, player2, n, pf, 2);

	FirstPlayerThread.join();
	SecondPlayerThread.join();

	std::cout << "Winner is player " << win_finder << "!\n";
	return 0;
}