#include "../include/player.h"
	
void Player::step() {
	int dice_roll = 1 + (rand()%6);
	pos += dice_roll;
	if (!(pos%17)) {
		pos *= 2;
	}
	if (!(pos%10)) {
		pos += 3;
	}
	else if (!(pos%5)) {
		pos -= 3;
	}
	else if (!(pos%14)) {
		pos += 2;
	}
	else if (!(pos%7)) {
		pos -= 2;
	}
	else if (!(pos%6)) {
		pos += 1;
	}
	else if (!(pos%3)) {
		pos -= 1;
	}
}
	
bool Player::is_win(int n){
	return pos >= n;
}

int Player::get_pos() const {
	return pos;
}

void PrintField::print(){
	int pos1 = std::min(player1->get_pos(), n);
	int pos2 = std::min(player2->get_pos(), n);
	for (int i = 0; i <= n; i++) {
		if (i != pos1 && i != pos2) {
			std::cout << WHITE << "X" << RESET;
		}
		else if (i == pos1 && i == pos2) {
			std::cout << RED << "3" << RESET;
		}
		else if (i == pos1) {
			std::cout << GREEN << "1" << RESET;
		}
		else if (i == pos2) {
			std::cout << GREEN << "2" << RESET;
		}
	}
	std::cout << "\n";
}