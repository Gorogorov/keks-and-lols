#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>

using namespace std;

void print(vector<vector<double>> V) {
	for (int i = 0; i < V.size(); i++) {
		if (i) cout << i << ": ";
		for (int j = 0; j < V[i].size(); j++) {
			cout << V[i][j] << " ";
		}
		cout << "\n";
	}
}

double fRand(double fMin, double fMax)
{
    double f = (double)rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

void fill(vector<vector<double>>& V) {
	for (int i = 0; i < V.size()-1; i++) {
		V[0].push_back(i+1);
	}
	for (int i = 0; i < V.size()-1; i++) {
		for (int j = 0; j < V.size()-1; j++) {
			V[i+1].push_back(fRand(-1000, 1000));
		}
	}
	print(V);
}

int main(){
	srand(time(NULL));
	int n;
	cin >> n;
	vector<vector<double>> V(n+1);

	fill(V);

	ofstream fout("output.csv");
	for (auto vctr : V) {
		for (int i = 0; i < vctr.size()-1; i++) {
			fout << vctr[i] << ";";
		}
		fout << vctr[vctr.size()-1] << "\n";
	}
	fout.close();
	return 0;
}