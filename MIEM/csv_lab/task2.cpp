#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>

using namespace std;

void print(vector<vector<string>> V) {
	for (int i = 0; i < V.size(); i++) {
		if (i) cout << i << ": ";
		for (int j = 0; j < V[i].size(); j++) {
			cout << V[i][j] << " ";
		}
		cout << "\n";
	}
}

void fill(const string& s, vector<string>& V) {
	stringstream ss(s);
	string item;
	vector<string> elems;
	while(getline(ss, item, ';')) {
		V.push_back(item);
	}
}

// Объединение множеств 
void set(vector<vector<string>> V1, vector<vector<string>> V2, ofstream& stream) {
	vector<vector<string>> V(V1.size());
	for (int i = 0; i < V1[0].size(); i++) {
		V[0].push_back(V1[0][i]);
	}
	for (int i = 1; i < V1.size(); i++) {
		for (int j = 0; j < V1[i].size(); j++) {
			V[i].push_back(V1[i][j]);
		}
	}
	for (int i = 1; i < V2.size(); i++) {
		bool equal = false;
		for (int j = 1; j < V1.size(); j++) {
			bool check = true;
			for (int k = 0; k < V2[0].size(); k++) {
				if (V2[i][k] != V1[j][k]) {
					check = false;
					break;
				}
			}
			if (check) {
				equal = true;
			}
		}
		if (equal) continue;
		V.push_back(vector<string>());
		for (int j = 0; j < V2[i].size(); j++) {
			V[V.size()-1].push_back(V2[i][j]);
		}
	}
	for (auto vctr : V) {
		for (int i = 0; i < vctr.size()-1; i++) {
			stream << vctr[i] << ";";
		}
		stream << vctr[vctr.size()-1] << "\n";
	}
}

int main(){

	ofstream fout_1("input_t2_1.csv");
	fout_1 << "Фамилия;Имя;Группа;Оценка\n";
	fout_1 << "Иванов;Michael;Сергеевич;10\n";
	fout_1 << "Петров;Иван;Александрович;0\n";
	fout_1.close();

	ofstream fout_2("input_t2_2.csv");
	fout_2 << "Фамилия;Имя;Группа;Оценка\n";
	fout_2 << "Иванов;Michael;Retrt;10\n";
	fout_2 << "Петров;Иван;Александрович;1\n";
	fout_2 << "Петров;Иван;Александрович;0\n";
	fout_2.close();

	string str;

	ifstream fin_1("input_t2_1.csv");
	vector<vector<string>> V1;
	while(getline(fin_1, str)) {
		V1.push_back(vector<string>());
		fill(str, V1[V1.size()-1]);
	}
	fin_1.close();

	ifstream fin_2("input_t2_2.csv");
	vector<vector<string>> V2;
	while(getline(fin_2, str)) {
		V2.push_back(vector<string>());
		fill(str, V2[V2.size()-1]);
	}
	fin_2.close();
	ofstream fout("output.csv");
	set(V1, V2, fout);
	/*for (auto vctr : V) {
		for (int i = 0; i < vctr.size()-1; i++) {
			fout2 << vctr[i] << ";";
		}
		fout2 << vctr[vctr.size()-1] << "\n";
	}*/
	fout.close();
	return 0;
}