#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <sstream>

using namespace std;

void print(vector<vector<string>> V) {
	for (int i = 0; i < V.size(); i++) {
		if (i) cout << i << ": ";
		for (int j = 0; j < V[i].size(); j++) {
			cout << V[i][j] << " ";
		}
		cout << "\n";
	}
}

void fill(const string& s, vector<string>& V) {
	stringstream ss(s);
	string item;
	vector<string> elems;
	while(getline(ss, item, ';')) {
		V.push_back(item);
	}
}

string id_find(vector<vector<string>> V, int row, int col) {
	if (row < 1 || col < 1 || V.size() <= row || V[0].size() < col){
		cout << "Illegal index\n";
		return "";
	}
	if (!V[row][col-1].size()) {
		cout << "Empty cell\n";
		return "";
	}
	return V[row][col-1];
}

void v_stack(vector<vector<string>>& V, int ind) {
	if (ind < 1 || ind > V.size()-1) {
		cout << "Illegal index\n";
		return;
	}
	V.push_back(vector<string>(V[0].size()));
	swap(V[ind+1], V[V.size()-1]);
}

void h_stack(vector<vector<string>>& V, int ind) {
	if (ind < 0 || ind > V.size()) {
		cout << "Illegal index\n";
		return;
	}
	for (int i = 0; i < V.size(); i++) {
		V[i].push_back("");
		for (int j = V[i].size()-1; j > ind; j--) {
			V[i][j] = V[i][j-1];
		}
		V[i][ind] = "";
	}
} 

void show_row(vector<vector<string>> V, int ind) {
	if (ind < 1 || ind > V.size()-1) {
		cout << "Illegal index\n";
		return;
	}
	cout << ind << " row:\n";
	for (int i = 0; i < V[ind].size(); i++) {
		cout << V[ind][i] << " ";
	}
	cout << "\n";
}

void show_col(vector<vector<string>> V, int ind) {
	if (ind < 1 || ind > V.size()) {
		cout << "Illegal index\n";
		return;
	}
	cout << ind << " col:\n";
	for (int i = 1; i < V.size(); i++) {
		cout << V[i][ind-1] << "\n";
	}
}

int main(){

	ofstream fout("input.csv");
	fout << "Фамилия;Имя;Группа;Оценка\n";
	fout << "Иванов;Michael;Сергеевич;10\n";
	fout << "Петров;Иван;Александрович;0\n";
	fout.close();

	ifstream fin("input.csv");
	vector<vector<string>> V;
	string str;
	while(getline(fin, str)) {
		V.push_back(vector<string>());
		fill(str, V[V.size()-1]);
	}

	//print(V);

	cout << "Find by id (1, 3): " << id_find(V, 1, 3) << "\n";
	v_stack(V, 2);
	//print(V);
	h_stack(V, 4);
	//print(V);
	show_row(V, 2);
	show_col(V, 2);
	fin.close();

	ofstream fout2("output.csv");
	for (auto vctr : V) {
		for (int i = 0; i < vctr.size()-1; i++) {
			fout2 << vctr[i] << ";";
		}
		fout2 << vctr[vctr.size()-1] << "\n";
	}
	fout2.close();
	return 0;
}