#include <stdio.h>
#include <stdlib.h>
#include <math.h>

typedef struct year {
  int year, cnt_days;
  float max_temp, min_temp;
  float* days_temp;
} year;

int main() {
  FILE* f;
  f = fopen("../data/stockholm_td_adj.dat", "r");
  int cnt_year = 0, cur_year = 0;
  while (!feof(f)) {
    int year, month, day, number_1;
    float T1, T2, T3;
    fscanf(f, "%d%d%d%f%f%f%d", &year, &month, &day, &T1, &T2, &T3, &number_1);
    if (cur_year != year) {
      cur_year = year;
      cnt_year++;
    }
  }
  fclose(f);
  year* years = (year*) malloc(sizeof(year) * cnt_year);
  for (int i = 0; i < cnt_year; i++) {
    years[i].days_temp = (float*) malloc(sizeof(float) * 366);
  }

  f = fopen("../data/stockholm_td_adj.dat", "r");
  cur_year = -1;
  int ind_year = -1;
  while (!feof(f)) {
    int year, month, day, number_1;
    float T1, T2, T3;
    fscanf(f, "%d%d%d%f%f%f%d", &year, &month, &day, &T1, &T2, &T3, &number_1);
    if (cur_year != year) {
      cur_year = year;
      ind_year++;
      years[ind_year].year = cur_year;
      years[ind_year].cnt_days = -1;
    }
    years[ind_year].cnt_days++;
    years[ind_year].days_temp[years[ind_year].cnt_days] = T1;
  }
  fclose(f);

  for (int i = 0; i < cnt_year; i++) {
    years[i].max_temp = -1000.0;
    years[i].min_temp = 1000.0;
    for(int j = 0; j < years[i].cnt_days; j++) {
      if (floor(years[i].days_temp[j]) < years[i].min_temp) {
        years[i].min_temp = floor(years[i].days_temp[j]);
      }
      if (ceil(years[i].days_temp[j]) > years[i].max_temp) {
        years[i].max_temp = ceil(years[i].days_temp[j]);
      }
    }
  }
  printf("Введите год от %d до %d\n", years[0].year, years[cnt_year-1].year);
  int input_year;
  scanf("%d", &input_year);
  if (input_year > years[cnt_year-1].year || input_year < years[0].year) {
    printf("Год не лежит в необходимом промежутке\n");
    return 0;
  }
  input_year -= years[0].year;
  int cnt_gaps = (int)ceil((years[input_year].max_temp - years[input_year].min_temp) / 5);
  int* cnt_temper = (int*) malloc(sizeof(int) * cnt_gaps);
  for (int i = 0; i < cnt_gaps; i++) {
    cnt_temper[i] = 0;
  }
  for (int i = 0; i < cnt_gaps; i++) {
    printf("%d [%d, %d)\n", i, (int)years[input_year].min_temp + 5*i, (int)years[input_year].min_temp + 5 + 5*i);
  }
  for (int i = 0; i < years[input_year].cnt_days; i++) {
    int is_find = 0, ind_gap = 0;
    while (!is_find) {
      if (years[input_year].days_temp[i] < (years[input_year].min_temp + 5 + 5*(ind_gap))) {
        cnt_temper[ind_gap]++;
        is_find = 1;
      }
      else {
        ind_gap++;
      } 
    }
  }
  printf("\n");
  for (int i = 0; i < cnt_gaps; i++) {
    int cnt_ht = (int)(cnt_temper[i]/5);
    printf("%d ", i);
    for (int i = 0; i < cnt_ht; i++) {
      printf("#");
    }
    printf("\n");
  }
  return 0;
}
