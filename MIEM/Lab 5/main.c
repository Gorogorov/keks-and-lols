#include <stdlib.h>
#include <stdio.h>
#include <string.h>

// Функция, выполняющая поставленную задачу: принимает на вход массив input_str
// со словами, которые разделены запятой, после последнего слова точка. 
// Функция должна записать в result_words все слова, которые употребляются больше одного раза
// Вывод данной функции - количество слов, которые она записала в result_words
int executable_program(char* input_str, char** result_words) {
  //word_array - массив со словами, которые употреблены хотя бы единожды
  char** word_array = (char**) malloc(sizeof(char*) * 30);
  //count_using - массив с числа, которые обозначают то, сколько раз слово было употреблено
  //count_using[i] - сколько раз было употреблено word_array[i]
  int* count_using = (int*) malloc(sizeof(int) * 30);
  //инициализирование массивов
  for (int i = 0; i < 30; i++) {
    word_array[i] = (char*) malloc(sizeof(char) * 11);
    count_using[i] = 0;
  }
  //cur_word - текущее слово, т.е. слово, которое в данный момент обрабатывается функцией strtok().
  //Выделение первого слова во входной строке input_str с помощью функци strtok(). 
  //Разделители - те, которые указаны в задаче
  char* cur_word = strtok (input_str,",.");
  //ind - текущий индекс в массиве word_array
  int ind = 0; 
  // cur_word == NULL <=> все слова во входящей строке обработаны
  while (cur_word != NULL) {
      //ind_in_past - индекс слова в массиве word_array. Если индекс = -1,
      //мы еще не обработали данное слово, т.е. оно встречется впервые
      int ind_in_past = -1;
      //поиск слова в массиве word_array с уже обработанными словами
      for (int i = 0; i < ind; i++) {
        //Если текущее слово cur_word и некоторое слово word_array[i] равны,
        //strcmp() вернет 0
        if (!strcmp(cur_word, word_array[i])) {
          ind_in_past = i;
        }
      }
      //Если ind_in_past != -1, текущее слово встречается не впервые.  
      if (ind_in_past != -1) {
        //увеличиваем количество употреблений данного слова в массиве
        //count_using с количествами использований употребленных слов
        count_using[ind_in_past]++;
      }
      //Если ind_in_past == -1, слово встретилось впервые
      else {
        //Записываем его в массив с употребленными словами word_array с текущим индексом ind
        strcpy(word_array[ind], cur_word);
        //Количество употреблений данного слова меняем с нуля на единицу
        count_using[ind]++;
        //Увеличиваем текущий индекс на 1
        ind++;
      }
      //strtok() изменяет текущее слово cur_word на следующее во входной строке input_str
      cur_word = strtok(NULL, ",.");
  }
  //ind_in_result - индекс в массиве result_words со словами, которые употреблены не единожды 
  int ind_in_result = 0;
  //Цикл до ind - количества различных слов во входной строке
  for (int i = 0; i < ind; i++) { 
    // count_using[i] > 1 <=> слово word_array[i] было употреблено не единожды
    if (count_using[i] > 1) {
      //strcpy() копирует слово word_array[i] в массив result_words
      //по текущему индеку ind_in_result
      strcpy(result_words[ind_in_result], word_array[i]);
      //Увеличиваем текущий индекс на 1
      ind_in_result++;
    }
  }
  //Освобождение выделенной динамической памяти
  for (int i = 0; i < 30; i++) {
    free(word_array[i]);
  }
  free(word_array);
  free(count_using);
  free(cur_word);
  //Возвращение количества слов в массиве result_words, т.е. в массиве со словами, 
  //которое употреблены не единожды
  return ind_in_result;
}

//Если пользователь согласится на тестирование, то будут проведены 3 теста с уже заданными строками
//Правильный ответ для этих тестов был определен эмпирическим путем
void testing(){
  //Выделение для массива result_words. Он выполняет такую же функцию, как и в 
  //в executable_program()
  char** result_words = (char**) malloc(sizeof(char*) * 30);
  for (int i = 0; i < 30; i++) {
    result_words[i] = (char*) malloc(sizeof(char) * 11);
  }
  
  //Тест 1
  printf("\nТест 1:\nИсходная строка: ");
  //succes - переменная, которая указывает, совпадают ли результаты теста с истинными результатами
  //Изначально она равна 1. Измененение ее значения на 0 - несовпадение тестовых и истинных результатов
  int success = 1;
  //input_str1 - первый тестовый массив со словами. Можно увидеть, что дважды употреблены только
  //два слова: 'the' и 'book'  
  char input_str1[] = "to,add,the,finis,touches,he,arched,natural,history,for,the,animals,presented,in,the,book,and,then,had,the,book.";
  printf("%s\n", input_str1);
  //count_res_words - сколько слов употреблено единожды по версии функции executable_program(),
  //т.е. сколько слов оказалось в массиве result_words
  int count_res_words = executable_program(input_str1, result_words);
  printf("Повторяющиеся больше одного раза слова:\n");
  //Если слово оказалось в массиве result_words, то оно употрблено единожды.
  //Для удобства пользователя напечатаем его
  for (int i = 0; i < count_res_words; i++) {
    printf("%s ", result_words[i]);
  }
  //Убеждаемся, что количество употребленных единожды слов по мнению функции executable_program()
  //и по нашему мнению одинаково
  if (count_res_words != 2) {
    success = 0;
  }
  //Убеждаемся, что первое найденное слово 'the'. Оно будет именно первым, т.к. в массиве input_str1 
  //оно впервые употребляется перед тем, как впервые употребляется слово 'book'
  if (strcmp(result_words[0], "the")) {
    success = 0;
  }
  //Убеждаемся, что второе найденное слово 'book'
  if (strcmp(result_words[1], "book")) {
    success = 0;
  }
  printf("\nТестирование №1 - ");
  //Если succes == 0, найдено какое-либо отличие тестовых результатов от истинных
  //\033[1;31m \033[0m - данные символы отвечают за то, чтобы вывод в консоли был красным цветом
  if (!success) {
    printf("\033[1;31mПровалено\033[0m\n");
  }
  //Иначе все верно
  //\033[1;32m \033[0m - данные символы отвечают за то, чтобы вывод в консоли был зеленым цветом
  else {
    printf("\033[1;32mУспешно\033[0m\n");
  }
  //Второй и третий тест абсолютно аналогичны первому. Так что, по моему мнению, не имеет смысла
  //их комментировать
  //Тест 2
  printf("\nТест 2:\nИсходная строка: ");
  success = 1;
  char input_str2[] = "Alice,think,it,so,very,much,out,of,the,way,to,hear,the,Rabbit,say,to,itself\
,Oh,dear,Oh,dear,I,shall,be,too,late.";
  printf("%s\n", input_str2);
  count_res_words = executable_program(input_str2, result_words);
  printf("Повторяющиеся больше одного раза слова:\n");
  for (int i = 0; i < count_res_words; i++) {
    printf("%s ", result_words[i]);
  }
  if (count_res_words != 4) {
    success = 0;
  }
  if (strcmp(result_words[0], "the")) {
    success = 0;
  }
  if (strcmp(result_words[1], "to")) {
    success = 0;
  }
  if (strcmp(result_words[2], "Oh")) {
    success = 0;
  }
  if (strcmp(result_words[3], "dear")) {
    success = 0;
  }
  printf("\nТестирование №2 - ");
  if (!success) {
    printf("\033[1;31mПровалено\033[0m\n");
  }
  else {
    printf("\033[1;32mУспешно\033[0m\n");
  }

  //Тест 3
  printf("\nТест 3:\nИсходная строка: ");
  success = 1;
  char input_str3[] = "I,am,sure,those,are,not,the,right,words";
  printf("%s\n", input_str3);
  count_res_words = executable_program(input_str3, result_words);
  printf("Повторяющиеся больше одного раза слова:\n");
  for (int i = 0; i < count_res_words; i++) {
    printf("%s ", result_words[i]);
  }
  if (count_res_words != 0) {
    success = 0;
  }
  printf("\nТестирование №3 - ");
  if (!success) {
    printf("\033[1;31mПровалено\033[0m\n");
  }
  else {
    printf("\033[1;32mУспешно\033[0m\n");
  }
  //освобождение всей динамической памяти, выделенной в testing()
  for (int i = 0; i < 30; i++) {
    free(result_words[i]);
  }
  free(result_words);
}

int main() {
  //input_str - входящая строка, begin_str - это строка, в которую скопируется
  //begin_str. begin_str нужна для того, чтобы по требованию пользователя 
  //выводить исходную строку. input_str вывести в этом случае невозможно,
  //т.к. strtok() ее преобразует
  char* input_str = (char*) malloc(sizeof(char) * (30*11 + 100));
  char* begin_str = (char*) malloc(sizeof(char) * (30*10 + 100));
  //result_words - массив со словами, которые были употреблены чаще, 
  //чем единожды
  char** result_words = (char**) malloc(sizeof(char*) * 30);
  for (int i = 0; i < 30; i++) {
    result_words[i] = (char*) malloc(sizeof(char) * 11);
  }
  // 30 - максимальное количество слов,
  // 10 - максимальное количество символов в них (+1 для возможного '\0'), 
  // 100 - максимальное количество символов под разделитители
  // (с учетом того, что между словами всего по одной запятой, это количество даже преувеличено)


  printf("Введите строку\nМаксимальное количество слов - 30\nМаксимальное количество символов в слове - 10\n");
  printf("Между соседними словами должна стоять запятая, за последним словом точка\n");
  printf("Программа выведет все слова, которые встречаются не по одному разу\n");
  //считываем вводимую пользователем строку в input_str, копируем ее в begin_str 
  scanf("%s", input_str);
  strcpy(begin_str, input_str);
  
  //count_res_words - количество слов, записанное в result_words, т.е. количество слов 
  //в input_str, которые употреблены чаще, чем единожды
  int count_res_words = executable_program(input_str, result_words);
  
  printf("Итог:\n");
  //выводим нужные по условию слова
  for (int i = 0; i < count_res_words; i++) {
    printf("%s ", result_words[i]);
  }
  printf("\n");
  printf("Хотите ли вы вывести исходную строку?\ny - да, n - нет\n");
  //is_repeat - флаг, который говорит о том, хочет ли пользователь 
  //увидеть на экране введенную строку
  char is_repeat;
  scanf("%s", &is_repeat);
  //если пользователь написал 'y' (yes), то выводим
  if (is_repeat == 'y') {
    printf("%s\n", begin_str);
  }

  printf("Хотите ли вы протестировать программу?\ny - да, n - нет\n");
  //is_test - флаг, который говорит о том, хочет ли пользователь 
  //увидеть на экране вывод функции testing(), т.е. хочет ли пользователь запустить тесты, 
  //встроенные в программу
  char is_test;
  scanf("%s", &is_test);
  if (is_test == 'y') {
    printf("\n---\n");
    testing();
  }
  //освобождение всей динамической памяти, выделенной в main()
  for (int i = 0; i < 30; i++) {
    free(result_words[i]);
  }
  free(input_str);
  free(begin_str);
  free(result_words);
  return 0;
}
