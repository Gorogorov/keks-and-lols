#include <iostream>
#include <fstream>
#include <vector>

using namespace std;

class Matrix {
private:
	vector<vector<double> > matrix;
	int cols, rows;
public:
	Matrix();
	void print();
	void change();
	//~Matrix();
};

Matrix::Matrix() {
	vector<vector<double> >::iterator it1;
	vector<double>::iterator it2;
	ifstream in("input.txt");
	streambuf *cinbuf = cin.rdbuf();
	cin.rdbuf(in.rdbuf());
	cin >> rows >> cols;
	for (int i = 0; i < rows; i++) {
		matrix.push_back(vector<double>());
		for (int j = 0; j < cols; j++) {
			matrix[i].push_back(0);
		}
	}
	for (it1 = matrix.begin(); it1 != matrix.end(); it1++) {
		for (it2 = (*it1).begin(); it2 != (*it1).end(); it2++) {
			cin >> (*it2);
		}
	}
	cin.rdbuf(cinbuf);
	in.close();
}

void Matrix::print() {
	vector<vector<double> >::iterator it1;
	vector<double>::iterator it2;
	ofstream out("output.txt");
	streambuf *coutbuf = cout.rdbuf();
	cout.rdbuf(out.rdbuf());
	for (it1 = matrix.begin(); it1 != matrix.end(); it1++) {
		for (it2 = (*it1).begin(); it2 != (*it1).end(); it2++) {
			cout << (*it2)  << " ";
		}
		cout << "\n";
	}
	cout.rdbuf(coutbuf);
	out.close();
}

void Matrix::change() {
	vector<vector<double> >::iterator it1;
	vector<double>::iterator it2;
	for (it1 = matrix.begin(); it1 != matrix.end(); it1++) {
		double max_d = -1000000;
		for (it2 = (*it1).begin(); it2 != (*it1).end(); it2++) {
			if (*(it2) > max_d) max_d = *it2;
		}
		for (it2 = (*it1).begin(); it2 != (*it1).end(); it2++) {
			if (*(it2) == max_d) *it2 = 0;
		}
	}
}

int main() {
	Matrix M;
	M.print();
	M.change();
	//M.print();
	return 0;
}