#include <iostream>
#include <exception>

using namespace std;

struct Drug {
	string name, substance, dosage, form;
	int amount, price;
};

void drug_input(Drug& d) {
	try {
		cout << "Creation new drug\n";
		cout << "Input name: ";
		cin >> d.name;
		cout << "Input substance: ";
		cin >> d.substance;
		cout << "Input dosage: ";
		cin >> d.dosage;
		cout << "Input form(pills/ampoule/ointment): ";
		cin >> d.form;
		if (d.form != "pills" && d.form != "ampoule" && d.form != "ointment") throw runtime_error("Illegal form");
		cout << "Input amount: ";
		cin >> d.amount;
		if (cin.fail()) throw runtime_error("Found string, expected integer");
		cout << "Input price: ";
		cin >> d.price;
		if (cin.fail()) throw runtime_error("Found string, expected integer");
	}
	catch(exception& e) {
		cout << e.what() << "\n";
		throw e;
	}
}

bool check_substance(Drug d, string substance) {
	if (d.substance == substance) return true;
	else return false;
}

double price_by_one(Drug d) {
	return 1.0*d.price/d.amount;
}

void drug_output(Drug d) {
	cout << "Name: " << d.name << "\n";
	cout << "Substance: " << d.substance << "\n";
	cout << "Dosage: " << d.dosage << "\n";
	cout << "Form: " << d.form << "\n";
	cout << "Amount: " << d.amount << "\n";
	cout << "Price: " << d.price << "\n";
}

int main() {
	Drug d;
	string ts;
	drug_input(d);
	cout << "Check substance\nInput test substance: ";
	cin >> ts;
	cout << (check_substance(d, ts) ? "equal" : "not equal")  << "\n";
	cout << "Price by one: " << price_by_one(d) << "\n";
	cout << "Your drug:\n";
	drug_output(d);
	return 0;
}