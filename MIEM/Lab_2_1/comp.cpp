#include <iostream>
#include <cmath>
#include <exception>

using namespace std;

struct Complex {
	double re, im;

	Complex() {}

	Complex(double x, double y) : re(x), im(y) {}

	// Аргумент конструктора коипрования должен быть const, без него этот пример не собирается под g++
	// https://stackoverflow.com/questions/46743898/what-is-this-strange-copy-constructor-error-complaining-about
	// https://stackoverflow.com/questions/16956693/why-c-copy-constructor-must-use-const-object
	Complex(const Complex &a) : re(a.re), im(a.im) {}

	Complex& operator=(const Complex &a){
		this->re = a.re;
		this->im = a.im;
		return *this;
	}

	Complex operator+(Complex c);
	Complex operator-(Complex c);
	Complex operator*(Complex c);
	Complex operator/(Complex c);

	double abs();
	double abs(Complex a);
	Complex conj();
	Complex conj(Complex a);
	Complex inversive();
	Complex inversive(Complex a);

	~Complex() { }
};

double Complex::abs() {
	return sqrt(re*re + im*im);
}

double Complex::abs(Complex a) {
	return sqrt(a.re*a.re + a.im*a.im);
}

Complex Complex::conj() {
	return Complex(re, -im);
}

Complex Complex::conj(Complex a) {
	return Complex(a.re, -a.im);
}

Complex Complex::inversive() {
	if (abs() == 0) throw runtime_error("Division by zero");
	return Complex(re/(abs()*abs()), -im/(abs()*abs()));
}

Complex Complex::inversive(Complex a) {
	if (a.abs() == 0) throw runtime_error("Division by zero");
	return Complex(a.re/(a.abs()*a.abs()), -a.im/(a.abs()*a.abs()));
}

Complex Complex::operator+(Complex a) {
	return Complex(re + a.re, im + a.im);
}

Complex Complex::operator-(Complex a) {
	return Complex(re - a.re, im - a.im);
}

Complex Complex::operator*(Complex a) {
	return Complex(re*a.re - im*a.im, re*a.im + im*a.re);
}

Complex Complex::operator/(Complex a) {
	if (abs(a) == 0) throw runtime_error("Division by zero");
	Complex c = *this * conj(a);
	c.im/=(abs(a)*abs(a));
	c.re/=(abs(a)*abs(a));
	return c;
}

int main()
{
	Complex z1, z2, z3, z4, z5, z6, z7, z8, z9, z0;
	z1.re = 5.09;
	z2.re = 5.9;
	z1.im = 5.1;
	z2.im = -5.9;
	z0.re = 0;
	z0.im = 0;

	cout << "z1 = " << z1.re << (z1.im > 0 ? "+" : "") << z1.im << "i" << endl;
	cout << "z2 = " << z2.re << (z2.im > 0 ? "+" : "") << z2.im << "i" << endl;

	z3 = z1 + z2;
	cout << "z1+z2 = " << z3.re << (z3.im > 0 ? "+" : "") << z3.im << "i" << endl;

	z4 = z1 - z2;
	cout << "z1-z2 = " << z4.re << (z4.im > 0 ? "+" : "") << z4.im << "i" << endl;

	z5 = z1*z2;
	cout << "z1*z2 = " << z5.re << (z5.im > 0 ? "+" : "") << z5.im << "i" << endl;

	z6 = z1/ z2;
	cout << "z1/z2 = " << z6.re << (z6.im > 0 ? "+" : "") << z6.im << "i" << endl;

	z7 = z1.conj();
	cout << "Conjugate to z1 = " << z7.re << (z7.im > 0 ? "+" : "") << z7.im << "i" << endl;

	z8 = z1.inversive();
	cout << "z1^{-1} = " << z8.re << (z8.im > 0 ? "+" : "") << z8.im << "i" << endl;

	// Division by zero
	//z6 = z2/z0;
	//z0.inversive();

	return 0;
}