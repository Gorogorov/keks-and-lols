#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct pr {
  char c;
  char* str;
} pr;

int main() {
  printf("Введите количество правил:\n");
  int cnt_pr;
  scanf("%d", &cnt_pr);
  pr* prs = (pr*) malloc(sizeof(pr) * cnt_pr);
  for (int i = 0; i < cnt_pr; i++) {
    prs[i].str = (char*) malloc(sizeof(char) * 100);
  }
  printf("Введите правила в виде <Symbol> -> <String>\n");
  char temp_sym;
  for (int i = 0; i < cnt_pr; i++) {
    scanf("%c", &temp_sym);
    scanf("%c", &prs[i].c);
    scanf("%c", &temp_sym);
    if (temp_sym != ' ') {
      printf("Некорректный ввод\n");
      return 1;
    }
    scanf("%c", &temp_sym);
    if (temp_sym != '-') {
      printf("Некорректный ввод\n");
      return 1;
    }
    scanf("%c", &temp_sym);
    if (temp_sym != '>') {
      printf("Некорректный ввод\n");
      return 1;
    }
    scanf("%c", &temp_sym);
    if (temp_sym != ' ') {
      printf("Некорректный ввод\n");
      return 1;
    }
    scanf("%s", prs[i].str);
  }
  printf("Введите исходную строку:\n");
  char* input_str = (char*) malloc(sizeof(char) * 1000);
  scanf("%s", input_str);
  for (int k = 0; k < 5; k++) {
    int ind_in_res = 0;
    for (int i = 0; i < strlen(input_str); i++) {
      int cnt_sym = 0;
      for (int j = 0; j < cnt_pr; j++) {
        if (prs[j].c == input_str[i]) {
          for (int z = 0; z < strlen(prs[j].str); z++) {
            cnt_sym++;
          }
          is_find = 1;
          break;
        }
      }
      if (!is_find) {
        cnt_sym++;
      }
    }
    char* res_str = (char*) malloc(sizeof(char) * cnt_sym);
    for (int i = 0; i < strlen(input_str); i++) {
      int is_find = 0;
      for (int j = 0; j < cnt_pr; j++) {
        if (prs[j].c == input_str[i]) {
          for (int z = 0; z < strlen(prs[j].str); z++) {
            res_str[ind_in_res] = prs[j].str[z];
            ind_in_res++;
          }
          is_find = 1;
          break;
        }
      }
      if (!is_find) {
        res_str[ind_in_res] = input_str[i];
        ind_in_res++;
      }
    }
    for (int i = 0; i < ind_in_res; i++) {
      printf("%c", res_str[i]);
    }
    printf("\n");
    strcpy(input_str, res_str);
  }
  return 0;
}
