#pragma once

typedef struct Node
{
    int deg, coef;
    struct Node *next;
} Node;
