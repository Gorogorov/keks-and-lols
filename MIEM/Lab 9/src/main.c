#include <stdio.h>
#include <stdlib.h>
#include "../include/single_linked_list.h"

// Добавляем элемент списка сразу за элементом lst.
// cur_deg - степень добавляемой вершины
// cur_coef - коэффициент добавляемой вершины
// Функция возвращает добавленную вершину
Node* add_node(Node* lst, int cur_deg, int cur_coef)
{
  Node *temp, *p;
  temp = (Node*) malloc(sizeof(Node));
  // Сейчас lst --> p
  p = lst->next;
  lst->next = temp;
  temp->deg = cur_deg;
  temp->coef = cur_coef;
  temp->next = p;
  // Теперь lst --> temp --> p
  return(temp);
}


// Добавляет первую вершину в список
// cur_deg - степень добавляемой вершины
// cur_coef - коэффициент добавляемой вершины
// Функция возвращает добавленную вершину
Node* init_list(int cur_deg, int cur_coef)
{
  Node *N;
  N = (Node*)malloc(sizeof(Node));
  N->deg = cur_deg;
  N->coef = cur_coef;
  N->next = NULL;
  return(N);
}


// Создает список-полином с cnt_nodes вершинами
// Возвращает вершину, являющуюся началом созданного списка-полинома
Node* create_polinom(int cnt_nodes)
{
  Node* head;
  for (int i = 0; i < cnt_nodes; i++)
  {
    // Считываем степень монома и его коэффициент
    int cur_deg, cur_coef;
    printf("Input degree of term number %d: ", (i+1));
    scanf("%d", &cur_deg);
    printf("Input coefficient of term number %d: ", (i+1));
    scanf("%d", &cur_coef);
    // Если это первая вершина, то создаем список с помощью init_list
    if (i == 0)
    {
      head = init_list(cur_deg, cur_coef);
    }
    // Если это не первая вершина
    else
    {
      // Если степень добавляемого монома больше, чем максимальная 
      // из добавленных, то делаем добавляемой моном первым в списке
      if (head->deg < cur_deg)
      {
        Node* temp = (Node*) malloc(sizeof(Node));
        temp->deg = cur_deg;
        temp->coef = cur_coef;
        temp->next = head;
        head = temp;
        continue;
      }
      Node* cur_node = head;
      // Находим для монома его позицию в листе:
      // Либо вставляем его в между мономами, т.ч. степень монома
      // лежит между степенями этих мономов, либо, если степень 
      // добавляемого монома меньше любой в листе, вставляем его
      // в конец листа
      while (cur_node->next != NULL &&
            !(cur_node->deg >= cur_deg &&
             cur_node->next->deg < cur_deg))
      {
        cur_node = cur_node->next;
      }
      // Если в листе уже есть моном такой же степени, как у
      // добавляемого, то уведомляем об этом пользователя
      if (cur_node->deg == cur_deg)
      {
        printf("Your degree already exists\n");
      }
      // Иначе добавляем моном в лист
      else
      {
        add_node(cur_node, cur_deg, cur_coef);
      }
    }
  }
  return head;
}


// Показавыет полином, принимает его начало
void show_polinom(Node* head)
{
  // Если в листе есть другие мономы, кроме свободного члена
  if (head->deg != 0 )
  {
    // Если коэффициент монома не равен 1 или 0
    if (head->coef != 1 && head->coef != 0)
      printf("%dx^%d ", head->coef, head->deg);
    // Если коэффициент монома равен 1, то этот коэффициент 
    // не выводим
    else if (head->coef == 1)
      printf("x^%d ", head->deg);
    // Если коэффициент монома равен 0, то вместо монома печатаем 0
    else
      printf("0 ");
  }
  // Если в листе есть только свободный член
  else
  {
    printf("%d ", head->coef);
  }
  if (head->next != NULL) printf("+ ");
  // Пока не дойдем до конца полинома, повторяем то же самое
  while (head->next != NULL)
  {
    head = head->next;
    if (head->deg != 0)
    {
      if (head->coef != 1 && head->coef != 0)
        printf("%dx^%d ", head->coef, head->deg);
      else if (head->coef == 1)
        printf("x^%d ", head->deg);
      else
        printf("0 ");
    }
    else
    {
      printf("%d ", head->coef);
    }
    if (head->next != NULL) printf("+ ");
  }
  printf("\n");
}


// Возвращает первый полином без мономов тех степеней, которых нет
// во втором полиноме.
// pol1 - первый полином
// pol2 - второй полином
Node* result_polinom(Node* pol1, Node* pol2)
{
  Node* res_polinom = NULL;
  // Пока не дойдем ко конца первого полинома
  while (pol1 != NULL)
  {
    Node* cur_node = pol2;
    // is_find = 1, если во втором полиноме есть моном такой же
    // степени
    int is_find = 0;
    // Ищем во втором полиноме моном такой же степени
    while (cur_node != NULL && cur_node->deg >= pol1->deg)
    {
      if (pol1->deg == cur_node->deg) is_find = 1;
      cur_node = cur_node->next;
    }
    // Если нашелся
    if (is_find)
    {
      // Если возвращаемый полином пустой, то добавляем первую 
      // вершину функцией init_list
      if (res_polinom == NULL)
        res_polinom = init_list(pol1->deg, pol1->coef);
      // Иначе добавляем ее функций add_node
      else
        add_node(res_polinom, pol1->deg, pol1->coef);
    }
    pol1 = pol1->next;
  }
  return res_polinom;
}


int main()
{
  printf("Input count of terms for the fisrt polinom: ");
  // cnt_nodes - количество мономов в первом полиноме
  int cnt_nodes;
  scanf("%d", &cnt_nodes);
  if (cnt_nodes < 1)
  {
    printf("Count of nodes < 1\n");
    return -1;
  }
  // Заполняем первый полином
  Node* pol1 = create_polinom(cnt_nodes);
  // Выводим его на экран
  show_polinom(pol1);
  // Теперь cnt_nodes - количество моном во втором полиноме
  printf("Input count of terms for the second polinom: ");
  scanf("%d", &cnt_nodes);
  if (cnt_nodes < 1)
  {
    printf("Count of nodes < 1\n");
    return -1;
  }
  // Создаем второй полином
  Node* pol2 = create_polinom(cnt_nodes);
  // Выводим его на экран
  show_polinom(pol2);
  // res_pol - полином, полученный из первого путем удаления всех
  // мономов тех степеней, которых нет во втором полиноме
  Node* res_pol = result_polinom(pol1, pol2);
  // Если итоговый полином пустой, уведомляем пользователя
  if (res_pol == NULL)
  {
    printf("Result polinom is empty\n");
  }
  // Иначе выводим его на экран
  else
  {
    printf("Result polinom:\n");
    show_polinom(res_pol);
  }
  return 0;
}
