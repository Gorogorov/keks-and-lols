#include <iostream>

using namespace std;

template<class T>
class BNode
{
public:
	T data;
	BNode<T> *left, *right;

	BNode(T dd, BNode<T> *l = nullptr, BNode<T> *r = nullptr) :
		data(dd), left(l), right(r)
	{}
};

template<class T>
void print(BNode<T> *r, int offset = 0)
{
	if (r == nullptr) return;
	print(r->right, offset + 3);
	for (int i = 0; i < offset; i++)
		cout << " ";
	cout << r->data << endl;
	print(r->left, offset + 3);
}

template<class T>
int count_gsons(BNode<T> *r)
{
	int c = 0;
	if (r == nullptr) return c;
	if (r->left != nullptr) {
		if (r->left->left != nullptr) c++;
		if (r->left->right != nullptr) c++;
	}
	if (r->right != nullptr) {
		if (r->right->left != nullptr) c++;
		if (r->right->right != nullptr) c++;
	}
	return c;
}

template<class T>
BNode<T>* new_root(BNode<T> *r, T value) {
	BNode<T> *tree = new BNode<T>(value, nullptr, r);
	return tree;
}

template <class T>
BNode<T>* left_rotation(BNode<T> *r) {
	if (r == nullptr || r->right == nullptr) {
		cout << "bad tree!\n";
		return nullptr;
	}
	BNode<T> *tree = new BNode<T>(r->right->data, r, r->right->right);
	tree->left->right = r->right->left;
	return tree;  
} 

int main() {
	BNode<int> *tree1 = new BNode<int>(1,
		new BNode<int>(2,
			new BNode<int>(4),
			new BNode<int>(5)),
		new BNode<int>(3,
			new BNode<int>(6)));
	cout << "count gsons: " << count_gsons(tree1) << "\n";
	tree1 = new_root(tree1, 0);
	cout << "New tree after adding new root:\n";
	print(tree1);
	tree1 = left_rotation(tree1);
	cout << "New root after left rotation:\n";
	print(tree1);
	return 0;
}