#include <iostream>
#include <stdio.h>

using namespace std;

//Деревья с произвольным числом сыновей
template<class T>
class ANode
{
public:
	T data;
	ANode<T> *down, *right;
	ANode(T newdata, ANode<T> *d = nullptr, ANode<T> *r = nullptr) :
		data(newdata), down(d), right(r) {}
};

template<class T>
void print(ANode<T> *r, int offset = 0)
{
	if (r == nullptr) return;
	for (int i = 0; i < offset; i++)
		cout << " ";
	cout << r->data << endl;
	print(r->down, offset + 3);
	print(r->right, offset);
}

template<class T>
int count_sons2(ANode<T> *r)
{
	int c = 0;
	if (r == nullptr) return c;
	r = r->down;
	while (r != nullptr)
	{
		c++;
		r = r->right;
	}
	return c;
}

template<class T>
ANode<T>* insert_son(ANode<T> *r, T value) {
	if (r == nullptr) return nullptr;
	ANode<T> *tree = new ANode<T>(r->data, new ANode<T>(value, nullptr, r->down), nullptr);
	return tree;
}

template<class T>
ANode<T>* delete_son(ANode<T> *r) {
	if (r == nullptr || r->down == nullptr) {
		cout << "illegal tree\n";
		return nullptr;
	}
	if (r->down->down != nullptr) {
		ANode<T> *tree = new ANode<T>(r->data, r->down->down, nullptr);
		tree->down->down = r->down->down->right;
		tree->down->right = r->down->right;
		return tree;	
	}
	else {
		ANode<T> *tree = new ANode<T>(r->data, r->down->right, nullptr);
		return tree;
	}
}

template<class T>
ANode<T>* find_son(ANode<T> *r, T d1, T d2) {
	if (r == nullptr) return r;
	ANode<T> *son = r->down;
	while (son != nullptr) {
		if (son->data != d1) {
			son = son->right;
			continue;
		}
		ANode<T> *dson = son->down;
		while (dson != nullptr) {
			if (dson->data == d2) return son;
			dson = dson->right;
		}
		son = son->right;
	}
	return nullptr;
} 

int main()
{
	ANode<int> *tree2 = new ANode<int>(1,
		new ANode<int>(2,
			new ANode<int>(5,
				nullptr,
				new ANode<int>(6,
					nullptr,
					new ANode<int>(7))),
			new ANode<int>(3,
				new ANode<int>(8),
				new ANode<int>(4,
					new ANode<int>(9,
						nullptr,
						new ANode<int>(10))))));

	print(tree2);

	cout << "Number: " << count_sons2(tree2) << endl; //посчитать число сыновей

	tree2 = insert_son(tree2, 100);
	cout << "New tree after addind the first son:\n";
	print(tree2);
	tree2 = delete_son(tree2);
	cout << "New trees after deleting the first and the seconds sons\n";
	print(tree2);
	tree2 = delete_son(tree2);
	print(tree2);
	cout << "The data of son with d1 == 4 and d2 == 10:\n";
	cout << find_son(tree2, 4, 10)->data << "\n";
	//Задания (написать сами функции и приложения, их тестирующие):
	//2.1) Написать функцию, вставляющую в дерево первого сына корня с данными, переданными в качестве параметра.
	//2.2) Написать функцию, удаляющую первого сына корня (его сыновья, если такие были, становятся сыновьями
	//корня перед бывшим вторым сыном корня).
	//2.3) Написать функцию, возвращающую указатель на первого сына корня с данными d1, у которого есть сын с
	//данными d2(d1 и d2 — параметры функции).

	return 0;
}