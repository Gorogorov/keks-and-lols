#include <iostream>
#include <stdio.h>
#include <vector>
#include <list>

using namespace std;

template<class T>
class BNode
{
public:
	T data;
	BNode<T> *left, *right;

	BNode(T dd, BNode<T> *l = nullptr, BNode<T> *r = nullptr) :
		data(dd), left(l), right(r)
	{}
};



template<class T>
class Stack {
private:
	vector<T> V;
public:
	Stack() : V(vector<T>()) {}
	void push(T value) {
		V.push_back(value);
	}
	T pop() {
		T temp = V[V.size()-1];
		V.pop_back();
		return temp;
	}
	bool empty() {
		return V.empty();
	}
};

template<class T>
class Queue {
private:
	list<T> V;
public:
	Queue() : V(list<T>()) {}
	void push(T value) {
		V.push_back(value);
	}
	T pop() {
		if (!V.size()) {
			cout << "Empty queue!\n";
			return T();
		}
		T temp = V.front();
		V.pop_front();
		return temp;
	}
	bool empty() {
		return V.empty();
	}
};

template<class T>
int count_depth_by_stack(BNode<T> *p)
{
	int c = 0;
	Stack<BNode<T>*> S;
	BNode<T> *t;
	if (p == nullptr) return 0;
	S.push(p);
	while (!S.empty())
	{
		c++;
		t = S.pop();
		if (t->left != nullptr) S.push(t->left);
		if (t->right != nullptr) S.push(t->right);
	}
	return c;
}

template<class T>
int count_width_by_queue(BNode<T> *p)
{
	int c = 0;
	Queue<BNode<T>*> S;
	BNode<T> *t;
	if (p == nullptr) return 0;
	S.push(p);
	while (!S.empty())
	{
		c++;
		t = S.pop();
		if (t->left != nullptr) S.push(t->left);
		if (t->right != nullptr) S.push(t->right);
	}
	return c;
}

template <class T>
T total(BNode<T> *r) {
	T sum = 0;
	Stack<BNode<T>*> S;
	BNode<T> *t;
	if (r == nullptr) return 0;
	S.push(r);
	while (!S.empty())
	{
		t = S.pop();
		sum += t->data;
		if (t->left != nullptr) S.push(t->left);
		if (t->right != nullptr) S.push(t->right);
	}
	return sum;
} 

template<class T>
int depth(BNode<T> *p)
{
	if (p == nullptr) return 0;
	return 1 + max(depth(p->left), depth(p->right));
}

template <class T>
T min_value(BNode<T> *r) {
	T min_v = 1000000;
	Stack<BNode<T>*> S;
	BNode<T> *t;
	if (r == nullptr) return 0;
	S.push(r);
	while (!S.empty())
	{
		t = S.pop();
		min_v = min(min_v, t->data);
		if (t->left != nullptr) S.push(t->left);
		if (t->right != nullptr) S.push(t->right);
	}
	return min_v;
} 

template<class T>
void print(BNode<T> *r, int offset = 0)
{
	if (r == nullptr) return;
	print(r->right, offset + 3);
	for (int i = 0; i < offset; i++)
		cout << " ";
	cout << r->data << endl;
	print(r->left, offset + 3);
}

template<class T>
int count_sons(BNode<T> *r)
{
	int c = 0;
	if (r == nullptr) return c;
	if (r->left != nullptr) c++;
	if (r->right != nullptr) c++;
	return c;
}

//Обход в глубину с помощью рекурсии
template<class T>
int count_depth(BNode<T> *p)
{
	if (p == nullptr) return 0;
	return 1 + count_depth(p->left) + count_depth(p->right);
}

int main()
{
	BNode<int> *tree1 = new BNode<int>(1,
		new BNode<int>(2,
			new BNode<int>(4),
			new BNode<int>(5)),
		new BNode<int>(3,
			new BNode<int>(6)));

	print(tree1);

	cout << "Number of nodes: " << count_depth(tree1) << endl; //посчитать число узлов дерева

	cout << count_depth_by_stack(tree1) << "\n";
	cout << count_width_by_queue(tree1) << "\n";
	cout << "total: " << total(tree1) << "\n";
	cout << "depth: " << depth(tree1) << "\n";
	cout << "min value: " << min_value(tree1) << "\n";
	//Задания:
	//3.1) Реализовать класс стек для нерекурсивного обхода в глубину  
	//3.2) Реализовать класс очередь для нерекурсивного обхода в ширину
	//3.3) Написать функции:
	//total, возвращающую сумму всех данных в узлах дерева.
	//height, возвращающую высоту дерева.
	//min, возвращающую минимальное значение данных в узлах дерева

	return 0;
}

/*
template<class T>
int count_depth_by_stack(BNode<T> *p)
{
	int c = 0;
	Stack<BNode<T>*> S;
	BNode<T> *t;
	if (p == nullptr) return 0;
	S.push(p);
	while (!S.empty())
	{
		c++;
		t = S.pop();
		if (t->left != nullptr) S.push(t->left);
		if (t->right != nullptr) S.push(t->right);
	}
	return c;
}
*/

/*
template<class T>
int count_width_by_queue(BNode<T> *p)
{
	int c = 0;
	Queue<BNode<T>*> S;
	BNode<T> *t;
	if (p == nullptr) return 0;
	S.push(p);
	while (!S.empty())
	{
		c++;
		t = S.pop();
		if (t->left != nullptr) S.push(t->left);
		if (t->right != nullptr) S.push(t->right);
	}
	return c;
}*/