 #pragma once
#include <string>
#include <iostream>

class Size {
protected:
	int length, width, height;
public:
	Size(int a, int b, int c);
	void set_length(int x);
	void set_width(int x);
	void set_height(int x);
	int get_length() const;
	int get_width() const;
	int get_height() const;
	virtual void print() const = 0;
};

class Price{
protected:
	int price;
public:
	Price(int a);
	void set_price(int x);
	int get_price() const;
};

class Material {
protected:
	std::string material;
public:
	Material(std::string a);
	void set_name(std::string x);
	std::string get_name() const;
};

class Color {
protected:
	std::string color;
public:
	Color(std::string a);
	void set_color(std::string x);
	std::string get_color() const;
};

class Sofa : public Size, public Price, public Material, public Color{
protected:
	int weight;
public:
	Sofa(int a, int b, int c, int d, std::string s1, std::string s2, int e);
	void set_weight(int x);
	int get_weight() const;
	void print() const override;
};

class Bed : public Size, public Price, public Material, public Color {
protected:
	int capacity;
public:
	Bed(int a, int b, int c, int d, std::string s1, std::string s2, int e);
	void set_capacity(int x);
	int get_capacity() const;
	void print() const override;
};

class Table : public Sofa {
protected:
	std::string shape;
public:
	Table(int a, int b, int c, int d, std::string s1, std::string s2, int e, std::string s3);
	void set_shape(std::string x);
	std::string get_shape() const;
	void print() const final;
};