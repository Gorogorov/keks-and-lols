#include "../include/furniture.h"

using namespace std;

Size::Size(int a, int b, int c) : length(a), width(b), height(c) 
{}

void Size::set_length(int x) {
	length = x;
}

void Size::set_width(int x) {
	width = x;
}

void Size::set_height(int x) {
	height = x;
}

int Size::get_length() const {
	return length;
}

int Size::get_width() const {
	return width;
}

int Size::get_height() const {
	return height;
}

Price::Price(int a) : price(a)
{}

void Price::set_price(int x) {
	price = x;
}

int Price::get_price() const {
	return price;
}

Material::Material(std::string a) : material(a) 
{}

void Material::set_name(std::string x) {
	material = x;
}

std::string Material::get_name() const {
	return material;
}

Color::Color(std::string a) : color(a)
{}

void Color::set_color(std::string x) {
	color = x;
}
	
std::string Color::get_color() const {
	return color;
}

Sofa::Sofa(int a, int b, int c, int d, std::string s1, std::string s2, int e) : Size(a, b, c), Price(d), 
                      Material(s1), Color(s2), weight(e)
{}

void Sofa::set_weight(int x) {
	weight = x;
}

int Sofa::get_weight() const {
	return weight;
}

void Sofa::print() const {
	cout << "length: " << length << "\n";
	cout << "width: " << width << "\n";
	cout << "height: " << height << "\n";
	cout << "price: " << price << "\n";
	cout << "material: " << material << "\n";
	cout << "color: " << color << "\n";
	cout << "weight: " << weight << "\n";
}

Bed::Bed(int a, int b, int c, int d, std::string s1, std::string s2, int e) : Size(a, b, c), Price(d), 
                     Material(s1), Color(s2), capacity(e)
{}

void Bed::set_capacity(int x) {
	capacity = x;
}

int Bed::get_capacity() const {
	return capacity;
}

void Bed::print() const {
	cout << "length: " << length << "\n";
	cout << "width: " << width << "\n";
	cout << "height: " << height << "\n";
	cout << "price: " << price << "\n";
	cout << "material: " << material << "\n";
	cout << "color: " << color << "\n";
	cout << "capacity: " << capacity << "\n";
}

Table::Table(int a, int b, int c, int d, std::string s1, std::string s2, int e, std::string s3) : Sofa(a, b, c, d, s1, s2, e), shape(s3)
{}

void Table::set_shape(string x) {
	shape = x;
}

string Table::get_shape() const {
	return shape;
}

void Table::print() const {
	cout << "length: " << length << "\n";
	cout << "width: " << width << "\n";
	cout << "height: " << height << "\n";
	cout << "price: " << price << "\n";
	cout << "material: " << material << "\n";
	cout << "color: " << color << "\n";
	cout << "weight: " << weight << "\n";
	cout << "shape: " << shape << "\n";
}
