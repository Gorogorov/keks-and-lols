 #pragma once
#include <vector>
#include <cstdlib>
#include <algorithm>
#include <iostream>
#include <ostream>
#include <fstream>

 class Item {
 private:
 	int skill, health, armor, power;
 public:
 	Item(int a, int b, int c, int d);
 	int get_health() const;
 	int get_power() const;
 	int get_armor() const;
 	int get_skill() const;
 	int get_item_power() const;
 };

class Base_unit {
protected:
 	int health, power, armor;
 	std::vector<Item> items;
public:
	Base_unit();
	int get_health() const;
	int get_power() const;
	int get_armor() const;
	std::vector<Item>& get_items();
	void set_health(int x);
	void set_power(int x);
	void set_armor(int x);
	void print() const;
	virtual int get_skill() const = 0;
	virtual void set_skill(int x) = 0;
 	virtual void attack(Base_unit& U) = 0;
 	virtual void get_item() = 0;
 };

class Warrior : public Base_unit {
protected:
 	int defend_skill;
public:
	Warrior();
	int get_skill() const override;
	void set_skill(int x) override;
 	void attack(Base_unit& U) override;
 	void get_item() override;
 	void defend();
 };

class Healer : public Base_unit {
 protected:
 	int heal_skill;
 public:
 	Healer();
 	int get_skill() const override;
	void set_skill(int x) override;
 	void attack(Base_unit& U) override;
 	void get_item() override;
 	void heal(Base_unit& U);
 };

class Thief : public Base_unit {
 protected:
 	int steal_skill;
 public:
 	Thief();
  	int get_skill() const override;
	void set_skill(int x) override;
 	void attack(Base_unit& U) override;
 	void get_item() override;
 	void steal(Base_unit& U);
 };