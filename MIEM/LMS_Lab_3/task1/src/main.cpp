#include "../include/Rts.h"
#include <ctime>

using namespace std;

int main() {
	srand(time(NULL));
	Warrior W1, W2;
	W1.get_item();
	W2.get_item();
	W1.attack(W2);
	W2.defend();
	W1.print();
	W2.print();
	Healer H1;
	H1.heal(W2);
	W2.print();
	Thief T1;
	T1.steal(W2);
	W2.print();
	T1.attack(W2);
	W2.print();
	return 0;
}