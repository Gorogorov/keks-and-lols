#include "../include/Rts.h"

using namespace std;

Item::Item(int a, int b, int c, int d) : power(a), health(b), armor(c), skill(d) 
{}

int Item::get_health() const {
	return health;
}

int Item::get_power() const {
	return power;
}

int Item::get_armor() const {
	return armor;
}

int Item::get_skill() const {
	return skill;
}

int Item::get_item_power() const {
	return health+power+armor+2*skill;
}

Base_unit::Base_unit() : power(5), health(50), armor(5)
{}

int Base_unit::get_power() const{
	return power;
}

int Base_unit::get_health() const{
	return health;
}

int Base_unit::get_armor() const{
	return armor;
}

void Base_unit::set_power(int x) {
	power = x;
}

void Base_unit::set_health(int x) {
	health = x;
}

void Base_unit::set_armor(int x) {
	armor = x;
}

void Base_unit::print() const {
	cout << "p: " << power;
	cout << " h: " << health;
	cout << " a: " << armor;
	cout << " s: " << get_skill() << "\n";
}

std::vector<Item>& Base_unit::get_items() {
	return items;
}

Warrior::Warrior() : Base_unit(), defend_skill(1)
{}

int Warrior::get_skill() const {
	return defend_skill;
}

void Warrior::set_skill(int x) {
	defend_skill = x;
}

void Warrior::attack(Base_unit& U){
	int damage = max(0, 2*power - U.get_armor()/2);
	U.set_health(max(U.get_health() - damage, 0));
	U.set_armor(max(U.get_armor() - armor/10, 0));
}

void Warrior::get_item(){
	int n_power = rand()%5;
	int n_health = rand()%10;
	int n_armor = rand()%5 + 3;
	int n_skill = rand()%3;
	Item N_item(n_power, n_health, n_armor, n_skill);
	items.push_back(N_item);
	power += n_power;
	health += n_health;
	armor += n_armor;
	defend_skill += n_skill;
}

void Warrior::defend() {
	armor += defend_skill*(rand()%5) + 5;
}

Healer::Healer() : Base_unit(), heal_skill(1)
{}

int Healer::get_skill() const {
	return heal_skill;
}

void Healer::set_skill(int x) {
	heal_skill = x;
}

void Healer::attack(Base_unit& U){
	int damage = max(0, power - U.get_armor()/2);
	U.set_health(max(U.get_health() - damage, 0));
	health += heal_skill;
}

void Healer::get_item(){
	int n_power = rand()%5;
	int n_health = rand()%10;
	int n_armor = rand()%5;
	int n_skill = rand()%3 + 1;
	Item N_item(n_power, n_health, n_armor, n_skill);
	items.push_back(N_item);
	power += n_power;
	health += n_health;
	armor += n_armor;
	heal_skill += n_skill;
}

void Healer::heal(Base_unit& U) {
	U.set_health(U.get_health()+heal_skill*(rand()%5) + 5);
}

Thief::Thief() : Base_unit(), steal_skill(1)
{}

int Thief::get_skill() const {
	return steal_skill;
}

void Thief::set_skill(int x) {
	steal_skill = x;
}

void Thief::attack(Base_unit& U){
	int damage = max(0, power - U.get_armor()/2);
	U.set_health(max(U.get_health() - damage, 0));
	if (!U.get_items().size()) return;
	int ind_item = rand()%U.get_items().size();
	Item i = U.get_items()[ind_item];
	U.get_items().erase(U.get_items().begin()+ind_item);
	U.set_power(U.get_power() - i.get_power());
	U.set_armor(U.get_armor() - i.get_armor());
	U.set_skill(U.get_skill() - i.get_skill());
}

void Thief::get_item(){
	int n_power = rand()%10;
	int n_health = rand()%15;
	int n_armor = rand()%10;
	int n_skill = rand()%3;
	Item N_item(n_power, n_health, n_armor, n_skill);
	items.push_back(N_item);
	power += n_power;
	health += n_health;
	armor += n_armor;
	steal_skill += n_skill;
}

void Thief::steal(Base_unit& U) {
	if (!U.get_items().size()) return;
	int ind_item = rand()%U.get_items().size();
	Item i = U.get_items()[ind_item];
	U.get_items().erase(U.get_items().begin()+ind_item);
	Item n_i(i.get_power()+steal_skill, i.get_health()+steal_skill, i.get_armor()+steal_skill, i.get_skill()+steal_skill);
	items.push_back(n_i);
	U.set_power(U.get_power() - i.get_power());
	U.set_armor(U.get_armor() - i.get_armor());
	U.set_skill(U.get_skill() - i.get_skill());
	health += n_i.get_health();
	armor += n_i.get_armor();
	power += n_i.get_power();
	steal_skill += n_i.get_skill();
}