#include <iostream>
#include <cmath>

using namespace std;

class Trapezoid{
private:
	double a, b;
public:
	Trapezoid(double x, double y) : a(x), b(y) {}
	double sum() const;
};

double Trapezoid::sum() const {
	return (a+b)/2;
}

class Complex {
private:
	double re, im;
public:
	Complex() : re(0), im(0) {}
	Complex(double x, double y) : re(x), im(y) {}
	~Complex();
	double abs() const;
	Complex inversive();
	void print() const;
};

Complex::~Complex() {
	cout << "Destroyed\n";
}

double Complex::abs() const {
	return sqrt(re*re + im*im);
}

Complex Complex::inversive() {
	if (abs() == 0) throw runtime_error("Division by zero");
	return Complex(re/(abs()*abs()), -im/(abs()*abs()));
}

void Complex::print() const{
	cout << re << " " << im << "\n";
}

int main() {
	Trapezoid test1(4, 5.5);
	cout << test1.sum() << "\n";
	Complex test2(3, 4);
	cout << test2.abs() << "\n";
	try{
		test2.inversive().print();	
	}
	catch(exception& e) {
		cout << e.what() << "\n";
	}
	return 0;
}