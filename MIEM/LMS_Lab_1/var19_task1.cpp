#include <iostream>
#include <cmath>

using namespace std;

class A{
private:
	double x, y;
public:
	A(double a, double b) : x(a), y(b) {}
	double go_res();
};

double A::go_res() {
	return fabs(x*y);
}

int main() {
	A test = A(3.33, 4.12);
	cout << test.go_res() << "\n";
	return 0;
}