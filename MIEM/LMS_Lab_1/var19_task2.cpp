#include <iostream>
#include <cmath>

using namespace std;

class A{
private:
	// зачем нужны подчеркивания: https://stackoverflow.com/questions/3136594/naming-convention-underscore-in-c-and-c-sharp-variables
	double _x1, _y1, _x2, _y2;
public:
	A() : _x1(0), _y1(0), _x2(0), _y2(0) {};
	A(double  a, double b, double c, double d) : _x1(a), _y1(b), _x2(c), _y2(d) {}
	~A();
	A task1();
	double task2();
	// функции ниже для того, чтобы тестировать прогу
	// по-умному они называется аксессоры, вроде
	double x1() { return _x1; }
	double x2() { return _x2; }
	double y1() { return _y1; }
	double y2() { return _y2; }
};

A::~A() {
	cout << "The object is destroyed\n";
}

A A::task1() {
	A res(_x1, _y1, 2*_x2, 2*_y2);
	return res;
	//return sqrt(fabs(x1-x2)*fabs(x1-x2) + fabs(y1-y2)*fabs(y1-y2));
}

double A::task2() {
	return fabs(_x1-_x2)*fabs(_y1-_y2)/2;
}

int main() {
	A test1(1, 1, 3, 4);
	A test2 = test1.task1();
	cout << "Coors = (" << test2.x1() << ", " << test2.y1() << ", " << test2.x2() << ", " << test2.y2() << ")\n";
	cout << "S = " << test2.task2() << "\n";
	return 0;
}