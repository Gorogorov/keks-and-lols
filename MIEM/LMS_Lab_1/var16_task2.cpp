#include <iostream>
#include <cmath>

using namespace std;

class A{
private:
	string name;
	int zp, year;
public:
	A() : name(" "), zp(0), year(0) {};
	A(string q, int w, int e) : name(q), zp(w), year(e) {}
	~A();
	int task1();
	int task2();
};

A::~A() {
	cout << "The object is destroyed\n";
}

int A::task1() {
	return 2018-year;
}

// точно кол-во дней не вычислить, т.к. указан только год рождения
int A::task2() {
	int res = (50-task1() + 1)*365;
	// учитываем високосные
	for (int i = 2018; i<2018+(50-task1()+1); i++) {
		if (!(i%4)) res++;
	}
	return max(0, res);
}

int main() {
	A test1;
	// опять же, точно возраст не вычислить, т.к. указан только год рождения
	cout << "Age is " << test1.task1()-1 << " or " << test1.task1() << "\n";
	cout << test1.task2() << "\n";
	A test2("Vasya", 30000, 1968);
	cout << "Age is " << test2.task1()-1 << " or " << test2.task1() << "\n";
	cout << test2.task2() << "\n";
	return 0;
}