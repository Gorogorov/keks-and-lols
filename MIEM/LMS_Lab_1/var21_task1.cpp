#include <iostream>

using namespace std;

class A{
private:
	double a, b;
public:
	A(double x, double y) : a(x), b(y) {}
	double go_res();
};

double A::go_res() {
	return a*a+b*b;
}

int main() {
	A test = A(3.0, 4.1);
	cout << test.go_res() << "\n";
	return 0;
}