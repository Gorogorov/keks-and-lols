#include <iostream>

using namespace std;

class A{
private:
	int x, y;
public:
	A(int a, int b) : x(a), y(b) {}
	int go_res();
};

int A::go_res() {
	return x<y ? x*x : y*y;
}

int main() {
	A test = A(3, 4);
	cout << test.go_res() << "\n";
	return 0;
}