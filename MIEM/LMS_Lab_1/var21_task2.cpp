#include <iostream>
#include <cmath>

using namespace std;

class A{
private:
	int a, b, c;
public:
	A() : a(0), b(0), c(0) {};
	A(int q, int w, int e) : a(q), b(w), c(e) {}
	~A();
	int task1();
	double task2();
};

A::~A() {
	cout << "The object is destroyed\n";
}

int A::task1() {
	return a*b*c;
}

double A::task2() {
	return max(max(sqrt(a*a + b*b), sqrt(b*b + c*c)), sqrt(a*a+c*c));
}

int main() {
	A test1;
	cout << test1.task1() << " " << test1.task2() << "\n";
	A test2 = A(3, 4, 5);
	cout << test2.task1() << " " << test2.task2() << "\n";
	return 0;
}