#include <iostream>
#include <fstream>
#include <exception>
#include <vector>
#include <iterator>

using namespace std;

class Matrix {
protected:
	int _rows, _cols;
	vector<vector<double>> matrix;
public:
	Matrix(int a, int b);
	Matrix(int a, int b, ifstream& inpt);
	Matrix(const Matrix& M);
	Matrix& operator=(const Matrix& M);
	int rows() const;
	int cols() const;
	double& val(int n, int m);
	double val_const(int n, int m) const;
	void print(ofstream& stream);
	template <typename T> friend T mul(const T& M, double a);
	template <typename T> friend T sum(const T& M1, const T& M2);
	template <typename T> friend T mul(const T& M1, const T& M2);
	template <typename T> friend T transpose(const T& M);
	template <typename T> friend T to_echelon(const T& M);
	template <typename T> friend T to_main_echelon(const T& M);
	template <typename T> friend double det(const T& M);
	template <typename T> friend T inverse(const T& M);
	~Matrix();
};

class vMatrix : public Matrix {
private:
	int _size;
public:
	vMatrix(int rows, int cols) : Matrix(rows, cols), _size(cols){ 
		if (cols != 1) throw runtime_error("Illegal size of vmatrix");
	}
	vMatrix(int rows, int cols, ifstream& inpt) : Matrix(rows, cols, inpt), _size(cols) {
		if (cols != 1) throw runtime_error("Illegal size of vmatrix");
	}
	vMatrix(const vMatrix& M) : Matrix(M) {};
	vMatrix& operator=(const vMatrix& M);
};

class sqMatrix : public Matrix {
private:
	int _size;
public:
	sqMatrix(int rows, int cols) : Matrix(rows, cols), _size(cols) {
		if (rows != cols) throw runtime_error("Illegal size of matrix");
	}
	sqMatrix(int rows, int cols, ifstream& inpt) : Matrix(rows, cols, inpt), _size(cols) {
		if (rows != cols) throw runtime_error("Illegal size of matrix");
	}
	sqMatrix(const sqMatrix& M) : Matrix(M) {};
	sqMatrix& operator=(const sqMatrix& M);
};

Matrix::Matrix(int a, int b) {
	if (a <= 0 || b <= 0) throw runtime_error("Illegal size of matrix");
	_rows = a;
	_cols = b;
	for (int i = 0; i < _rows; i++) {
		matrix.push_back(vector<double>(_cols));
	}
}

Matrix::Matrix(int a, int b, ifstream& inpt) {
	if (a <= 0 || b <= 0) throw runtime_error("Illegal size of matrix");
	_rows = a;
	_cols = b;
	for (int i = 0; i < _rows; i++) {
		matrix.push_back(vector<double>(_cols));
	}
	for (vector<vector<double>>::iterator it = matrix.begin(); it != matrix.end(); it++) {
		for (vector<double>::iterator itt = (*it).begin(); itt != (*it).end(); itt++) {
			inpt >> *itt;
		}
	}
}

Matrix::Matrix(const Matrix& M) {
	if (this == &M) return;
	_rows = M._rows;
	_cols = M._cols;
	for (int i = 0; i < _rows; i++) {
		matrix.push_back(vector<double>(_cols));
	}
	for (int i = 0; i < _rows; i++) {
		for (int j = 0; j < _cols; j++) {
			matrix[i][j] = M.val_const(i, j);
		}
	}
}

Matrix& Matrix::operator=(const Matrix& M) {
	if (this == &M) {
		return *this;
	}
	_rows = M._rows;
	_cols = M._cols;
	for (int i = 0; i < _rows; i++) {
		matrix.push_back(vector<double>(_cols));
	}
	for (int i = 0; i < _rows; i++) {
		for (int j = 0; j < _cols; j++) {
			matrix[i][j] = M.val_const(i, j);
		}
	}
	return *this;
}

Matrix::~Matrix() {
	matrix.clear();
}

double& Matrix::val(int n, int m){
	if (n < 0||n>=_rows||m<0||m>=_cols) {
		throw runtime_error("Illegal index");
	}
	return matrix[n][m];
}

double Matrix::val_const(int n, int m) const{
	if (n < 0||n>=_rows||m<0||m>=_cols) {
		throw runtime_error("Illegal index");
	}
	return matrix[n][m];
}

int Matrix::rows() const {
	return _rows;
}

int Matrix::cols() const {
	return _cols;
}

void Matrix::print(ofstream& stream) {
	for (int i = 0; i < _rows; i++) {
		for (int j = 0; j < _cols; j++) {
			if (matrix[i][j] == -0.0) matrix[i][j] = 0; 
			stream << matrix[i][j] << " ";
		}
		stream << "\n";
	}
}

template <typename T>
T mul(const T& M, double a) {
	T result(M._rows, M._cols);
	for (int i = 0; i < M._rows; i++) {
		for (int j =0; j < M._cols; j++) {
			result.val(i, j) = M.val_const(i, j)*a;
		}
	}
	return result;
}

template <typename T>
T sum(const T& M1, const T& M2) {
	if (M1._rows != M2._rows || M1._cols != M2._cols) throw runtime_error("Illegal size (sum)");
	T result(M1._rows, M1._cols);
	for (int i = 0; i < M1._rows; i++) {
		for (int j = 0; j < M1._cols; j++) {
			result.val(i, j) = M1.val_const(i, j)+M2.val_const(i, j);
		}
	}
	return result;
}

template <typename T>
T mul(const T& M1, const T& M2) {
	if (M1._cols != M2._rows) throw runtime_error("Illegal size (mul)");
	T result(M1._rows, M2._cols);
	for (int i = 0; i < M1._rows; i++) {
		for (int j = 0; j < M1._cols; j++) {
			double sum = 0;
			for (int k = 0; k < M1._cols; k++) {
				sum += M1.val_const(i, k)*M2.val_const(k, j);
			}
			result.val(i, j) = sum;
		}
	}
	return result;
}

template <typename T> 
T transpose(const T& M) {
	T result(M._rows, M._cols);
	for (int i = 0; i < M._rows; i++) {
		for (int j = 0; j < M._cols; j++) {
			result.val(j,i) = M.val_const(i, j);
		}
	} 
	return result;
}

template <typename T> 
T to_echelon(const T& M) {
	T result = M;
	for (int i = 0; i < min(result._cols, result._rows); i++) {
		if (!result.val(i, i)) {
			for (int j = i+1; j < result._rows; j++) {
				if (result.val(j, i)) {
					for (int k = 0; k < result._cols; k++) {
						swap(result.val(i, k), result.val(j, k));
					}
					break;
				}
			}
		}
		if (!result.val(i, i)) continue;
		for (int j = i+1; j < result._rows; j++) {
			double K = result.val(i, i), M = result.val(j, i);
			result.val(j, i) = 0;
			for (int k = i+1; k < result._cols; k++) {

				result.val(j, k) -= M/K*result.val(i, k);
			}
		}
	}
	return result;
}

template <typename T> 
T to_main_echelon(const T& M) {
	T result = to_echelon(M);
	for (int i = result._rows - 1; i >= 0; i--) {
		for (int k = 0; k < result._cols; k++) {
			if (result.val(i, k)) {
				for (int j = 0; j < i; j++) {
					double K = result.val(i, k), M = result.val(j, k);
					for (int q = 0; q < result._cols; q++) {
						result.val(j, q) -= M/K*result.val(i, q);
					}
				}
				double D = result.val(i, k);
				for (int q = 0; q < result._cols; q++) {
					result.val(i, q) /= D;
				}
				break;
			}
		}
	}
	return result;
}

template <typename T>
double det(const T& M) {
	if (M._rows != M._cols) throw runtime_error("Det for non-square matrix");
	T result = M;
	int sign = 1;
	for (int i = 0; i < min(result._cols, result._rows); i++) {
		if (!result.val(i, i)) {
			for (int j = i+1; j < result._rows; j++) {
				if (result.val(j, i)) {
					sign *= -1;
					for (int k = 0; k < result._cols; k++) {
						swap(result.val(i, k), result.val(j, k));
					}
					break;
				}
			}
		}
		if (!result.val(i, i)) continue;
		for (int j = i+1; j < result._rows; j++) {
			double K = result.val(i, i), M = result.val(j, i);
			for (int k = 0; k < result._cols; k++) {
				result.val(j, k) -= M/K*result.val(i, k);
			}
		}
	}
	double ans = 1;
	for (int i = 0; i < result._cols; i++) {
		ans *= result.val(i, i);
	}
	return ans*sign;
}

template <typename T>
T inverse(const T& M) {
	if (M._rows != M._cols) throw runtime_error("Cannot inverse non-square matrix");
	if (!det(M)) throw runtime_error("Det == 0");
	Matrix pIdentity(M._rows, 2*M._cols);
	for (int i = 0; i < M._rows; i++) {
		for (int j = 0; j < 2*M._cols; j++) {
			if (j >= M._cols) pIdentity.val(i, j) = 0;
			else pIdentity.val(i, j) = M.val_const(i, j);
		}
		pIdentity.val(i, M._cols+i) = 1;
	}
	pIdentity =  to_main_echelon(pIdentity);
	T result(M._rows, M._cols);
	for (int i = 0; i < M._rows; i++) {
		for (int j = 0; j < M._cols; j++) {
			result.val(i, j) = pIdentity.val(i, M._cols+j);
		}
	}
	return result;
}

vMatrix& vMatrix::operator=(const vMatrix& M) {
	if (this == &M) {
		return *this;
	}
	_rows = M._rows;
	_cols = M._cols;
	for (int i = 0; i < _rows; i++) {
		matrix.push_back(vector<double>(_cols));
	}
	for (int i = 0; i < _rows; i++) {
		for (int j = 0; j < _cols; j++) {
			matrix[i][j] = M.val_const(i, j);
		}
	}
	return *this;
}

sqMatrix& sqMatrix::operator=(const sqMatrix& M) {
	if (this == &M) {
		return *this;
	}
	_rows = M._rows;
	_cols = M._cols;
	for (int i = 0; i < _rows; i++) {
		matrix.push_back(vector<double>(_cols));
	}
	for (int i = 0; i < _rows; i++) {
		for (int j = 0; j < _cols; j++) {
			matrix[i][j] = M.val_const(i, j);
		}
	}
	return *this;
}

int main() {
	/*ifstream in("in.txt");
	Matrix X(10, 10, in);
	Matrix Y = Matrix(X);

	// vMatrix test
	ifstream vm_in("vMatrix_test_in.txt");
	vMatrix VM(10, 1, vm_in);
	vm_in.close();
	cout << VM.val(3, 0) << "\n";

	VM = mul(VM, 3.5);
	VM = sum(VM, VM);

	ofstream vm_out("vMatrix_test_out.txt");
	VM.print(vm_out);
	vm_out.close();*/

	// sqMatrix test
	/*ifstream sqm_in("sqMatrix_test_in.txt");
	sqMatrix SQM(3, 3, sqm_in);
	sqm_in.close();
	cout << SQM.val(1, 2) << "\n";

	SQM = mul(SQM, 4.4);
	SQM = sum(SQM, SQM);
	SQM = mul(SQM, SQM);
	//SQM+VM;

	ofstream sqm_out("sqMatrix_test_out.txt");
	SQM.print(sqm_out);
	sqm_out.close();*/

	try {
		cout << "input task\n";
		int n;
		cin >> n; 
		if (n == 2) {
			ifstream in("in.txt");
			int a, b;
			in >> a >> b;
			Matrix M0(a, b);
			for (int i = 0; i < a; i++) {
				for (int j = 0; j < b; j++) {
					in >> M0.val(i, j);
				}
			}
			in.close();

			Matrix M01 = to_main_echelon(M0);
			int rank_M01 = 0;
			vector<int> main_val;
			for (int i = 0; i < a; i++) {
				for (int j = 0; j < b; j++) {
					if (M01.val(i, j)) {
						rank_M01++;
						main_val.push_back(j);
						break;
					}
				}
			}
			vector<int> free_val;
			int ind_main = 0;
			for (int i = 0; i < b; i++) {
				if (ind_main < main_val.size() && main_val[ind_main] == i){
					ind_main++;
				}
				else {
					free_val.push_back(i);
				}
			}

			ofstream out("out.txt");
			//M01.print(out);
			out << max(0, b-rank_M01) << " " << b << "\n";
			if (b - rank_M01 < 0) {
				out.close();
				return 0;
			}
			for (int k = 0; k < free_val.size(); k++) {
				vector<double> B(a, 0);
				vector <double> ans(b, 0);
				ans[free_val[k]] = 1;
				for (int i = a-1; i>=0; i--) {
					for (int j = 0; j < b; j++) {
						if (M01.val(i, j)) {
							for (int h = j+1; h < b; h++) {
								if (ans[h]) {
									B[i] -= M01.val(i,h)*ans[h];
								}
							}
							B[i] /= M01.val(i, j);
							ans[j] = B[i];
							break;
						}
					}
				}
				for (int i = 0; i < b; i++) {
					out << ans[i] << " ";
				}
				out << "\n";
			}
			out.close();
			return 0;
		}
		if (n == 3) {
			fstream in("in.txt");
			int a, b;
			in >> a >> b;
			Matrix M0(a, b+1);
			for (int i = 0; i < a; i++) {
				for (int j = 0; j < b; j++) {
					in >> M0.val(i, j);
				}
			}
			for (int i = 0; i < a; i++) {
				in >> M0.val(i, b);
			}
			in.close();

			Matrix Mm(a, b);
			for (int i = 0; i < a; i++) {
				for (int j = 0; j < b; j++) {
					Mm.val(i, j) = M0.val(i, j);
				}
			}
			Matrix M01 = to_echelon(M0);
			int rank_M01 = 0, rank_Mm = 0;
			vector<int> main_val;
			for (int i = 0; i < a; i++) {
				for (int j = 0; j < b; j++) {
					if (M01.val(i, j)) {
						rank_M01++;
						main_val.push_back(j);
						break;
					}
				}
			}
			Matrix Mm1 = to_main_echelon(Mm); 
			for (int i = 0; i < a; i++) {
				for (int j = 0; j < b; j++) {
					if (Mm1.val(i, j)) {
						rank_Mm++;
						break;
					}
				}
			}
			ofstream out("out.txt");
			//Mm1.print(out);
			//out << rank_M01 << " " << rank_Mm << "\n";
			if (rank_M01 != rank_Mm) {
				out << "-1\n";
				out.close();
				return 0;
			}
			out << b << " " << b - rank_M01 << "\n";
			vector<double> B(a, 0);
			for (int i = 0; i < a; i++) {
				B[i] = M01.val(i, b);
			}
			vector <double> ans(b, 0);
			for (int i = a-1; i>=0; i--) {
				for (int j = 0; j < b; j++) {
					if (M01.val(i, j)) {
						for (int h = j+1; h < b; h++) {
							if (ans[h]) {
								B[i] -= M01.val(i,h)*ans[h];
							}
						}
						B[i] /= M01.val(i, j);
						ans[j] = B[i];
						break;
					}
				}
			}
			for (int i = 0; i < b; i++) {
				out << ans[i] << " ";
			}
			out << "\n";

			vector<int> free_val;
			int ind_main = 0;
			for (int i = 0; i < b; i++) {
				if (ind_main < main_val.size() && main_val[ind_main] == i){
					ind_main++;
				}
				else {
					free_val.push_back(i);
				}
			}
			//M01.print(out);
			for (int k = 0; k < free_val.size(); k++) {
				vector<double> B(a, 0);
				vector <double> ans(b, 0);
				ans[free_val[k]] = 1;
				for (int i = a-1; i>=0; i--) {
					for (int j = 0; j < b; j++) {
						if (M01.val(i, j)) {
							for (int h = j+1; h < b; h++) {
								if (ans[h]) {
									B[i] -= M01.val(i,h)*ans[h];
								}
							}
							B[i] /= M01.val(i, j);
							ans[j] = B[i];
							break;
						}
					}
				}
				for (int i = 0; i < b; i++) {
					out << ans[i] << " ";
				}
				out << "\n";
			}
			out.close();



			//M01.print(out);
			out.close();
			return 0;
		}

		double k;
		ifstream in("in.txt");
		in >> k;
		int n_a, m_a;
		in >> n_a >> m_a;
		Matrix A(n_a, m_a, in);
		int n_b, m_b;
		in >> n_b >> m_b;
		Matrix B(n_b, m_b, in);
		int n_c, m_c;
		in >> n_c >> m_c;
		Matrix C(n_c, m_c, in);
		in.close();

		Matrix D = C;
		D = inverse(D);
		D = mul(transpose(B), D);
		D = mul(D, k);
		D = sum(A, D);

		ofstream out("out.txt");
		out << det(D) << "\n" << D.rows() << " " << D.cols() << "\n";
		D.print(out);
		out.close();	
	}
	catch (exception& e) {
		ofstream out("out.txt");
		out << "-1\n" << e.what() << "\n";
	}
	return 0;
}