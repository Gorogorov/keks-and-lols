import random
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

sleep_after_jump = 150 + 5
sleep_after_sniff = 25*60 + 5
sleep_after_dig = 5*60 + 5

driver = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver")
driver.get("https://catwar.su/cw3/")
time.sleep(80)
print("Opening")

cur_email = "gorogorov@yandex.ru"
cur_password = "zxcvbn12345"
login = driver.find_element_by_id('mail')
login.send_keys(cur_email)
password = driver.find_element_by_id('pass')
password.send_keys(cur_password)
entr = driver.find_element_by_xpath("//input[@value='Войти']")
entr.click()
time.sleep(5)

game = driver.find_element_by_id('emblem')
game.click()
time.sleep(5)

while(1):
    try:
        action = random.randint(0, 50)
        if action == 0:
            sniff = driver.find_element_by_xpath("//a[@data-id='13']")
            sniff.click()
            print("Понюхал")
            time.sleep(sleep_after_sniff)
        elif action == 1 or action == 2:
            dig = driver.find_element_by_xpath("//a[@data-id='17']")
            dig.click()
            print("Покопал")
            time.sleep(sleep_after_dig)
        elif action == 13:
            print("Постоял")
            time.sleep(600)
        else:
            write_file = open("../data/catwar", "w")
            write_file.write(driver.page_source)
            write_file.close()
            rfile = open("../data/catwar", "r")
            location = ""
            for line in rfile:
                ind_in_search = line.find("Моё местонахождение")
                if ind_in_search != -1:
                    ind_in_subsearch = line.find("</span>")
                    location = line[48:ind_in_subsearch]

            move = driver.find_elements_by_xpath("//span[@class='move_name']")
            ttime = sleep_after_jump + random.randint(0, 10)

            loc_num = 0
            # Луна
            if (location == "Пещера костей"):
                loc_num = random.randint(0, 1)
            if (location == "Зубастая пещера"):
                loc_num = 1
            if (location == "Поваленное дерево"):
                loc_num = random.randint(1, 3)
            if (location == "Заброшенный лаз"):
                loc_num = random.randint(0, 1)
            if (location == "Око Луны"):
                loc_num = random.randint(1, 4)
            if (location == "Заросли"):
                loc_num = 0
            if (location == "Пещера светлячков"):
                loc_num = 0
            move[loc_num].click()
            print("Ушел из", location)
            time.sleep(ttime)
    except:
        print("Error")
        time.sleep(20)
        is_refresh = 0
        while is_refresh == 0:
            try:
                driver.refresh()
                is_refresh = 1
            except:
                time.sleep(20)

driver.close()
