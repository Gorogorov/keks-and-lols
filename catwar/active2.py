import random
import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

sleep_after_jump = 150 + 5
sleep_after_sniff =50*60 + 5
sleep_after_dig = 5*60 + 5

driver = webdriver.Chrome("/usr/lib/chromium-browser/chromedriver")
driver.get("https://catwar.su/cw3/")

cur_email = input()
cur_password = input()
login = driver.find_element_by_id('mail')
login.send_keys(cur_email)
password = driver.find_element_by_id('pass')
password.send_keys(cur_password)
entr = driver.find_element_by_xpath("//input[@value='Войти']")
entr.click()
time.sleep(5)
startses2 = input()
game = driver.find_element_by_id('emblem')
game.click()
time.sleep(5)

while(1):
    try:
        action = random.randint(0, 50)
        if action == 0:
            sniff = driver.find_element_by_xpath("//a[@data-id='13']")
            sniff.click()
            print("Понюхал")
            time.sleep(sleep_after_sniff)
        elif action == 1 or action == 2:
            dig = driver.find_element_by_xpath("//a[@data-id='17']")
            dig.click()
            print("Покопал")
            time.sleep(sleep_after_dig)
        elif action == 13:
            print("Постоял")
            time.sleep(600)
        else:
            write_file = open("../data/catwar2", "w")
            write_file.write(driver.page_source)
            write_file.close()
            rfile = open("../data/catwar2", "r")
            location = ""
            for line in rfile:
                ind_in_search = line.find("Моё местонахождение")
                if ind_in_search != -1:
                    ind_in_subsearch = line.find("</span>")
                    location = line[48:ind_in_subsearch]

            move = driver.find_elements_by_xpath("//span[@class='move_name']")
            ttime = sleep_after_jump + random.randint(0, 10)

            loc_num = 0
            # Луна
            #if location == "Пещера костей":
            #    loc_num = random.randint(0, 1)
            #elif location == "Зубастая пещера":
            #    loc_num = 1
            #elif location == "Поваленное дерево":
            #    loc_num = random.randint(1, 3)
            #elif location == "Заброшенный лаз":
            #    loc_num = random.randint(0, 1)
            #elif location == "Око Луны":
            #    loc_num = random.randint(1, 4)
            #elif location == "Заросли":
            #    loc_num = 0
            #elif location == "Пещера светлячков":
            #    loc_num = 0

            # Тени
            if location == "Сумрачная обитель":
                loc_num = 1
                while loc_num == 1:
                    loc_num = random.randint(0, 5)
            elif location == "Воинские покои":
                loc_num = 0
            elif location == "Малый круг":
                loc_num = 0
            elif location == "Поганое место":
                loc_num = random.randint(0, 1)
            elif location == "Место для отдыха":
                loc_num = random.randint(0, 1)
            elif location == "Хвойная ветвь":
                loc_num = random.randint(0, 1)
            elif location == "Лисья поляна":
                loc_num = 1
                while loc_num == 1 or loc_num == 2:
                    loc_num = random.randint(0, 5)
            elif location == "Детская":
                loc_num = random.randint(0, 1)
            elif location == "Игровые камни":
                loc_num = random.randint(0, 1)
            elif location == "Лужок на берегу":
                loc_num = random.randint(0, 2)
            elif location == "Берег Хитрости":
                loc_num = 0
            elif location == "Грязное место":
                loc_num = 0


            move[loc_num].click()
            print("Ушел из", location)
            time.sleep(ttime)
    except:
        print("Error")
        time.sleep(20)
        is_refresh = 0
        while is_refresh == 0:
            try:
                driver.refresh()
                is_refresh = 1
            except:
                time.sleep(20)

driver.close()
