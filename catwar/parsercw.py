vocab = {}
rfile = open("../data/catwardb", "r")
for line in rfile:
    if vocab.get(line):
        vocab[line] += 1
    else:
        vocab[line] = 1
rfile.close()

wfile = open("../data/catwarndb", "w")
for key, value in vocab.items():
    wfile.write(str(key)+"|"+str(value)+"\n")

