import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

try:
    status_file = open("../data/Alice_status", "r")
    prev_status = status_file.read()
    status_file.close()
    driver = webdriver.Firefox()
except:
    print(time.asctime(), "Error during opening tyan's page")
while(1):
    try:
        driver.get("https://vk.com/id255678651")

        content = driver.page_source
        write_file = open("../data/Alice", "w")
        write_file.write(content)
        write_file.close()
        input_file = open("../data/Alice", "r")
        is_status = 0
        for line in input_file:
            if "page_current_info" in line:
                status = line[83:-14]
                is_status = 1
        input_file.close()
    except:
        print(time.asctime(), "Error during read a status")
    try:
        if is_status == 0:
            print(time.asctime(), "Status is empty")
        elif status != "" and status != prev_status:
            driver_twit = webdriver.Firefox()
            driver_twit.get("https://twitter.com/login")

            time.sleep(5)
            login = driver_twit.find_element_by_xpath("//input[@class='js-username-field email-input js-initial-focus']")
            cur_login = "+79654164883"
            login.send_keys(cur_login)

            password = driver_twit.find_element_by_xpath("//input[@class='js-password-field']")
            cur_pass = "badpassword"
            password.send_keys(cur_pass)
            time.sleep(2)

            entr = driver_twit.find_element_by_xpath("//button[@class='submit EdgeButton EdgeButton--primary EdgeButtom--medium']")
            entr.click()
            time.sleep(5)

            twit_text = driver_twit.find_element_by_xpath("//div[@id='tweet-box-home-timeline']")
            twit_text.click()
            time.sleep(3)
            twit_text.send_keys(status)
            time.sleep(3)
            twit = driver_twit.find_element_by_xpath("//button[@class='tweet-action EdgeButton EdgeButton--primary js-tweet-btn']")
            twit.click()
            time.sleep(5)

            print(time.asctime(), "New status is", status)
            time.sleep(20)

            driver_twit.close()

            status_file = open("../data/Alice_status", "w")
            status_file.write(status)
            status_file.close()

            prev_status = status
        else:
            print(time.asctime(), "Status didn't change")
    except:
        print(time.asctime(), "Error during twitting")
        driver_twit.close()
    finally:
        time.sleep(100)

driver.close()
